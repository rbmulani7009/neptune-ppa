<!doctype html>
<html>
<head>
    @include('includes.head')
	<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
<div class="container-fluid">
	<div class="header">
		<div class="logo clearfix">
			<h1><a title="Neptune PPA" href="#"><img src="{{ URL::asset('images/logo.jpg') }}" alt="Neptune PPA" /> </a></h1>
		</div>
	</div>
	
	@include('includes.menu')
	
	@yield('content')
</div>
@yield('footer')



</body>
</html>