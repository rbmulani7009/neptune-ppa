
@if(Auth::user()->roll == "1")
	<div class="main-menu">
		<ul>
			<li {{ Request::is('admin/clients') ? 'class=active' : '' || Request::is('admin/clients/add') ? 'class=active' : '' || Request::is('admin/clients/edit/*') ? 'class=active' : '' }}>
				<a href="{{ URL::to('admin/clients') }}" title="">Clients</a>	
			</li>
			<li {{ Request::is('admin/projects') ? 'class=active' : '' || Request::is('admin/projects/add') ? 'class=active' : '' || Request::is('admin/projects/edit/*') ? 'class=active' : '' }}><a href="{{ URL::to('admin/projects') }}" title="">Projects</a></li>
			<li {{ Request::is('admin/category') ? 'class=active' : '' || Request::is('admin/category/add') ? 'class=active' : '' || Request::is('admin/category/edit/*') ? 'class=active' : '' || Request::is('admin/category/viewclient/*') ? 'class=active' : '' }}><a href="{{ URL::to('admin/category') }}" title=""> Categories</a></li>
			<li {{ Request::is('admin/users') ? 'class=active' : '' || Request::is('admin/users/add') ? 'class=active' : '' || Request::is('admin/users/edit/*') ? 'class=active' : '' }}><a href="{{ URL::to('admin/users') }}" title="">Users</a></li>
			<li {{ Request::is('admin/manufacturer') ? 'class=active' : '' || Request::is('admin/manufacturer/add') ? 'class=active' : '' || Request::is('admin/manufacturer/edit/*') ? 'class=active' : '' }}><a href="{{ URL::to('admin/manufacturer') }}" title="">Manufacturers</a></li>
			<li {{ Request::is('admin/devices') ? 'class=active' : '' || Request::is('admin/devices/add') ? 'class=active' : '' || Request::is('admin/devices/edit/*') ? 'class=active' : '' || Request::is('admin/devices/view/*') ? 'class=active' : ''}}><a href="{{ URL::to('admin/devices') }}" title="">Devices</a></li>
			<li {{ Request::is('admin/orders') ? 'class=active' : '' || Request::is('admin/orders/edit/*') ? 'class=active' : '' }}><a href="{{ URL::to('admin/orders') }}" title="">Orders</a></li>
			<li {{ Request::is('admin/schedule') ? 'class=active' : ''  || Request::is('admin/schedule/add') ? 'class=active' : '' || Request::is('admin/schedule/edit/*') ? 'class=active' : ''  }}><a href="{{ URL::to('admin/schedule') }}" title="">Schedule</a></li>
			<li {{ Request::is('admin/marketshare') ? 'class=active' : '' }}><a href="{{ URL::to('admin/marketshare') }}" title="">Market Share</a></li>
			<li><a href="{{ URL::to('admin/logout') }}" title=""> Log Out</a></li>
		</ul>
	</div>
@elseif(Auth::user()->roll == "2")
<div class="main-menu">
		<ul>
			
			<li {{ Request::is('admin/projects') ? 'class=active' : '' || Request::is('admin/projects/add') ? 'class=active' : '' || Request::is('admin/projects/edit/*') ? 'class=active' : '' }}><a href="{{ URL::to('admin/projects') }}" title="">Projects</a></li>
			<li {{ Request::is('admin/category') ? 'class=active' : '' || Request::is('admin/category/add') ? 'class=active' : '' || Request::is('admin/category/edit/*') ? 'class=active' : '' || Request::is('admin/category/viewclient/*') ? 'class=active' : '' }}><a href="{{ URL::to('admin/category') }}" title=""> Categories</a></li>
			<li {{ Request::is('admin/users') ? 'class=active' : '' || Request::is('admin/users/add') ? 'class=active' : '' || Request::is('admin/users/edit/*') ? 'class=active' : '' }}><a href="{{ URL::to('admin/users') }}" title="">Users</a></li>
			<li {{ Request::is('admin/devices') ? 'class=active' : '' || Request::is('admin/devices/add') ? 'class=active' : '' || Request::is('admin/devices/edit/*') ? 'class=active' : '' || Request::is('admin/devices/view/*') ? 'class=active' : ''}}><a href="{{ URL::to('admin/devices') }}" title="">Devices</a></li>
			<li {{ Request::is('admin/orders') ? 'class=active' : '' || Request::is('admin/orders/edit/*') ? 'class=active' : '' }}><a href="{{ URL::to('admin/orders') }}" title="">Orders</a></li>
			<li {{ Request::is('admin/schedule') ? 'class=active' : ''  || Request::is('admin/schedule/add') ? 'class=active' : '' || Request::is('admin/schedule/edit/*') ? 'class=active' : ''  }}><a href="{{ URL::to('admin/schedule') }}" title="">Schedule</a></li>
			<li {{ Request::is('admin/marketshare') ? 'class=active' : '' }}><a href="{{ URL::to('admin/marketshare') }}" title="">Market Share</a></li>
			<li><a href="{{ URL::to('admin/logout') }}" title=""> Log Out</a></li>
		</ul>
	</div>

@elseif(Auth::user()->roll == "4")
<div class="main-menu">
		<ul>
			<li {{ Request::is('admin/orders') ? 'class=active' : '' || Request::is('admin/orders/edit/*') ? 'class=active' : '' }}><a href="{{ URL::to('admin/orders') }}" title="">Orders</a></li>
			<li><a href="{{ URL::to('admin/logout') }}" title=""> Log Out</a></li>
		</ul>
	</div>
@elseif(Auth::user()->roll == "5")
<div class="main-menu">
		<ul>
			<li {{ Request::is('admin/marketshare') ? 'class=active' : '' }}><a href="{{ URL::to('admin/marketshare') }}" title="">Market Share</a></li>
			<li><a href="{{ URL::to('admin/logout') }}" title=""> Log Out</a></li>
		</ul>
	</div>
@endif
