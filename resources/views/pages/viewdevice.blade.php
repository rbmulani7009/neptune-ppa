@extends ('layout.default')
@section ('content')

<div class="content-area clearfix">
    <a title="" href="{{ URL::to('admin/devices') }}" class="pull-right">X</a>
    <div class="tab-container">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#Devices-Details" aria-controls="Devices-Details"
                role="tab" data-toggle="tab">Device Details</a></li>
                <li role="presentation"><a href="#Prices-Details" aria-controls="Prices-Details" role="tab"
                    data-toggle="tab">Price Details</a></li>
                    <li role="presentation"><a href="#Features" aria-controls="Features" role="tab" data-toggle="tab">Features</a>
                    </li>
                    <li role="presentation"><a href="#Contacts" aria-controls="Contacts" role="tab" data-toggle="tab">Contacts</a>
                    </li>
                    <li role="presentation"><a href="#Survey" aria-controls="Survey" role="tab" data-toggle="tab">Survey</a>
                    </li>
                     <li role="presentation"><a href="#Reps" aria-controls="Reps" role="tab" data-toggle="tab">Reps</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="Devices-Details">

                        @if(Auth::user()->roll == 1)
                        <a title="" href="{{ URL::to('admin/devices/edit/'.$device_view->id) }}" class="pull-right"
                            style="margin-top:10px;" data-toggle="modal" style="float:right;">Edit Device Details</a>
                            @endif
                            <div class="col-md-12 col-lg-12 modal-box" style="border:solid 1px #ccc; margin-top:10px;">
                                <div class="content-area clearfix" style="padding:30px 0px 30px 0px;">

                                    <div class="col-md-6 col-lg-6 modal-box" style="border-right:solid 1px #ccc;">
                                        <div class="input1">
                                            {{Form::label('label1', 'Device Image',array("style"=>"vertical-align:top;"))}}
                                            <img src="../../../upload/{{$device_view->device_image}}" width="210" height="150"
                                            style="border:solid 1px #ccc;"/>
                                        </div>
                                        <div class="input1">
                                            {{Form::label('label2', 'Select Level')}}
                                            {{ Form::text('level',$device_view->level_name,array('placeholder'=>'Device ID','readonly'=>'true'))}}
                                        </div>
                                        <div class="input1">
                                            {{Form::label('label2', 'Select Project')}}
                                            {{ Form::text('project',$device_view->project_name,array('placeholder'=>'Device ID','readonly'=>'true'))}}
                                        </div>
                                        <div class="input1">
                                            {{Form::label('label2', 'Select Category')}}
                                            {{ Form::text('category',$device_view->category_name,array('placeholder'=>'Device ID','readonly'=>'true'))}}
                                        </div>
                                        <div class="input1">
                                            {{Form::label('label2', 'Device ID')}}
                                            {{ Form::text('deviceid',$device_view->id,array('placeholder'=>'Device ID','readonly'=>'true'))}}
                                        </div>
                                        <div class="input1">
                                            {{Form::label('label3', 'Manufacturer Name')}}
                                            {{ Form::text('manufacturername',$device_view->manu_name,array('placeholder'=>'Manufacturer Name','readonly'=>'true'))}}
                                        </div>
                                        <div class="input1">
                                            {{Form::label('label4', 'Device Name')}}
                                            {{ Form::text('devicename',$device_view->device_name,array('placeholder'=>'Device Name','readonly'=>'true'))}}
                                        </div>
                                        <div class="input1">
                                            {{Form::label('label5', 'Model No')}}
                                            {{ Form::text('model',$device_view->model_name,array('placeholder'=>'Model No','readonly'=>'true'))}}
                                        </div>
                                        <div class="input1">
                                            {{Form::label('label6', 'Longevity/Yrs')}}
                                            {{ Form::text('longevity',$device_view->longevity,array('placeholder'=>'Longevity/Yrs','readonly'=>'true'))}}

<!-- @if(Auth::user()->roll == 1)
@if($device_view->longevity_check == "True")
{{ Form::checkbox('chk_longevity','True',array('checked'=>'checked')) }}
@else
{{ Form::checkbox('chk_longevity','False') }}
@endif
@endif-->
</div>
<div class="input1">
    {{Form::label('label7', 'Shock CT')}}
    {{ Form::text('shock',$device_view->shock,array('placeholder'=>'Shock CT','readonly'=>'true'))}}

<!--  @if(Auth::user()->roll == 1)
@if($device_view->shock_check == "True")
{{ Form::checkbox('chk_shock','True',array('checked'=>'checked')) }}
@else
{{ Form::checkbox('chk_shock','False') }}
@endif
@endif-->
</div>
<div class="input1">
    {{Form::label('label8', 'Size')}}
    {{ Form::text('size',$device_view->size,array('placeholder'=>'Size','readonly'=>'true'))}}

<!--@if(Auth::user()->roll == 1)
@if($device_view->size_check == "True")
{{ Form::checkbox('chk_size','True',array('checked'=>'checked')) }}
@else
{{ Form::checkbox('chk_size','False') }}
@endif
@endif-->
</div>

</div>
<div class="col-md-6 col-lg-6 modal-box">
    <div class="input1">
        {{Form::label('label9', 'REP Email')}}
        {{ Form::text('repemail',$device_view->email,array('placeholder'=>'REP Email','readonly'=>'true'))}}
    </div>
    <div class="input1">
        {{Form::label('label10', 'Status')}}
        {{ Form::text('status',$device_view->status,array('placeholder'=>'Status','readonly'=>'true'))}}
    </div>
    <div class="input1">
        {{Form::label('label11', 'Research')}}
        {{ Form::text('research',$device_view->research,array('placeholder'=>'Research','readonly'=>'true'))}}

<!--   @if(Auth::user()->roll == 1)
@if($device_view->research_check == "True")
{{ Form::checkbox('research_size','True',array('checked'=>'checked')) }}
@else
{{ Form::checkbox('research_size','False') }}
@endif
@endif -->
</div>
<div class="input1">
    {{Form::label('label12', 'Site Info:')}}
    {{ Form::text('siteinfo',$device_view->website_page,array('placeholder'=>'Site Info','readonly'=>'true'))}}

<!-- @if(Auth::user()->roll == 1)
@if($device_view->website_page_check == "True")
{{ Form::checkbox('chk_website_page','True',array('checked'=>'checked')) }}
@else
{{ Form::checkbox('chk_website_page','False') }}
@endif
@endif -->
</div>
<div class="input1">
    {{Form::label('label13', 'URL')}}
    {{ Form::text('URL',$device_view->url,array('placeholder'=>'URL','readonly'=>'true'))}}
</div>
<div class="input1">
    {{Form::label('label14', 'Overall Value')}}
    {{ Form::text('overall_value',$device_view->overall_value,array('placeholder'=>'Overall Value','readonly'=>'true'))}}

<!--	@if(Auth::user()->roll == 1)
@if($device_view->overall_value_check == "True")
{{ Form::checkbox('chk_overall_value','True',array('checked'=>'checked')) }}
@else
{{ Form::checkbox('chk_overall_value','False') }}
@endif
@endif -->
</div>
@foreach($custom_fields as $custom_field)
<div class="input1">
    {{Form::label('label13', $custom_field->field_name)}}
    {{ Form::text('custom_field'.$custom_field->id,NULL,array('placeholder'=>$custom_field->field_value,'readonly'=>'true'))}}
    @if(Auth::user()->roll == 1)
    @if($custom_field->field_check == "True")
    {{ Form::checkbox('chk_custom','True',array('checked'=>'checked')) }}
    @else
    {{ Form::checkbox('chk_custom','False') }}
    @endif
    @endif
</div>
@endforeach
</div>
</div>
</div>
</div>
<div role="tabpanel" class="tab-pane" id="Prices-Details">
    <div class="content-area clearfix" style="padding:30px 30px 30px;">
        <div class="top-links clearfix">
            @if(Auth::user()->roll == 1)
            <a title="" href="{{ URL::to('admin/devices/clientprice/'.$device_view->id) }}"
                class="pull-right" data-toggle="modal"><i class="fa fa-plus-circle"></i> Add Client
                Price</a>
                @endif
            </div>
            <div class="table">
                <table>
                    <thead>
                        <tr>
                            <th>Client Name</th>
                            <th>Unit Cost</th>
                            <th>Dis. Unit Cost%</th>
                            <th>Repless Disc.</th>
                            <th>Repless Disc%</th>
                            <th>System Cost</th>
                            <th>Disc.Sys. Cost%</th>
                            <th>CCO Discount</th>
                            <th>CCO Disc. %</th>
                            <th>Repless Disc.</th>
                            <th>Repless Disc.%</th>
                            <th>Unit Bulk</th>
                            <th>System Bulk</th>
                            <th>Reimb.</th>
                            @if(Auth::user()->roll == 1)
                            <th>Action</th>
                            @endif
                        </tr>
                        <tr>

                            <td><input type="text" class='search_text' data-field='clients.client_name'/></td>
                            <td><input type="text" class='search_text' data-field='client_price.unit_cost'/>
                            </td>
                            <td><input type="text" class='search_text'
                                data-field='client_price.bulk_unit_cost'/></td>
                                <td><input type="text" class='search_text' data-field='client_price.unit_rep_cost'/>
                                </td>
                                <td><input type="text" class='search_text'
                                    data-field='client_price.unit_repless_discount'/></td>
                                    <td><input type="text" class='search_text' data-field='client_price.system_cost'/>
                                    </td>
                                    <td><input type="text" class='search_text'
                                        data-field='client_price.bulk_system_cost'/></td>
                                        <td><input type="text" class='search_text' data-field='client_price.cco'/></td>
                                        <td><input type="text" class='search_text' data-field='client_price.cco_discount'/>
                                        </td>
                                        <td><input type="text" class='search_text'
                                            data-field='client_price.system_repless_cost'/></td>
                                            <td><input type="text" class='search_text'
                                                data-field='client_price.system_repless_discount'/></td>
                                                <td><input type="text" class='search_text' data-field='client_price.bulk'/></td>
                                                <td><input type="text" class='search_text' data-field='client_price.system_bulk'/>
                                                </td>
                                                <td><input type="text" class='search_text' data-field='client_price.reimbursement'/>
                                                </td>
                                                @if(Auth::user()->roll == 1)
                                                <td style="width:100px;"></td>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody id="device_result">
                                            @foreach($client_prices as $client_price)
                                            <tr>
                                                <td>{{$client_price->client_name}}</td>
                                                <td>${{$client_price->unit_cost}}</td>
                                                <td>{{$client_price->bulk_unit_cost > 0 ? $client_price->bulk_unit_cost : '-'}}</td>
                                                <td>{{$client_price->unit_rep_cost > 0 ? "$".$client_price->unit_rep_cost : '-'}}</td>
                                                <td>{{$client_price->unit_repless_discount > 0 ? $client_price->unit_repless_discount ."%": '-'}}</td>
                                                <td>${{$client_price->system_cost}}</td>
                                                <td>{{$client_price->bulk_system_cost > 0 ? $client_price->bulk_system_cost : '-'}}</td>
                                                <td>{{$client_price->cco > 0 ? "$".$client_price->cco : '-'}}</td>
                                                <td>{{$client_price->cco_discount > 0 ? $client_price->cco_discount : '-'}}</td>
                                                <td>{{$client_price->system_repless_cost > 0 ? "$".$client_price->system_repless_cost : '-'}}</td>
                                                <td>{{$client_price->system_repless_discount > 0 ? $client_price->system_repless_discount  : '-'}}</td>
                                                <td>{{$client_price->remain_bulk > 0 ? $client_price->remain_bulk : '-' }}</td>
                                                <td>{{$client_price->remain_system_bulk > 0 ? $client_price->remain_system_bulk : '-' }}</td>
                                                <td>{{$client_price->reimbursement > 0 ? "$".$client_price->reimbursement : '-'}}</td>
                                                @if(Auth::user()->roll == 1)
                                                <td>
                                                    <a href="{{ URL::to('admin/devices/clientpriceedit/'.$client_price->id) }}"
                                                        data-toggle="modal"><i class="fa fa-edit"></i></a>
                                                        &nbsp; <a
                                                        href="{{ URL::to('admin/devices/clientpriceremove/'.$client_price->id) }}"
                                                        onclick="return confirm(' Are you sure you want to delete clientprice?');"><i
                                                        class="fa fa-close"></i></a>
                                                    </td>
                                                    @endif
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="bottom-count clearfix">

                                    </div>
                                </div>
                            </div>


        <div role="tabpanel" class="tab-pane" id="Features">
            <div class="content-area clearfix" style="padding:30px 30px 30px;">
                <div class="top-links clearfix">
                    @if(Auth::user()->roll == 1)
                    <a title="" href="{{ URL::to('admin/devices/devicefeatures/'.$device_view->id) }}"
                        class="pull-right" data-toggle="modal"><i class="fa fa-plus-circle"></i> Add Device
                        Features</a>
                        @endif
                    </div>
                    <div class="table">
                        <table>
                            <thead>
                                <tr>
                                    <th>Client Name</th>
                                    <th>Longevity/Yrs</th>
                                    <th>Shock CT</th>
                                    <th>Size</th>
                                    <th>Research</th>
                                    <th>Site Info</th>
                                    <th>URL</th>
                                    <th>Overall Value</th>
                                    @if(Auth::user()->roll == 1)
                                    <th>Action</th>
                                    @endif
                                </tr>
                                <tr>

                                    <td><input type="text" class='dfsearch_text' data-field='clients.client_name'/></td>
                                    <td><input type="text" class='dfsearch_text'
                                        data-field='device_features.longevity'/></td>
                                        <td><input type="text" class='dfsearch_text' data-field='device_features.shock'/>
                                        </td>
                                        <td><input type="text" class='dfsearch_text' data-field='device_features.size'/>
                                        </td>
                                        <td><input type="text" class='dfsearch_text' data-field='device_features.research'/>
                                        </td>
                                        <td><input type="text" class='dfsearch_text'
                                            data-field='device_features.site_info'/></td>
                                            <td><input type="text" class='dfsearch_text' data-field='device_features.url'/></td>
                                            <td><input type="text" class='dfsearch_text'
                                                data-field='device_features.overall_value'/></td>
                                                @if(Auth::user()->roll == 1)
                                                <td style="width:100px;"></td>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody id="device_features_result">
                                            @foreach($device_features as $row)
                                            <tr>
                                                <td>{{$row->client_name}}</td>
                                                <td>{{$row->longevity}}</td>
                                                <td>{{$row->shock}}</td>
                                                <td>{{$row->size}}</td>
                                                <td>{{$row->research}}</td>
                                                <td>{{$row->website_page}}</td>
                                                <td>{{$row->url}}</td>
                                                <td>{{$row->overall_value}}</td>
                                                @if(Auth::user()->roll == 1)
                                                <td>
                                                    <a href="{{ URL::to('admin/devices/devicefeatures/edit/'.$row->id) }}"
                                                        data-toggle="modal"><i class="fa fa-edit"></i></a>
                                                        &nbsp; <a
                                                        href="{{ URL::to('admin/devices/devicefeatures/remove/'.$row->id) }}"
                                                        onclick="return confirm(' Are you sure you want to delete device features?');"><i
                                                        class="fa fa-close"></i></a>
                                                    </td>
                                                    @endif
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="bottom-count clearfix">

                                    </div>
                                </div>
                            </div>
                    <div role="tabpanel" class="tab-pane" id="Survey">
                                <div class="content-area clearfix" style="padding:30px 30px 30px;">
                                    <div class="top-links clearfix">
                                        @if(Auth::user()->roll == 1)

                                        <a title="" href=""
                                        id="copysurvey" class="pull-right surveystyle" data-toggle="modal"><i
                                        class="fa fa-files-o" aria-hidden="true"></i> Copy Existing Survey</a>
                                        <a title="" href="{{ URL::to('admin/devices/devicesurvey/'.$device_view->id) }}"
                                            class="pull-right" data-toggle="modal"><i class="fa fa-plus-circle"></i> Add Survey
                                            Features</a>
                                            @endif
                                        </div>
                                        <div class="table">
                                            <table>
                                                <thead>
                                                    <tr>
                                                        @if(Auth::user()->roll == 1)
                                                        <th width="30">&nbsp;  </th>
                                                        @endif
                                                        <th>Client Name</th>
                                                        <th>Question 1</th>
                                                        <th>Question 2</th>
                                                        <th>Question 3</th>
                                                        <th>Question 4</th>
                                                        <th>Question 5</th>
                                                        <th>Question 6</th>
                                                        <th>Question 7</th>
                                                        <th>Question 8</th>
                                                        @if(Auth::user()->roll == 1)
                                                        <th>Action</th>
                                                        @endif
                                                    </tr>
             <tr>
                {{  Form::open(array('url'=>'' , 'method' =>'POST','class'=>'form-horizontal','id'=>'ajax_data','files'=>'true'))}}

                @if(Auth::user()->roll == 1)
                <td></td>
                @endif
                {{ Form::hidden('device_id',$device_view->id)}}
                <td><input type="text" class='dssearch_text' name="search[]"
                    data-field='clients.client_name'/></td>
                    <td><input type="text" class='dssearch_text' name="search[]"
                        data-field='survey.que_1'/></td>
                        <td><input type="text" class='dssearch_text' name="search[]" data-field='survey.que_2'/>
                    </td>
                    <td><input type="text" class='dssearch_text' name="search[]"
                         data-field='survey.que_3'/>
                    </td>
                    <td><input type="text" class='dssearch_text' name="search[]"
                         data-field='survey.que_4'/>
                    </td>
                    <td><input type="text" class='dssearch_text' name="search[]"
                         data-field='survey.que_5'/></td>
                    <td><input type="text" class='dssearch_text' name="search[]"
                             data-field='survey.que_6'/></td>
                    <td><input type="text" class='dssearch_text' name="search[]"
                                 data-field='survey.que_7'/></td>
                    <td><input type="text" class='dssearch_text' name="search[]"
                                     data-field='survey.que_8'/></td>
                     @if(Auth::user()->roll == 1)
                     <td style="width:100px;"></td>
                     @endif
                    {{ Form::close()}}
                </tr>
                 </thead>
                 
                 <tbody id="device_survey_result">
                    @foreach($device_survey as $row)
                    <tr>
                        @if(Auth::user()->roll == 1)
                        <td><input type="checkbox" class='chk_device' name="chk_survey"
                         value="{{$row->id}}"/></td>
                         @endif

                         <td>{{$row->client->client_name}}</td>
                         <td>{{$row->que_1}}</td>
                         <td>{{$row->que_2}}</td>
                         <td>{{$row->que_3}}</td>
                         <td>{{$row->que_4}}</td>
                         <td>{{$row->que_5}}</td>
                         <td>{{$row->que_6}}</td>
                         <td>{{$row->que_7}}</td>
                         <td>{{$row->que_8}}</td>
                         @if(Auth::user()->roll == 1)
                         <td>
                            <a href="{{ URL::to('admin/devices/devicesurvey/view/'.$row->id) }}"
                             data-toggle="modal"><i class="fa fa-eye"></i></a>
                             &nbsp;
                             <a href="{{ URL::to('admin/devices/devicesurvey/edit/'.$row->id) }}"
                                 data-toggle="modal"><i class="fa fa-edit"></i></a>
                                 &nbsp; <a
                                 href="{{ URL::to('admin/devices/devicesurvey/remove/'.$row->id) }}"
                                 onclick="return confirm(' Are you sure you want to delete device Survey?');"><i
                                 class="fa fa-close"></i></a>
                             </td>
                             @endif
                         </tr>
                         @endforeach
                     </tbody>
                        
                 </table>
             </div>
             <div class="bottom-count clearfix">

             </div>
         </div>
        </div>


        <!-- Custom Contact Information Start -->

             <div role="tabpanel" class="tab-pane" id="Contacts">
                <div class="content-area clearfix" style="padding:30px 30px 30px;">
                    <div class="top-links clearfix">
                        @if(Auth::user()->roll == 1)

                        <a title="" href="{{ URL::to('admin/devices/customcontact/'.$device_view->id) }}"
                            class="pull-right" data-toggle="modal"><i class="fa fa-plus-circle"></i> Add Contact Features</a>
                            @endif
                        </div>
                        <div class="table">
                            <table>
                                <thead>
                                    <tr>
                                    
                                        <th>Client Name</th>
                                        <th>Order Email</th>
                                        <th>Email CC 1</th>
                                        <th>Email CC 2</th>
                                        <th>Email CC 3</th>
                                        <th>Email CC 4</th>
                                        <th>Email CC 5</th>
                                       
                                        <th>Subject</th>
                                       
                                        @if(Auth::user()->roll == 1)
                                        <th>Action</th>
                                        @endif
                                    </tr>
                                     <tr>
                                        {{  Form::open(array('url'=>'' , 'method' =>'POST','class'=>'form-horizontal','id'=>'ajax_contact_data','files'=>'true'))}}

                                    
                                        {{ Form::hidden('device_id',$device_view->id)}}
                                        <td><input type="text" class='cisearch_text' name="search[]"
                                            data-field='clients.client_name'/></td>
                                            <td><input type="text" class='cisearch_text' name="search[]"
                                                data-field='survey.order_email'/></td>
                                                <td><input type="text" class='cisearch_text' name="search[]" data-field='survey.cc1'/>
                                            </td>
                                            <td><input type="text" class='cisearch_text' name="search[]"
                                                 data-field='survey.cc2'/>
                                            </td>
                                            <td><input type="text" class='cisearch_text' name="search[]"
                                                 data-field='survey.cc3'/>
                                            </td>
                                            <td><input type="text" class='cisearch_text' name="search[]"
                                                 data-field='survey.cc4'/></td>
                                            <td><input type="text" class='cisearch_text' name="search[]"
                                                     data-field='survey.cc5'/></td>
                                            
                                            <td><input type="text" class='cisearch_text' name="search[]"
                                                             data-field='survey.subject'/></td>
                                             @if(Auth::user()->roll == 1)
                                             <td style="width:100px;"></td>
                                             @endif
                                            {{ Form::close()}}
                                        </tr>
                                </thead>
                                     
                                <tbody id="device_contact_result">
                                        @foreach($deviceCustomContact as $row)
                                        <tr>
                                    
                                            <td>{{$row->client->client_name}}</td>
                                            <td>{{$row->user->email}}</td>
                                            <td>{{$row->cc1}}</td>
                                            <td>{{$row->cc2}}</td>
                                            <td>{{$row->cc3}}</td>
                                            <td>{{$row->cc4}}</td>
                                            <td>{{$row->cc5}}</td>
                                            <td>{{$row->subject}}</td>
                                           
                                             @if(Auth::user()->roll == 1)
                                            <td>
                                                <a href="{{ URL::to('admin/devices/devicesurvey/view/'.$row->id) }}"
                                                 data-toggle="modal"><i class="fa fa-eye"></i></a>
                                                 &nbsp;
                                                 <a href="{{ URL::to('admin/devices/customcontact/edit/'.$row->id) }}"
                                                     data-toggle="modal"><i class="fa fa-edit"></i></a>
                                                     &nbsp; <a
                                                     href="{{ URL::to('admin/devices/customcontact/remove/'.$row->id) }}"
                                                     onclick="return confirm(' Are you sure you want to delete device Survey?');"><i
                                                     class="fa fa-close"></i></a>
                                            </td>
                                                 @endif
                                        </tr>
                                             @endforeach
                                </tbody>
                                            
                            </table>
                             </div>
                             <div class="bottom-count clearfix">

                             </div>
         </div>
        </div>
        <!-- Custom Contact Information End -->

        <!-- Rep User Tab start -->

            <div role="tabpanel" class="tab-pane" id="Reps">
                <div class="content-area clearfix" style="padding:30px 30px 30px;">
                    <div class="top-links clearfix">
                        @if(Auth::user()->roll == 1)

                        <a title="" href="#"
                            class="pull-left" id="deviceexport" data-toggle="modal">Export</a>
                            @endif
                        </div>
                        <div class="table">
                            <table>
                                <thead>
                                    <tr>
                                          <th></th>
                                        <th>Rep Name</th>
                                        <th>Email</th>
                                        <th>Mobile</th>
                                        <th>Title</th>
                                        <th>Company</th>
                                        
                                        <th>Receives Order Email</th>
                                    @if(Auth::user()->roll == 1)
                                        <th>Action</th>
                                    @endif
                                    </tr>
                                     <tr>
                                        {{  Form::open(array('url'=>'' , 'method' =>'POST','class'=>'form-horizontal','id'=>'ajax_rep_data','files'=>'true'))}}

                                    
                                        {{ Form::hidden('deviceId',$device_view->id)}}
                                        @if(Auth::user()->roll == 1)
                                        <td><input type="checkbox" id="checkmain"/></td>
                                        @endif
                                        <td><input type="text" class='repsearch_text' name="search[]"
                                            data-field='rep.name'/></td>
                                            <td><input type="text" class='repsearch_text' name="search[]"
                                                data-field='rep.email'/></td>
                                            <td><input type="text" class='repsearch_text' name="search[]"
                                                 data-field='rep.mobile'/>
                                            </td>
                                            <td><input type="text" class='repsearch_text' name="search[]"
                                                 data-field='rep.title'/>
                                            </td>
                                            <td><input type="text" class='repsearch_text' name="search[]" data-field='rep.company'/>
                                            </td>
                                            <td style="width:100px;"></td>
                                            
                                             @if(Auth::user()->roll == 1)
                                             <td style="width:100px;"></td>
                                             @endif
                                            {{ Form::close()}}
                                        </tr>
                                </thead>
                                     {{Form::open(array('url'=>'admin/devices/reinfo/export','method'=>'post','id'=>'formexport'))}}
                                <tbody id="device_rep_result">
                                        @foreach($deviceRepUser as $row)
                                        <tr>
                                            
                                            {{ Form::hidden('deviceId',$device_view->id,array('id'=>'deviceId'))}}
                                            @if(Auth::user()->roll == 1)
                                            <td><input type="checkbox" class='chk_rep' name="chk_rep[]" value="{{$row->id}}"/></td>
                                            @endif
                                            <td>{{$row->name}}</td>
                                            <td>{{$row->email}}</td>
                                            <td>{{$row->mobile == "" ? '-':$row->mobile}}</td>
                                            <td>{{$row->title == "" ? '-':$row->title}}</td>
                                            <td>{{$row->manufacturer_name}}</td>
                                          
                                            <td>{{ Form:: hidden('repId',$row->id,array('class'=>'repId'))}}
                                            {{ Form::select('status',array('No' => 'No','Yes' => 'Yes'),$row->repStatus =="Yes"?"Yes":"No",array('id'=>'repStatus','class'=>'repStatus')) }}</td>
                                             @if(Auth::user()->roll == 1)
                                            <td>
                                                <a href="#"
                                                 data-toggle="modal">View</a>
                                                 &nbsp;
                                                 <a href="{{ URL::to('admin/devices/repinfo/edit/'.$row->id.'/'.$device_view->id) }}"
                                                     data-toggle="modal">Edit</a>
                                                     
                                            </td>
                                             {{ Form::close()}}
                                                 @endif
                                        </tr>
                                             @endforeach
                                </tbody>
                                 {{Form::close()}}
                                            
                            </table>

                             </div>
                            <div class="bottom-count clearfix">
                            {{$deviceRepUser->count()}} of {{$count}} displayed 
                            {{Form::open(array('url'=>'admin/devices/view/'.$device_view->id,'method'=>'get','id'=>'pagesize_form','style'=>'display:inline-block;'))}}
                            {{Form::select('repPageSize', array('10' => 'Show 10','15' => 'Show 15','20' => 'Show 20',$count=>'Show all'),$pagesize,array('id'=>'pagesize','onchange' => 'this.form.submit()'))}}

                            {{Form::close()}}
                            </div>
         </div>
        </div>
        <!-- Rep User Tab End -->
        </div>

    </div>


</div>

<script>
$(document).ready(function () {


$(".search_text").keyup(function () {
var userrole = {{Auth::user()->roll}};
var fieldName = $(this).data('field');
var value = $(this).val();
var id = {{$device_view->id}};
if (userrole == 1) {

    $.ajax({
        url: "{{ URL::to('admin/searchclientprice')}}",
        data: {
            id: id,
            fieldName: fieldName,
            value: value
        },
        success: function (data) {
            // console.log(data);
            var html_data = '';
            if (data.status) {
                $.each(data.value, function (i, item) {

                    var bulk_unit_cost = item.bulk_unit_cost > 0 ? item.bulk_unit_cost : "-";
                    var bulk_system_cost = item.bulk_system_cost > 0 ? item.bulk_system_cost : "-";
                    var unit_rep_cost = item.unit_rep_cost > 0 ? "$" + item.unit_rep_cost : "-";
                    var unit_repless_discount = item.unit_repless_discount > 0 ? item.unit_repless_discount + "%" : "-";
                    var cco = item.cco > 0 ? "$" + item.cco : "-";
                    var cco_discount = item.cco_discount > 0 ? item.cco_discount + "%" : "-";

                    var system_repless_cost = item.system_repless_cost > 0 ? "$" + item.system_repless_cost : "-";
                    var system_repless_discount = item.system_repless_discount > 0 ? item.system_repless_discount + "%" : "-";
                    html_data += "<tr><td>" + item.client_name + "</td><td>$" + item.unit_cost + "</td><td>" + bulk_unit_cost + "%</td><td>" + unit_rep_cost + "</td><td>" + unit_repless_discount + "</td><td>$" + item.system_cost + "</td><td>" + bulk_system_cost + "%</td><td>" + cco + "</td><td>" + cco_discount + "</td><td>" + system_repless_cost + "</td><td>" + system_repless_discount + "</td><td>" + item.remain_bulk + "</td><td>" + item.remain_system_bulk + "</td><td>" + reimbursement + "</td><td><a href=../clientpriceedit/" + item.id + "><i class='fa fa-edit'></i></a>&nbsp; <a href=../clientpriceremove/" + item.id + " onclick=return&nbsp;confirm('Are&nbsp;you&nbsp;sure&nbsp;you&nbsp;want&nbsp;to&nbsp;delete&nbsp;clientprice?');><i class='fa fa-close'></i></a></td></tr>";
                var reimbursement = item.reimbursement > 0 ? "$" + item.reimbursement : "-";

                });
            } else {
                html_data = "<tr> <td colspan='14' style='text-align:center;'> " + data.value + " </td> </tr>"
            }

            // console.log(html_data);
            $("#device_result").html(html_data);

        }

    });
}
else {
    $.ajax({
        url: "{{ URL::to('admin/searchclientprice')}}",
        data: {
            id: id,
            fieldName: fieldName,
            value: value
        },
        success: function (data) {
            // console.log(data);
            var html_data = '';
            if (data.status) {
                $.each(data.value, function (i, item) {

                    var bulk_unit_cost = item.bulk_unit_cost > 0 ? item.bulk_unit_cost : "-";
                    var bulk_system_cost = item.bulk_system_cost > 0 ? item.bulk_system_cost : "-";
                    var unit_rep_cost = item.unit_rep_cost > 0 ? "$" + item.unit_rep_cost : "-";
                    var unit_repless_discount = item.unit_repless_discount > 0 ? item.unit_repless_discount : "-";
                    var cco = item.cco > 0 ? "$" + item.cco : "-";
                    var cco_discount = item.cco_discount > 0 ? item.cco_discount : "-";

                    var system_repless_cost = item.system_repless_cost > 0 ? "$" + item.system_repless_cost : "-";
                    var system_repless_discount = item.system_repless_discount > 0 ? item.system_repless_discount : "-";
                    var reimbursement = item.reimbursement > 0 ? "$" + item.reimbursement : "-";


                    html_data += "<tr><td>" + item.client_name + "</td><td>" + item.unit_cost + "</td><td>" + bulk_unit_cost + "</td><td>" + unit_rep_cost + "$</td><td>" + unit_repless_discount + "</td><td>" + item.system_cost + "</td><td>" + bulk_system_cost + "</td><td>" + cco + "</td><td>" + cco_discount + "</td><td>" + system_repless_cost + "</td><td>" + system_repless_discount + "</td><td>" + item.remain_bulk + "</td><td>" + item.remain_system_bulk + "</td><td>" + reimbursement + "$</td></tr>";

                });
            } else {
                html_data = "<tr> <td colspan='14' style='text-align:center;'> " + data.value + " </td> </tr>"
            }

            // console.log(html_data);
            $("#device_result").html(html_data);

        }

    });

}

});


$(".dfsearch_text").keyup(function () {
    var fieldName = $(this).data('field');
    var value = $(this).val();
    var deviceid = {{$device_view->id}};

    $.ajax({
        url: "{{ URL::to('admin/searchdevicefeatures')}}",
        data: {
            fieldName: fieldName,
            value: value,
            deviceid: deviceid
        },
        success: function (data) {
            // console.log(data);
            var html_data = '';
            if (data.status) {
                $.each(data.value, function (i, item) {


                    html_data += "<tr><td>" + item.client_name + "</td><td>" + item.longevity + "</td><td>" + item.shock + "</td><td>" + item.size + "</td><td>" + item.research + "</td><td>" + item.site_info + "</td><td>" + item.url + "</td><td>" + item.overall_value + "</td><td><a href=../devicefeatures/edit/" + item.id + "><i class='fa fa-edit'></i></a>&nbsp; <a href=../devicefeatures/remove/" + item.id + " onclick=return&nbsp;confirm('Are&nbsp;you&nbsp;sure&nbsp;you&nbsp;want&nbsp;to&nbsp;delete&nbsp;clientprice?');><i class='fa fa-close'></i></a></td></tr>";

                });
            } else {
                html_data = "<tr> <td colspan='14' style='text-align:center;'> " + data.value + " </td> </tr>"
            }

            // console.log(html_data);
            $("#device_features_result").html(html_data);

        }

    });


});

//            Device Survey
$(".dssearch_text").keyup(function () {
    var data = $('#ajax_data').serialize();
    // console.log(data);
    $.ajax({
        url: "{{URL::to('admin/devices/devicesurvey/search')}}",
        data: $('#ajax_data').serialize(),
        type: "POST",
        success: function (data) {
            // console.log(data);
            var html_data = '';
            if (data.status) {
                $.each(data.value, function (i, item) {


                    html_data += "<tr><td><input type='checkbox' class='chk_device' name='chk_survey' value=" + item.id + "></td><td>" + item.client_name + "</td><td>" + item.que_1 + "</td><td>" + item.que_2 + "</td><td>" + item.que_3 + "</td><td>" + item.que_4 + "</td><td>" + item.que_5 + "</td><td>" + item.que_6 + "</td><td>" + item.que_7 + "</td><td>" + item.que_8 + "</td><td><a href=../devicesurvey/view/" + item.id + "><i class='fa fa-eye'></i></a>&nbsp;<a href=../devicesurvey/edit/" + item.id + "><i class='fa fa-edit'></i></a>&nbsp; <a href=../devicefeatures/remove/" + item.id + " onclick=return&nbsp;confirm('Are&nbsp;you&nbsp;sure&nbsp;you&nbsp;want&nbsp;to&nbsp;delete&nbsp;survey?');><i class='fa fa-close'></i></a></td></tr>";

                });
            } else {
                html_data = "<tr> <td colspan='14' style='text-align:center;'> " + data.value + " </td> </tr>"
            }

            // console.log(html_data);
            $("#device_survey_result").html(html_data);

        }
    });

               

            });

// Custom Contact
$(".cisearch_text").keyup(function () {
    var data = $('#ajax_contact_data').serialize();
    $.ajax({
        url: "{{URL::to('admin/devices/customcontact/search')}}",
        data: $('#ajax_contact_data').serialize(),
        type: "POST",
        success: function (data) {
           
            var html_data = '';
            if (data.status) {
                $.each(data.value, function (i, item) {


                    html_data += "<tr><td>" + item.client_name + "</td><td>" + item.email + "</td><td>" + item.cc1 + "</td><td>" + item.cc2 + "</td><td>" + item.cc3 + "</td><td>" + item.cc4 + "</td><td>" + item.cc5 + "</td><td>" + item.subject + "</td><td><a href=../devicesurvey/view/" + item.id + "><i class='fa fa-eye'></i></a>&nbsp;<a href=../customcontact/edit/" + item.id + "><i class='fa fa-edit'></i></a>&nbsp; <a href=../customcontact/remove/" + item.id + " onclick=return&nbsp;confirm('Are&nbsp;you&nbsp;sure&nbsp;you&nbsp;want&nbsp;to&nbsp;delete&nbsp;survey?');><i class='fa fa-close'></i></a></td></tr>";

                });
            } else {
                html_data = "<tr> <td colspan='14' style='text-align:center;'> " + data.value + " </td> </tr>"
            }

            // console.log(html_data);
            $("#device_contact_result").html(html_data);

        }
    });

               

            });

// rep Contact
$(".repsearch_text").keyup(function () {
    var data = $('#ajax_rep_data').serialize();
    $.ajax({
        url: "{{URL::to('admin/devices/repcontact/search')}}",
        data: $('#ajax_rep_data').serialize(),
        type: "POST",
        success: function (data) {
           
            var html_data = '';
            if (data.status) {
                $.each(data.value, function (i, item) {


                     if("{{Auth::user()->roll == 1}}"){
                                            var check ="<input type='checkbox' class='chk_rep' name='chk_rep[]' value="+item.id+">";

                            }         
                    if(item.repStatus == 'Yes'){
                         var option = "<option value='Yes' selected='selected'>Yes</option><option value='No'>No</option>";
                    } else {
                      var option = "<option value='Yes' >Yes</option><option value='No' selected='selected'>No</option>";
                    }
                            
                    html_data += "<tr><td>"+check+"</td><td>" + item.name + "</td><td>" + item.email + "</td><td>" + item.mobile + "</td><td>" + item.title + "</td><td>" + item.manufacturer_name + "</td><td><input type='hidden' name='deviceId' value='"+item.device+"' id='deviceId'><select name='status' class ='repStatus' >"+option+"</select><input type='hidden' name='repId' value='"+item.id+"' id='repId'></td><td><a href=#" + item.id + ">View</a> &nbsp; <a href=../repinfo/edit/" + item.id +"/"+ item.device+ ">Edit</a></td></tr>";
                });
            } else {
                html_data = "<tr> <td colspan='14' style='text-align:center;'> " + data.value + " </td> </tr>";
            }

            // console.log(html_data);
            $("#device_rep_result").html(html_data);

        }
    });

               

            });
});
</script>


<script type="text/javascript">

    // rep status change start

    $(document).on("change",".repStatus",function (event) {
        var deviceId = $('#deviceId').val();
        var repId = $(this).parent("td").children(".repId").val();
        var repStatus = $(this).val();
        alert(repStatus);
        $.ajax({
            url: "{{URL::to('admin/devices/repinfo/status')}}",
            data: {
                        _token: "{{ csrf_token() }}",
                        deviceId: deviceId,repId:repId,repStatus:repStatus   
                    },
            type: "POST",
            success: function (data) {
               
            }
        });

    });
    
    // rep status change end


    $(function () {
        $('#copysurvey').on("click",function (e) {

            if ($(".chk_device:checked").length == 0) {

                alert("Please select record ");
                
                return false;
            }
            else if ($(".chk_device:checked").length > 1) {
                alert("Please select only one record ");
                
                return false;
            }
            else {

               // e.preventDefault();
                var daa = $("input[name='chk_survey']:checked").val();
                var url = '{{URL::to("admin/devices/devicesurvey/copysurvey/")}}' ;
                var path = url +"/"+daa;
                // console.log(path);
                window.location.href = path;
                // window.location(path);
                // $(location).attr('href',url+"/"+daa);
            }

        });
    })
    ;
</script>
<script type="text/javascript">
$(document).ready(function(){
    $("#checkmain").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });

    $("#deviceexport").click(function(){
        
        if($(".chk_device:checked").length == 0)
        {
            
            alert("Please select record and export");
            return false;
        }
        else
        {
            
            $("#formexport").submit();
            return true;
        }
    });
});
</script>

@stop