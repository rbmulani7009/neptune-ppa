@extends ('layout.default')
@section ('content')
<div class="add_new">
<div class="box-center1">
<div class="add_new_box">
		
<div class="col-md-12 col-lg-12 modal-box" style="margin-top:10px;">
 <a title="" href="{{ URL::to('admin/devices/view/'.$deviceid) }}" class="pull-right" data-toggle="modal" >X</a>		<h3 style="text-align:center;">Update Features</h3>
			<ul>
                @foreach($errors->all() as $error)
                <li style="color:red; margin:5px;">{{ $error }}</li>
                @endforeach
            </ul>
			{{ Form::open(array('url' => 'admin/devices/devicefeatures/update/'.$devicefeatures['id'],'method'=>'POST','files'=>true)) }}
			<div class="content-area clearfix"  style="padding:30px 0px 30px 0px;">
				<div class="col-md-6 col-lg-6 modal-box" style="border-right:solid 1px #ccc;">
					<div class="input1">
						{{ Form::hidden('device_id',$deviceid)}}
					</div>
					<div class="input1">
						{{Form::label('label2', 'Select Client')}}
						{{ Form::select('client_name', $client_name,$devicefeatures['client_name'],array('id'=>'clientname')) }}
					</div>
					<div class="input1">
						{{Form::label('label3', 'Longevity/Yrs')}}
						{{ Form::text('longevity',$devicefeatures['longevity'],array('placeholder'=>'Longevity/Yrs','Readonly'=>'True'))}}
                        @if($devicefeatures['longevity_check'] == "True")
						{{ Form::checkbox('chk_longevity','True',array('checked'=>'checked')) }}
						@else
						{{ Form::checkbox('chk_longevity','True') }}
						@endif
					</div>
					<div class="input1">
						{{Form::label('label4', 'Shock CT')}}
						{{ Form::text('shock',$devicefeatures['shock'],array('placeholder'=>'Shock CT','Readonly'=>'True'))}}
						
						@if($devicefeatures['shock_check'] == "True")
						{{ Form::checkbox('shock_check','True',array('checked'=>'checked')) }}
						@else
						{{ Form::checkbox('shock_check','True') }}
						@endif
					</div>
					<div class="input1">
						{{Form::label('label5', 'Size')}}
						{{ Form::text('size',$devicefeatures['size'],array('placeholder'=>'Size','Readonly'=>'True'))}}
						@if($devicefeatures['size_check'] == "True")
						{{ Form::checkbox('size_check','True',array('checked'=>'checked')) }}
						@else
						{{ Form::checkbox('size_check','True') }}
						@endif
					</div>
					<div class="input1">
						{{Form::label('label17', 'Research')}}
						{{ Form::text('research',$devicefeatures['research'],array('placeholder'=>'Research','Readonly'=>'True'))}}
						@if($devicefeatures['research_check'] == "True")
						{{ Form::checkbox('research_check','True',array('checked'=>'checked')) }}
						@else
						{{ Form::checkbox('research_check','True') }}
						@endif
						
					</div>
                    <div class="input1">
						{{Form::label('label6', 'Site Info')}}
						{{ Form::text('site_info',$devicefeatures['website_page'],array('placeholder'=>'Site Info','Readonly'=>'True'))}}
						@if($devicefeatures['siteinfo_check'] == "True")
						{{ Form::checkbox('siteinfo_check','True',array('checked'=>'checked')) }}
						@else
						{{ Form::checkbox('siteinfo_check','True') }}
						@endif
					</div>
					<div class="input1">
						{{Form::label('label7', 'URL')}}
						{{ Form::text('url',$devicefeatures['url'],array('placeholder'=>'URL','Readonly'=>'True'))}}
					</div>
					
				</div>
				<div class="col-md-6 col-lg-6 modal-box">
					
					<div class="input1">
						{{Form::label('label8', 'Overall Value')}}
						{{ Form::text('overall_value',$devicefeatures['overall_value'],array('placeholder'=>'Overall Value','Readonly'=>'True'))}}
						@if($devicefeatures['overall_value_check'] == "True")
						{{ Form::checkbox('overall_value_check','True',array('checked'=>'checked')) }}
						@else
						{{ Form::checkbox('overall_value_check','True') }}
						@endif
					</div>

					 <div class="addmore-panel" id="add-more">
							@foreach($custom_fields as $customfield)
							<div class="input1" style="margin:5px 0;">
								<div class="arrow-img">
								<img src="{{URL::to('images/arrows1.jpg')}}" />
								</div> 
								<input type="hidden" value="{{$customfield->id}}" name="customhidden[]" />
								{{ Form::text('fieldnameedit[]',$customfield->field_name,array('placeholder'=>'Field Name','style'=>'width:100px;','Readonly'=>'True'))}}
								{{ Form::text('fieldvalueedit[]',$customfield->field_value,array('placeholder'=>'Value','style'=>'width:100px;','Readonly'=>'True'))}}								
								@if($customfield->field_check == "True")
								{{ Form::checkbox('chk_field[]','True','',array('checked'=>'checked','class'=>'chkbox','data-id'=>$customfield->id)) }}
								@else
								{{ Form::checkbox('chk_field[]','True','',array('class'=>'chkbox','data-id'=>$customfield->id)) }}
								@endif
								{{ Form::hidden('chk_hd_field[]',$customfield->field_check,array('class'=>'chkhdbox','data-id'=>'chkhd'.$customfield->id)) }}
							
								
							 </div>
							@endforeach
							
						</div>
                    
				</div>
			</div>
			
				<div class="modal-btn clearfix">
					{{ Form::submit('UPDATE') }}
					<a href="{{ URL::to('admin/devices/view/'.$deviceid) }}" style="padding:8px 75px; border-radius:5px; color:#fff; text-decoration:none; background:red;">CANCEL</a>
				</div>
			{{ Form::close() }}
		</div>
</div>
</div>
</div>
<script>
$(document).ready(function(){

	/*var max_fields      = 10; //maximum input boxes allowed
		var wrapper         = $("#add-more"); //Fields wrapper
		var add_button      = $(".plus-icon"); //Add button ID
		
		var x = 1; //initlal text box count
		$(add_button).click(function(e){ //on add input button click
		var fieldname = document.getElementById('fieldname').value;
			if(fieldname == "")
			{
				alert("Please enter fieldname");
			}
			else
			{
				e.preventDefault();
				if(x < max_fields){ //max input box allowed
					x++; //text box increment
					$(wrapper).append("<div class='input1'  style='margin-top:5px;'><div class='arrow-img'><img src='../../../images/arrows1.jpg' /></div><input placeholder='Field Name' name='fieldname[]' type='text' style='width:100px;'><input placeholder='Value' name='fieldvalue[]' type='text' style='width:100px;'><input name='chk_fieldname[]' type='checkbox' value='True' class='chkbox'><a href='javascript:void(0);' class='minus-icon remove_field'><img src='../../../images/minus.jpg' /></a><a href='javascript:void(0);' class='plus-icon'><img src='' /></a></div>"); //add input box
				}
			}
		});
		
		$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
			e.preventDefault(); $(this).parent('div').remove(); x--;
		})*/


		$('input[class="chkbox"]').on('change',function(e){
			e.preventDefault();
			var chkid = $(this).data('id');
			var isChecked = $(this).is(":checked");
            if (isChecked) {
                var chkhidden = $('[data-id="chkhd'+chkid+'"]').val('True');
			
            } else {
               var chkhidden = $('[data-id="chkhd'+chkid+'"]').val('False');
			
            }
		});

});
</script>

@stop

