@extends ('layout.default')
@section ('content')

<div class="content-area clearfix" style="margin:0 5%;">
	{{ Form::open(array('url' => 'admin/users/updateall')) }}
	<div class="top-links clearfix">
		<ul class="add-links">
			<li><a title="Add User" href="{{ URL::to('admin/users/add') }}" data-toggle="modal">Add User</a></li>
		</ul>
		<div  style="float:right;">
			{{ Form::submit('EDIT / SAVE',array('class'=>'btn_add_new','style'=>'width:100px; height:30px;')) }}
		</div>
	</div>
	<div class="table" >
		<table>
			<thead>
				<tr>
					<th>Name</th>
					<th>Email</th>
					<th>Role</th>
					<!-- @if(Auth::user()->roll == 1)
					<th>Organization</th>
					@endif -->
					<!-- <th>Projects</th> -->
					<th>Status</th>
					<th>Action</th>
				</tr>
                                @if(Auth::user()->roll == 1)
                                <tr>
					<td></td>
					<td></td>
					<td></td>
                                        <!-- <td style="text-align:center;white-space:nowrap">
                                            <label class="radio-inline"><input type='radio' name="org_value" value="clients.client_name" checked="checked"/>Client</label>
                                            <label class="radio-inline"><input type='radio' name="org_value" value="manufacturers.manufacturer_name" />Manufacturer</label>
                                            <label class="radio-inline"><input type='radio' name="org_value" value="users.organization" />Neptune PPA</label>

                                        </td>     -->                               
					<!-- <td></td> -->
					<td></td>
					<td style="width:85px;"></td>
				</tr>
                                @endif
				<tr>
					<td><input type='t`ext' class='search_text' data-field='users.name' /></td>
					<td><input type='text' class='search_text' data-field='users.email' /></td>
					<td><input type='text' class='search_text' data-field='roll.roll_name' /></td>
                    <!--  @if(Auth::user()->roll == 1)
                        <td>
                            <input type='text' class='search_text' id="organization_search" data-field='organization' />
                        </td>
					@endif -->
					<!-- <td><input type='text' class='search_text' data-field='projects.project_name'/></td> -->
					<td><input type='text' class='search_text' data-field='users.status'  style="width:85px;"/></td>
					<td style="width:85px;"></td>
				</tr>
			</thead>
			<tbody id='user_result'>
				@foreach($users as $user)
					<tr>
						<td>{{ $user->name }}</td>
						<td>{{ $user->email }}</td>
						<td>{{ $user->role->roll_name }}</td>
						<!-- @if(Auth::user()->roll == 1)
						<td>{{ $user->org_name}}</td>
						@endif -->
						<!-- <td>
                        
                        </td> -->
						<td>{{ Form:: hidden('hiddenid[]',$user->id)}}{{ Form::select('status[]',array('Enabled' => 'Enabled', 'Disabled' => 'Disabled'),$user->status) }}</td>
						<td><a href="{{ URL::to('admin/users/edit/'.$user->id) }}"><i class="fa fa-edit"></i></a> &nbsp;
                        @if($user->roll == 3)
                            <a href="{{ URL::to('admin/users/scorecard/'.$user->id) }}"><i class="fa fa-check-square-o" aria-hidden="true"></i></a>&nbsp;
                        @endif
                         <a href="{{ URL::to('admin/users/remove/'.$user->id) }}" onclick="return confirm(' Are you sure you want to delete user?');"><i class="fa fa-close"></i></a></td>
						
					</tr>
				@endforeach
			</tbody>


		</table>

	</div>
	{{ Form::close() }}
	<div class="bottom-count clearfix">
				{{$users->count()}} of {{$count}} displayed 
				{{Form::open(array('url'=>'admin/users','method'=>'get','id'=>'pagesize_form','style'=>'display:inline-block;'))}}
					{{Form::select('pagesize', array('10' => 'Show 10','15' => 'Show 15','20' => 'Show 20',$count=>'Show all'),$pagesize,array('id'=>'pagesize','onchange' => 'this.form.submit()'))}}
				{{Form::close()}}
		</div>
</div>
<script>
    $(document).ready(function () {
        $("input[name='org_value']").change(function() {
            var fieldName = $("input[name='org_value']:checked").val();
            if(fieldName == "users.organization"){
                $("#organization_search").prop('disabled', true);
                search_call(fieldName,'');
            }else{
                $("#organization_search").prop('disabled', false);
            }
        });
        $(".search_text").keyup(function () {
            var userrole = {{Auth::user()->roll}};
            var fieldName = $(this).data('field');
            if(fieldName == "organization"){
                fieldName = $("input[name='org_value']:checked").val();
            }
            
            var value = $(this).val();
            if(userrole == 1)
            {
                search_call(fieldName,value);
            }
            else
            {
                    $.ajax({
                            url: "{{ URL::to('admin/search_user')}}",
                            data: {
                                    fieldName: fieldName,
                                    value: value
                            },
                            success: function (data) {
                                    console.log(data);
                                    var html_data = '';
                                    if (data.status) 
                                    {
                                            $.each(data.value, function (i, item) {
                                                    // console.log(item);
                                                    if(item.status == 'Enabled')
                                                    {
                                                      var option = "<option value='Enabled' selected='selected'>Enabled</option><option value='Disabled'>Disabled</option>";
                                                    }
                                                    else
                                                    {
                                                      var option = "<option value='Enabled' >Enabled</option><option value='Disabled' selected='selected'>Disabled</option>";
                                                    }
                                                     var project = (item.project_name != null) ? item.project_name : '';
                                                    html_data +="<tr><td>"+item.name+"</td><td>"+item.email+ "</td><td>"+item.roll_name+"</td><td>"+project+"</td><td><input type='hidden' value='"+item.id+"' name='hiddenid[]' /><select name='status[]'>"+option+"</select></td><td><a href=users/edit/"+item.id+"><i class='fa fa-edit'></i></a>&nbsp; <a href=users/remove/"+item.id+" onclick=return&nbsp;confirm('Are&nbsp;you&nbsp;sure&nbsp;you&nbsp;want&nbsp;to&nbsp;delete&nbsp;user?');><i class='fa fa-close'></i></a></td></tr>";
                                            });
                                    } 
                                    else 
                                    {
                                            html_data = "<tr> <td colspan='6' style='text-align:center;'> " + data.value + " </td> </tr>"
                                    }

                                    $("#user_result").html(html_data);

                            }

                    });

            }

        });

    });
    
    function search_call(fieldName,value){
        $.ajax({
            url: "{{ URL::to('admin/search_user')}}",
            data: {
                    fieldName: fieldName,
                    value: value
            },
            success: function (data) {
                    console.log(data);
                    var html_data = '';
                    if (data.status) 
                    {
                            $.each(data.value, function (i, item) {
                                    console.log(item);
                                    if(item.status == 'Enabled')
                                    {
                                      var option = "<option value='Enabled' selected='selected'>Enabled</option><option value='Disabled'>Disabled</option>";
                                    }
                                    else
                                    {
                                      var option = "<option value='Enabled' >Enabled</option><option value='Disabled' selected='selected'>Disabled</option>";
                                    }
                                    var org = (item.org_name != null) ? item.org_name : '';
                                    var project = (item.project_name != null) ? item.project_name : '';
                                    html_data +="<tr><td>"+item.name+"</td><td>"+item.email+ "</td><td>"+item.roll_name+"</td><td>"+ org +"</td><td>"+project+"</td><td><input type='hidden' value='"+item.id+"' name='hiddenid[]' /><select name='status[]'>"+option+"</select></td><td><a href=users/edit/"+item.id+"><i class='fa fa-edit'></i></a>&nbsp; <a href=users/remove/"+item.id+" onclick=return&nbsp;confirm('Are&nbsp;you&nbsp;sure&nbsp;you&nbsp;want&nbsp;to&nbsp;delete&nbsp;user?');><i class='fa fa-close'></i></a></td></tr>";
                            });
                    } 
                    else 
                    {
                            html_data = "<tr> <td colspan='7' style='text-align:center;'> " + data.value + " </td> </tr>"
                    }

                    $("#user_result").html(html_data);

            }

        });
    }
</script>
@stop       