@extends ('layout.default')
@section ('content')
    <div class="add_new">
        <div class="box-center1">
            <div class="add_new_box">

                <div class="col-md-12 col-lg-12 modal-box" style="margin-top:10px;">
                    <a title="" href="{{ URL::to('admin/devices/view/'.$data['deviceId']) }}"
                       class="pull-right" data-toggle="modal">X</a>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li style="color:red; margin:5px;">{{ $error }}</li>
                        @endforeach
                    </ul>
                    {{ Form::open(array('url' => 'admin/devices/devicesurvey/create','method'=>'POST','files'=>true)) }}
                    <div class="content-area clearfix" style="padding:30px 0px 30px 0px;">
                        <div class="col-md-12 col-lg-12 modal-box" align="center">
                            <div class="input1">
                                {{ Form::hidden('device_id',$data['deviceId'])}}
                            </div>
                            <div class="input1">
                                {{Form::label('label2', 'Select Client')}}
                                {{ Form::select('clientId',$client_name,'',array('id'=>'clientname')) }}
                            </div>
                            <div class="input1">
                                {{Form::label('label3', 'Question 1')}}
                                {{ Form::text('que_1',$data['que_1'],array('placeholder'=>'Question 1'))}}

                                @if($data['que_1_check'] == "True")
                                    {{ Form::checkbox('que_1_check','True',array('checked'=>'checked')) }}
                                @else
                                    {{ Form::checkbox('que_1_check','True') }}
                                @endif

                            </div>
                            <div class="input1">
                                {{Form::label('label4', 'Question 2')}}
                                {{ Form::text('que_2',$data['que_2'],array('placeholder'=>'Question 2'))}}

                                @if($data['que_2_check'] == "True")
                                    {{ Form::checkbox('que_2_check','True',array('checked'=>'checked')) }}
                                @else
                                    {{ Form::checkbox('que_2_check','True') }}
                                @endif
                            </div>
                            <div class="input1">
                                {{Form::label('label5', 'Question 3')}}
                                {{ Form::text('que_3',$data['que_3'],array('placeholder'=>'Question 3'))}}

                                @if($data['que_3_check'] == "True")
                                    {{ Form::checkbox('que_3_check','True',array('checked'=>'checked')) }}
                                @else
                                    {{ Form::checkbox('que_3_check','True') }}
                                @endif
                            </div>
                            <div class="input1">
                                {{Form::label('label17', 'Question 4')}}
                                {{ Form::text('que_4',$data['que_4'],array('placeholder'=>'Question 4'))}}
                                @if($data['que_4_check'] == "True")
                                    {{ Form::checkbox('que_4_check','True',array('checked'=>'checked')) }}
                                @else
                                    {{ Form::checkbox('que_4_check','True') }}
                                @endif

                            </div>
                            <div class="input1">
                                {{Form::label('label6', 'Question 5')}}
                                {{ Form::text('que_5',$data['que_5'],array('placeholder'=>'Question 5'))}}
                                @if($data['que_5_check'] == "True")
                                    {{ Form::checkbox('que_5_check','True',array('checked'=>'checked')) }}
                                @else
                                    {{ Form::checkbox('que_5_check','True') }}
                                @endif

                            </div>
                            <div class="input1">
                                {{Form::label('label7', 'Question 6')}}
                                {{ Form::text('que_6',$data['que_6'],array('placeholder'=>'Question 6'))}}
                                @if($data['que_6_check'] == "True")
                                    {{ Form::checkbox('que_6_check','True',array('checked'=>'checked')) }}
                                @else
                                    {{ Form::checkbox('que_6_check','True') }}
                                @endif

                            </div>
                            <div class="input1">
                                {{Form::label('label7', 'Question 7')}}
                                {{ Form::text('que_7',$data['que_7'],array('placeholder'=>'Question 7'))}}
                                @if($data['que_7_check'] == "True")
                                    {{ Form::checkbox('que_7_check','True',array('checked'=>'checked')) }}
                                @else
                                    {{ Form::checkbox('que_7_check','True') }}
                                @endif

                            </div>
                            <div class="input1">
                                {{Form::label('label7', 'Question 8')}}
                                {{ Form::text('que_8',$data['que_8'],array('placeholder'=>'Question 8'))}}
                                @if($data['que_8_check'] == "True")
                                    {{ Form::checkbox('que_8_check','True',array('checked'=>'checked')) }}
                                @else
                                    {{ Form::checkbox('que_8_check','True') }}
                                @endif
                            </div>
                            <div class="input1">
                                {{Form::label('label7','Status')}}
                                {{Form::select('status', array(''=>'Status','True' => 'Active','False'=>'DeActive'), $data['status'], array('class' => 'name'))}}
                            </div>

                        </div>

                    </div>

                    <div class="modal-btn clearfix">
                        {{ Form::submit('SAVE') }}
                        <a href="{{ URL::to('admin/devices/view/'.$data['deviceId']) }}"
                           style="padding:8px 75px; border-radius:5px; color:#fff; text-decoration:none; background:red;">CANCEL</a>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            /*var max_fields      = 10; //maximum input boxes allowed
             var wrapper         = $("#add-more"); //Fields wrapper
             var add_button      = $(".plus-icon"); //Add button ID

             var x = 1; //initlal text box count
             $(add_button).click(function(e){ //on add input button click
             var fieldname = document.getElementById('fieldname').value;
             if(fieldname == "")
             {
             alert("Please enter fieldname");
             }
             else
             {
             e.preventDefault();
             if(x < max_fields){ //max input box allowed
             x++; //text box increment
             $(wrapper).append("<div class='input1'  style='margin-top:5px;'><div class='arrow-img'><img src='../../../images/arrows1.jpg' /></div><input placeholder='Field Name' name='fieldname[]' id='fieldname' type='text' style='width:100px;'><input placeholder='Value' name='fieldvalue[]' type='text' id='fieldvalue' style='width:100px;'><input name='chk_fieldname[]' type='checkbox' value='True' class='chkbox'><a href='javascript:void(0);' class='minus-icon remove_field'><img src='../../../images/minus.jpg' /></a><a href='javascript:void(0);' class='plus-icon'><img src='' /></a></div>"); //add input box
             }
             }
             });

             $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
             e.preventDefault(); $(this).parent('div').remove(); x--;
             })*/

            $('input[class="chkbox"]').on('change', function (e) {
                e.preventDefault();
                var chkid = $(this).data('id');
                var isChecked = $(this).is(":checked");
                if (isChecked) {
                    var chkhidden = $('[data-id="chkhd' + chkid + '"]').val('True');

                } else {
                    var chkhidden = $('[data-id="chkhd' + chkid + '"]').val('False');

                }
            });

        });
    </script>

@stop
