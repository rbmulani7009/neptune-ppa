@extends ('layout.default')
@section ('content')
<div class="add_new">
<div class="box-center1">
<div class="add_new_box">
		
<div class="col-md-12 col-lg-12 modal-box" style="margin-top:10px;">
 <a title="" href="{{ URL::to('admin/devices/view/'.$customContact['deviceId']) }}" class="pull-right" data-toggle="modal" >X</a>
			<ul>
                @foreach($errors->all() as $error)
                <li style="color:red; margin:5px;">{{ $error }}</li>
                @endforeach
            </ul>
			{{ Form::open(array('url' => 'admin/devices/customcontact/update/'.$customContact['id'],'method'=>'PUT','files'=>true)) }}
			<div class="content-area clearfix"  style="padding:30px 0px 30px 0px;">
				<div class="col-md-12 col-lg-12 modal-box" align="center">
					<div class="input1">
						{{ Form::hidden('deviceId',$customContact['deviceId'])}}
					</div>
					<div class="input1">
						{{Form::label('label2', 'Select Client')}}
						{{ Form::select('clientId', $client_name,$customContact['clientId'],array('id'=>'clientname')) }}
					</div>
					<div class="input1">
						{{Form::label('label2', 'Order Email')}}
						{{ Form::select('order_email', array(''=>'Select Order Email'),'',array('id'=>'ordermail')) }}
					</div>
					<br>
					<div class="input1">
						{{Form::label('label4', 'CC #1 Email')}}
						{{ Form::text('cc1',$customContact['cc1'],array('placeholder'=>'Email Address'))}}
					</div>
					<div class="input1">
						{{Form::label('label5', 'CC #2 Email')}}
						{{ Form::text('cc2',$customContact['cc2'],array('placeholder'=>'Email Address'))}}
					</div>
					<div class="input1">
						{{Form::label('label17', 'CC #3 Email')}}
						{{ Form::text('cc3',$customContact['cc3'],array('placeholder'=>'Email Address'))}}
					</div>
                    <div class="input1">
						{{Form::label('label6', 'CC #4 Email')}}
						{{ Form::text('cc4',$customContact['cc4'],array('placeholder'=>'Email Address'))}}
					</div>
					<div class="input1">
						{{Form::label('label7', 'CC #5 Email')}}
						{{ Form::text('cc5',$customContact['cc5'],array('placeholder'=>'Email Address'))}}
					</div>
					
					<br>
					<div class="input1">
						{{Form::label('label7', 'Subject')}}
						{{ Form::text('subject',$customContact['subject'],array('placeholder'=>'Default Subject Line Editable '))}}
					</div>
					
				</div>
				
			</div>
			
				<div class="modal-btn clearfix">
					{{ Form::submit('SAVE') }}
					<a href="{{ URL::to('admin/devices/view/'.$customContact['deviceId']) }}" style="padding:8px 75px; border-radius:5px; color:#fff; text-decoration:none; background:red;">CANCEL</a>
				</div>
			{{ Form::close() }}
		</div>
</div>
</div>
</div>
<script>
$(document).ready(function(){

	    $('#clientname').change(function () {
                var userId = document.getElementById("clientname").value;
               
                 $.ajax({
                    url: "{{ URL::to('admin/devices/customcontact/getordermail')}}",
                    type: "POST",
                    data: {
                    	 _token: "{{ csrf_token() }}",
                        userId: userId
                       
                    },
                    success: function (data)
                    {
                      
                        var html_data = '';
                        if (data.status) {
                            html_data += "<option value=''>Select Order Email</option>";
                            $.each(data.value, function (i, item) {
                                
                                html_data += "<option value=" + item.id + ">" + item.email + "</option>";

                            });
                        } else
                        {
                            html_data = "<option value=''>Select Order Email</option>";
                        }
                       
                        $("#ordermail").html(html_data);

                    }

        			});
        });

        $(function(){
        	 var userId = document.getElementById("clientname").value;

        	 $.ajax({
                    url: "{{ URL::to('admin/devices/customcontact/getordermail')}}",
                    type: "POST",
                    data: {
                    	 _token: "{{ csrf_token() }}",
                        userId: userId
                       
                    },
                    success: function (data)
                    {
                      	var ordermail = {{$customContact->order_email}};
                        var html_data = '';
                        if (data.status) {
                            
                            $.each(data.value, function (i, item) {
                                	var is_selected = (item.id == ordermail) ? "selected":"";
                                html_data += "<option value=" + item.id + ">" + item.email + "</option>";

                            });
                        } else
                        {
                            html_data = "<option value=''>Select Order Email</option>";
                        }
                       
                        $("#ordermail").html(html_data);

                    }

        			});
        });


});
</script>

@stop

