@extends ('layout.default')
@section ('content')
<div class="add_new">
<div class="add_new_box">
		
        <div class="col-md-12 col-lg-12 modal-box">
        <a title="edit Scorecard" href="{{ URL::to('admin/users/scorecard/'.$score['userId']) }}" class="pull-right" data-toggle="modal">X</a>    
            <h4> Edit Scorecard </h4>
            {{ Form::open(array('url' => 'admin/users/scorecard/update/'.$score['id'],'method'=>'PUT','files'=>'true') )}}
            <ul>
                @foreach($errors->all() as $error)
                <li style="color:red; margin:5px;">{{ $error }}</li>
                @endforeach
            </ul>
            {{form::hidden('userId',$score['userId'])}}
            <div class="input1">
                {{Form::select('monthId',$month, $score['monthId'], array('class' => 'name'))}}
            </div>
            <div class="input1">
                {{ Form::selectYear('year', 2010, 2030,$score['year'])}}
            </div>
            &nbsp;
            <div class="input1">
               <div class="input1">
               
                    {{ Form::file('scorecardImage[]',array('id'=>'fp_upload','multiple'=>'true'))}} 
               
                </div>
                <div>
                    @foreach($scoreImage as $image)
                        <p><img src="{{URL::to('public/'.$image['scorecardImage'])}}" width="80px" height="70px"></p>
                    @endforeach
                </div>
            </div>

            <div>
                {{ Form::submit('UPDATE',array('class'=>'btn_add_new')) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>	



@stop       