@extends ('layout.default')
@section ('content')

<div class="content-area clearfix">
	

	<h3 style="text-align: center;">Scorecard: {{$user['name']}} @foreach($user->userclients as $row)| {{$row->clientname->client_name}}  @endforeach</h3>
	

	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<ul><li><h3 class="pull-left">{{$score->months->month}} {{$score['year']}}</h3></li></ul>
				
					<ul class="add-links pull-right">
							@if(Auth::user()->roll == 1)
							<li><a title="Add Device" href="{{ URL::to('admin/users/scorecard/'.$user['id']) }}">View</a></li>
							
							<li><a href="#" id="deleteimage" onclick="return confirm(' Are you sure you want to import device?');">Delete</a>
			                </li>
							<li><a href="{{ URL::to('admin/users/scorecard/'.$user['id'])}}"  id="deviceexport">Close</a></li>
							
							@endif
							
					</ul>
					
				</div>
				<hr>
				<div class="col-sm-12">
					{{ Form::open(array('url' => 'admin/users/scorecard/image/remove/'.$score['id'],'method'=>'POST','files'=>'true','id'=>'imageRemove') )}}
					@foreach($scoreImage as $row)
					<div class="col-sm-3 scorecardimage "><input type="checkbox" name="chekImage[]" class="scorecard chk_Image" value="{{$row->id}}"><img src="{{URL::to('public/'.$row->scorecardImage)}}" class="scimage"></div>
					@endforeach
					{{ Form::close() }}
				</div>
			</div>	
		</div>
		<div class="bottom-count clearfix">

		</div>



	</div>
<script type="text/javascript">
	$(document).ready(function(){
    

    $("#deleteimage").click(function(){
        
        if($(".chk_Image:checked").length == 0)
        {
            
            alert("Please select Image And Delete");
            return false;
        }
        else
        {
            
            $("#imageRemove").submit();
            return true;
        }
    });
});

</script>

	@stop