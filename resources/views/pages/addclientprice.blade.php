@extends ('layout.default')
@section ('content')
<div class="add_new">
<div class="box-center1">
<div class="add_new_box">
		
<div class="col-md-12 col-lg-12 modal-box" style="margin-top:10px;">
 <a title="" href="{{ URL::to('admin/devices/view/'.$deviceid) }}" class="pull-right" data-toggle="modal" >X</a>		<h3 style="text-align:center;">Add Client Price</h3>
			<ul>
                @foreach($errors->all() as $error)
                <li style="color:red; margin:5px;">{{ $error }}</li>
                @endforeach
            </ul>
			{{ Form::open(array('url' => 'admin/devices/clientpricecreate','method'=>'POST','files'=>true)) }}
			<div class="content-area clearfix"  style="padding:30px 0px 30px 0px;">
				<div class="col-md-6 col-lg-6 modal-box" style="border-right:solid 1px #ccc;">
					<div class="input1">
						{{ Form::hidden('device_id',$deviceid)}}
					</div>
					<div class="input1">
						{{Form::label('label2', 'Select Client')}}
						{{ Form::select('manufacturer_name', $manufacturer,'',array('id'=>'clientname')) }}
					</div>
					<div class="input1">
						{{Form::label('label3', 'Unit Cost')}}
						{{ Form::text('unit_cost',null,array('placeholder'=>'Unit Cost'))}}
                                                {{ Form::checkbox('unit_cost_check','True') }}
					</div>
					<div class="input1">
						{{Form::label('label4', 'Bulk Unit Cost%')}}
						{{ Form::text('bulk_unit_cost',null,array('placeholder'=>'Bulk Unit Cost%'))}}
						{{ Form::checkbox('chk_bulk_unit_cost','True','',array('id'=>'bulk_unit_cost_check')) }}
					</div>
					<div class="input1">
						{{Form::label('label5', 'Unit Bulk')}}
						{{ Form::text('bulk',null,array('placeholder'=>'Bulk'))}}
						{{ Form::checkbox('chk_bulk','True') }}
					</div>
					<div class="input1">
						{{Form::label('label17', 'CCO Discount')}}
						{{ Form::text('cco_discount',null,array('placeholder'=>'CCO Discount'))}}
						{{ Form::checkbox('chk_cco_discount','True','',array('id'=>'chk_cco_discount')) }}
						
					</div>
                    <div class="input1">
						{{Form::label('label6', 'CCO Discount %')}}
						{{ Form::text('cco',null,array('placeholder'=>'CCO Discount%'))}}
						{{ Form::checkbox('chk_cco','True','',array('id'=>'chk_cco')) }}
						
					</div>
					<div class="input1">
						{{Form::label('label7', 'Unit Repless Discount')}}
						{{ Form::text('unit_rep_cost',null,array('placeholder'=>'Unit Repless Discount'))}}
						{{ Form::checkbox('chk_unit_rep_cost','True','',array('id'=>'unit_rep_cost_check')) }}
						
					</div>
					<div class="input1">
						{{Form::label('label8', 'Unit Repless Discount %')}}
						{{ Form::text('unit_rep_discount',null,array('placeholder'=>'Unit Repless Discount%'))}}
						{{ Form::checkbox('chk_unit_rep_discount','True','',array('id'=>'unit_rep_discount_check')) }}
					
					</div>
					
				</div>
				<div class="col-md-6 col-lg-6 modal-box">
					<div class="input1">
						{{Form::label('label9', 'System Cost')}}
						{{ Form::text('system_cost',null,array('placeholder'=>'System Cost'))}}
                                                {{ Form::checkbox('system_cost_check','True') }}
						
					</div>
                    <div class="input1">
						{{Form::label('label9', 'System Bulk')}}
						{{ Form::text('system_bulk',null,array('placeholder'=>'System Bulk'))}}
                        {{ Form::checkbox('system_bulk_check','True') }}
						
					</div>
					<div class="input1">
						{{Form::label('label12', 'Bulk System Cost%')}}
						{{ Form::text('bulk_system_cost',null,array('placeholder'=>'Bulk System Cost%'))}}
						{{ Form::checkbox('chk_bulk_system_cost','True','',array('id'=>'bulk_system_cost_check')) }}
						
					</div>
					<div class="input1">
						{{Form::label('label13', 'System Repless Discount')}}
						{{ Form::text('system_rep_cost',null,array('placeholder'=>'System Repless Discount'))}}
						{{ Form::checkbox('chk_system_rep_cost','True','',array('id'=>'system_rep_cost_check')) }}
					</div>
					<div class="input1">
						{{Form::label('label14', 'System Repless Discount%')}}
						{{ Form::text('system_rep_discount',null,array('placeholder'=>'System Repless Discount%'))}}
						{{ Form::checkbox('chk_system_rep_discount','True','',array('id'=>'system_rep_discount_check')) }}
					</div>
					<div class="input1">
						{{Form::label('label15', 'Reimbursement')}}
						{{ Form::text('reimbursement',null,array('placeholder'=>'Reimbursement'))}}
						{{ Form::checkbox('chk_reimbursement','True') }}
					</div>
					<div class="input1">
						{{Form::label('label16', 'Order Email')}}
						{{ Form::select('order_email',array('0'=>'Select Order Email'),'',array('id'=>'orderemail')) }}
						
					</div>
				</div>
			</div>
			
				<div class="modal-btn clearfix">
					{{ Form::submit('SAVE') }}
					<a href="{{ URL::to('admin/devices/view/'.$deviceid) }}" style="padding:8px 75px; border-radius:5px; color:#fff; text-decoration:none; background:red;">CANCEL</a>
				</div>
			{{ Form::close() }}
		</div>
</div>
</div>
</div>
<script>
$(document).ready(function(){
	
	
	
	$('#chk_cco_discount').change(function() {
		var chk_cco = $('#chk_cco').prop('checked');
		if(chk_cco) 
		{
			alert('You can select only one in CCO Discount or CCO Discount%');
			$(this).prop("checked",false);
		} 
		else 
		{
			$(this).attr("checked");
        }
          
    });
	
	$('#chk_cco').change(function() {
		var chk_cco_discount = $('#chk_cco_discount').prop('checked');
		if(chk_cco_discount) 
		{
			alert('You can select only one in CCO Discount or CCO Discount%');
			$(this).prop("checked",false);
		} 
		else 
		{
			$(this).attr("checked");
        }
          
    });
	
	
	
	$('#unit_rep_discount_check').change(function() {
		var unit_rep_cost_check = $('#unit_rep_cost_check').prop('checked');
		if(unit_rep_cost_check) 
		{
			alert('You can select only one in Unit Repless Discount or Unit Repless Discount%');
			$(this).prop("checked",false);
		} 
		else 
		{
			$(this).attr("checked");
        }
          
    });
	
	$('#unit_rep_cost_check').change(function(){
	
		var unit_rep_discount_check = $('#unit_rep_discount_check').prop('checked');
		if(unit_rep_discount_check) 
		{
			alert('You can select only one in Unit Repless Discount or Unit Repless Discount%');
			$(this).prop("checked",false);
		} 
		else 
		{
			$(this).attr("checked");
        }
	
	});
	
	$('#system_rep_discount_check').change(function() {
		var system_rep_cost_check = $('#system_rep_cost_check').prop('checked');
		if(system_rep_cost_check) 
		{
			alert('You can select only one in System Repless Discount or System Repless Discount%');
			$(this).prop("checked",false);
		} 
		else 
		{
			$(this).attr("checked");
        }
          
    });
	
	$('#system_rep_cost_check').change(function() {
		var system_rep_discount_check = $('#system_rep_discount_check').prop('checked');
		if(system_rep_discount_check) 
		{
			alert('You can select only one in System Repless Discount or System Repless Discount%');
			$(this).prop("checked",false);
		} 
		else 
		{
			$(this).attr("checked");
        }
          
    });
	
	
	$('#clientname').change(function(){
			var clientid = $('#clientname').val();
		
			$.ajax({
                url: "{{ URL::to('admin/getorderemail')}}",
                data: {
                    clientid: clientid
                },
                success: function (data) 
				{
					var html_data = '';
					 if (data.status) {
					$.each(data.value, function (i, item) {
						console.log(item);
                            html_data += "<option value="+item.id+">"+item.email+"</option>";

					});
					 }
					 else
					 {
						 html_data = "<option value=0>Select Oreder Email</option>";
					 }
					console.log(html_data);
                    $("#orderemail").html(html_data);

                }

            });
		});
});
</script>

@stop

