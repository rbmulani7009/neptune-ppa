@extends('layouts.userlogin')

@section('content')
	<div style="float:right; padding:20px; font-weight:bold;">
		<a href="{{ URL::to('logout') }}" style="color:#fff;">Logout</a>
	</div>
	<div class="container menu-panel">
		<div class="header">
		<a class="info-icon" href="#" title="Info">Info</a>
			<h1><img src="{{ URL::asset('frontend/images/logo.png') }}" /></h1>
		</div>
        @if($nodevice == "False")
		<ul class="menu-list">
			<li class="col-xs-6 col-sm-6 col-md-6 col-lg-6 changeout">
				<a href="{{ URL::asset('changeout/mainmenu') }}">
					<div class="menu-img">
						<img src="{{ URL::asset('frontend/images/changeout.png') }}" />
					</div>
					<span>CHANGE OUT</span>
				</a>
			</li>
			<li class="col-xs-6 col-sm-6 col-md-6 col-lg-6 newdevice">
				<a href="{{ URL::asset('newdevice/mainmenu') }}">
					<div class="menu-img">
						<img src="{{ URL::asset('frontend/images/newdevice.png') }}" />
					</div>	
					<span>NEW DEVICE</span>
				</a>
			</li
		</ul>
        @else
        <p class="alert alert-danger">There is no device available!</p>
        @endif
	</div>
@endsection