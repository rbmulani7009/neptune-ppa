@extends('layouts.userlogin')

@section('content')
<script src="{{ URL::asset('frontend/js/jquery-ui.js') }}"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.2/jquery.ui.touch-punch.min.js"></script>
<script>
  $( function() {
    $( "#sortable" ).sortable();
   // $( "#sortable" ).disableSelection();
  } );
  </script>
	<div style="float:right; padding:20px; font-weight:bold;">
		<a href="{{ URL::to('logout') }}" style="color:#fff;">Logout</a>
	</div>
	<div class="container menu-panel">
		<div class="header">
                    <a class="info-icon" href="#" title="Info">Info</a>
			<h1> <a href="{{URL::to('menu')}}" ><img src="{{ URL::asset('frontend/images/logo.png') }}" /></a></h1>
		</div>
        <!--@if(Session::has('message'))
        <p class="alert alert-danger">{{ Session::get('message') }}</p>
        @endif-->
        @if(isset($categories))
		<ul class="mainmenu-list" id="sortable">
			@foreach($categories as $category)
			<li class="col-xs-6 col-sm-6 col-md-6 col-lg-6 ui-state-default">
				<a href="devices/{{$category->id}}" title="{{ $category->category_name}}">{{ $category->category_name}}</a>
			</li>
			@endforeach
		</ul>
		@else
        <p class="alert alert-danger">There is no device available!</p>
        @endif
	</div>
	
@endsection