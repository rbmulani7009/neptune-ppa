@extends('layouts.userlogin')

@section('content')
    <div id="mobile-landscape">
        <div class="top-header clearfix">
            <div class="col-xs-4 col-sm-4 col-md-4 humberhan-icon">
        
                    <a class="humber-icon" href="{{ URL::to('newdevice/mainmenu') }}">
              
                    <a class="humber-icon" href="{{ URL::to('changeout/mainmenu') }}">
                 
                    <img src="{{ URL::asset('frontend/images/menu.png') }}" /></a>
                    <span class="clientname">{{Auth::user()->name}} | xyz</span>
                    <span class="updateddates">Update: {{Auth::user()->updated_at}}</span>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 title-text">
                <span>Physician | {{Auth::user()->name}}</span>
            </div>

            <div class="col-xs-4 col-sm-4 col-md-4 inner-logo">
                <a href="{{URL::to('menu')}}" id="testtest"><img src="{{ URL::asset('frontend/images/min-logo.png') }}" alt="" /></a>
            </div>			
        </div>
        
        
    </div>
    <div class="row">
        
        <center>
            <div class="col-sm-12 scorecardfront">
                <div class="">
                    <div class="scorecarddiv">
                        {{Form::select('name', $year,'', array('class' => 'name','id'=>'year'))}}
                    </div>

                    <div class="scorecarddiv">
                        {{Form::select('name',array(''=>'Select Month'), '', array('class' => 'name','id'=>'month'))}}
                    </div>        
                </div>
            </div>
        </center>
    </div>
     
<div id="warning-message">
	<img src="{{ URL::to('images/Neptune-bg-landscape.png')}}" />
</div>



<script>
$(document).ready(function(){
      $(document).on("change",'#year',function (e) {

                var scoreId = document.getElementById("year").value;

                 $.ajax({
                    url: "{{ URL::to('scorecard/getmonth')}}",
                    type: "POST",
                    data: {
                         _token: "{{ csrf_token() }}",
                        scoreId: scoreId
                    },
                    success: function (data)
                    {
                      
                        var html_data = '';
                        if (data.status) {
                            html_data += "<option value=''>Select Month</option>";
                            $.each(data.value, function (i, item) {
                                
                                html_data += "<option value=" + item.id + ">" + item.month + "</option>";

                            });
                        } else
                        {
                            html_data = "<option value=''>Select Month</option>";
                        }
                       
                        $("#month").html(html_data);

                    }

                    });
        });

        $(document).on("change",'#month',function () {

              var scoreId = document.getElementById("month").value;
              console.log(scoreId);
              
                var url = '{{URL::to("scorecard/scorecardimage/")}}' ;
                var path = url +"/"+scoreId;
                
                window.location.href = path;
             
        });

});
</script>

@endsection

