@extends('layouts.userlogin')

@section('content')
<div id="mobile-landscape">
<div class="top-header clearfix">
<div class="col-xs-4 col-sm-4 col-md-4 humberhan-icon">
	<a class="humber-icon" href="{{ $devicetype == "New Device" ? URL::to('newdevice/mainmenu') : URL::to('changeout/mainmenu') }}"><img src="{{ URL::asset('frontend/images/menu.png') }}" /></a>
	<span class="clientname">{{$clientname}} | {{$devicetype}}</span>
	<span class="updateddates">Update: {{$updatedate}}</span>
</div>
<div class="col-xs-4 col-sm-4 col-md-4 title-text">
<span>{{$categoryname}}</span>
</div>
<!--<div class="logout-link">
                <a href="{{ URL::to('logout') }}" style="color:#fff;">Logout</a>
            </div>-->
<div class="col-xs-4 col-sm-4 col-md-4 inner-logo">
	<a href="{{URL::to('menu')}}">
            <img src="{{ URL::asset('frontend/images/min-logo.png') }}" alt="" />
        </a>
</div>			
</div>
<div class="optional-panel clearfix">
	<div class="radio entry-level" id="radiodiv1">
	<label for="entry-level" class="nolabelbg">ENTRY LEVEL TECHNOLOGIES</label> <input type="radio"  name="level" id="entry-level" /><label for="entry-level"></label>
	</div>
	<div class="radio" id="radiodiv2">
		 <input type="radio" id="advanced-level" name="level" /><label for="advanced-level">ADVANCED LEVEL TECHNOLOGIES</label>
	</div>
	
</div>
<div class="main clearfix">
	<div class="compared-left col-xs-7 col-sm-7 col-md-7 compare-productleft">	
		<div class="compared-list col-xs-4 col-sm-4 col-md-4">
			<div class="compared-checkbox clearfix">
			  <div class="compared-inner">	
				<div class="checkbox">
					<input type="checkbox" checked="checked" disabled = "True"/> <label></label>				
				</div>
					<img src="{{ URL::asset('frontend/images/arrows.png') }}" />
				<div class="checkbox">
					<input type="checkbox" checked="checked" disabled = "True"/> <label></label>				
				</div>
			</div>	
		   </div>
		   <div class="reload-img">
					<a href="{{ URL::previous() }}"><img src="{{ URL::asset('frontend/images/refresh.png') }}" /></a>
			</div>
			<ul class="compared-modal">
				<li class="active"><a href="#">Model</a></li>
				<li><a href="#">{{ $devicetype == 'New Device' ? 'System Cost' : 'Unit Cost' }}</a></li>
                @if($devicetype == "Changeout")
                @if($cco_discount_option == "True")
				<li><a href="#">CCO</a></li>
				@endif
                @endif
                @if($repless_discount_option == "True")
                <li><a href="#">Repless</a></li>
                @endif
                @if($bulk_option == "True")
                <li><a href="#">Bulk</a></li>
                @endif
				<!--<li><a href="#">REIMB</a></li>
                @if($exclusive_option == "True")
				<li><a href="#">Exclusives</a></li>
                @endif
                @if($longevity_option == "True")
				<li><a href="#">Longevity</a></li>
				@endif
                @if($shock_option == "True")
                <li><a href="#">Shock/CT</a></li>
				@endif
                @if($size_option == "True")
                <li><a href="#">Size</a></li>
				@endif
                @if($bulk_option == "True")
                <li><a href="#"><b>Bulk</b></a></li>
				@endif
                @if($siteinfo_option == "True")
                <li><a href="#">Site Info</a></li>
				@endif
                <li class="active"><a href="#">Overall Value</a></li>-->
			</ul>	
			</div>	
			@foreach($first_product as $productdetail)
		<div class="comparision-list col-xs-4 col-sm-4 col-md-4">
			<div class="comparision-inner">
				<div class="clearfix">
				<div class="col-xs-9 col-sm-9 col-md-9 comapred-logo">
					<img src="upload/{{$productdetail->manufacturer_logo}}" />
				</div>				
				<div class="checkbox">
					<input type="checkbox" checked="checked" disabled="disabled"/> <label></label>
				</div>
				</div>
					<img class="comapred-img" src="upload/{{$productdetail->device_image}}" />
					<h2>{{$productdetail->device_name}}</h2>
					 <ul>
								<li class="system-value">
                                ${{ $devicetype == 'New Device' ? number_format($productdetail->system_cost,2) : number_format($productdetail->unit_cost,2) }}
								 </li>
                                 @if($devicetype == 'Changeout')
                                 @if($cco_discount_option == "True")
                                <li class="system-value">
								{{$productdetail->cco_discount == "-" ? '-' : '$'. number_format($productdetail->cco_discount,2)}}
                                </li>
                                @endif
                                @endif
                                @if($repless_discount_option == "True")
                                <li class="system-value">
                                {{ $devicetype == 'New Device' 
                                	?( $productdetail->system_repless_cost == "-" 
                                    	? "-"
                                        :'$'.number_format($productdetail->system_repless_cost,2)
                                     ) 
                                    : ( $productdetail->unit_repless_cost == "-" ? "-"
                                    	:'$'.number_format($productdetail->unit_repless_cost,2)
                                      )
                                  }}
                                </li>
                                @endif
                                @if($bulk_option == "True")
		                        <li class="system-value">
		                                @if($devicetype == 'New Device')
										@if($productdetail->system_bulk_check == "True")
										  	{{  $productdetail->remain_bulk > 0 
		                                	?  $productdetail->remain_bulk
		                                    : "0"
		                                    }}
		                                    @else
		                                    -
	                                 	@endif
		                                @elseif($devicetype == 'Changeout')
											@if($productdetail->bulk_check == "True")
										  	{{  $productdetail->remain_bulk > 0 
		                                	?  $productdetail->remain_bulk
		                                    : "0"
		                                    }}
		                                    @else
		                                    -
		                                	@endif
										@endif
		                        </li>
		                        @endif
                               <!-- <li class="green-color">
								{{ $productdetail->reimbursement_check == "True" 
                                	? '$'.number_format($productdetail->reimbursement,2)
                                    : '-'
                                }}
								</li> -->
								@if($exclusive_option == "True")
		                        <li class="col-xs-6 col-sm-6 col-md-6 listlabel">Exclusives:</li>
		                        <li class="col-xs-6 col-sm-6 col-md-6 listvalue">
		                                @if($productdetail->exclusive_check == "True")
		                                {{$productdetail->exclusive}}
		                                @else
		                                -
		                                @endif
		                        </li>
		                        @endif
                                
                                @if($longevity_option == "True")
		                        <li class="col-xs-6 col-sm-6 col-md-6 listlabel">Longevity:</li>
		                        <li class="col-xs-6 col-sm-6 col-md-6 listvalue">
		                                @if($productdetail->longevity_check == "True")
										{{$productdetail->longevity}} years
										@else
										-
										@endif
		                        </li>
		                        @endif


                                @if($shock_option == "True")
                        		<li class="col-xs-6 col-sm-6 col-md-6 listlabel">Shock/CT:</li>
                        		<li class="col-xs-6 col-sm-6 col-md-6 listvalue">
                                	@if($productdetail->shock_check == "True" && $shock_f_product != "")
									{{$shock_f_product}}
									@else
									-
									@endif
                        		</li>
                        		@endif

                        		@if($size_option == "True")
                        		<li class="col-xs-6 col-sm-6 col-md-6 listlabel">Size:</li>
                        		<li class="col-xs-6 col-sm-6 col-md-6 listvalue">
                                	@if($productdetail->size_check == "True" && $size_f_product != "")
									{{$size_f_product}}
									@else
									-
									@endif
                        		</li>
                        		@endif

                        		@if($research_option == "True")
		                        <li class="col-xs-6 col-sm-6 col-md-6 listlabel">Research:</li>
		                        <li class="col-xs-6 col-sm-6 col-md-6 listvalue">
		                                @if($productdetail->research_check == "True")
										{{$productdetail->research}}
										@else
										-
										@endif
		                        </li>
		                        @endif


                                
                                
                                @if($siteinfo_option == "True")
                                <li class="col-xs-6 col-sm-6 col-md-6 listlabel">Site Info:</li>
                                <li class="col-xs-6 col-sm-6 col-md-6 listvalue blue-color">
                                @if($productdetail->siteinfo_check == "True")
								<a href="{{$productdetail->url}}" target="_blank">{{$productdetail->site_info}}</a>
								@else
								-
								@endif
                                </li>
                        		@endif

                                
                               <!-- <li>
								@if($productdetail->overall_value_check == "True")
								{{$productdetail->overall_value}}
								@else
								-
								@endif
								</li> -->
								@foreach($productdetail->custom_fields as $custom)
									@if($custom->field_check == "True")
	                                <li class="col-xs-6 col-sm-6 col-md-6 listlabel">{{$custom->field_name}}:</li>
	                                <li class="col-xs-6 col-sm-6 col-md-6 listvalue">{{$custom->field_value}}</li>
	                                @endif
	                        	@endforeach		
                    </ul>
			</div>
		</div>
		@endforeach
		@foreach($second_product as $productdetail)
		<div class="comparision-list col-xs-4 col-sm-4 col-md-4">
			<div class="comparision-inner">
				<div class="clearfix">
				<div class="col-xs-9 col-sm-9 col-md-9 comapred-logo">
					<img src="upload/{{$productdetail->manufacturer_logo}}" />
				</div>				
				<div class="checkbox">
					<input type="checkbox" checked="checked" disabled="disabled" /> <label></label>
				</div>
				</div>
					<img class="comapred-img" src="upload/{{$productdetail->device_image}}" />
					<h2>{{$productdetail->device_name}}</h2>
					<ul>
								<li class="system-value">
                                ${{ $devicetype == 'New Device' ? number_format($productdetail->system_cost,2) : number_format($productdetail->unit_cost,2) }}
								 </li>
                                 @if($devicetype == 'Changeout')
                                 @if($cco_discount_option == 'True')
                                <li class="system-value">
								{{$productdetail->cco_discount == "-" ? '-' : '$'. number_format($productdetail->cco_discount,2)}}
                                @endif
                                </li>
                                @endif
                                @if($repless_discount_option == 'True')
                                <li class="system-value">
                                {{ $devicetype == 'New Device' 
                                	?( $productdetail->system_repless_cost == "-" 
                                    	? "-"
                                        :'$'.number_format($productdetail->system_repless_cost,2)
                                     ) 
                                    : ( $productdetail->unit_repless_cost == "-" ? "-"
                                    	:'$'.number_format($productdetail->unit_repless_cost,2)
                                      )
                                  }}
                                </li>
                                @endif
                                @if($bulk_option == "True")
		                        <li class="system-value">
		                                @if($devicetype == 'New Device')
										@if($productdetail->system_bulk_check == "True")
										  	{{  $productdetail->remain_bulk > 0 
		                                	?  $productdetail->remain_bulk
		                                    : "0"
		                                    }}
		                                    @else
		                                    -
	                                 	@endif
		                                @elseif($devicetype == 'Changeout')
											@if($productdetail->bulk_check == "True")
										  	{{  $productdetail->remain_bulk > 0 
		                                	?  $productdetail->remain_bulk
		                                    : "0"
		                                    }}
		                                    @else
		                                    -
		                                	@endif
										@endif
		                        </li>
		                        @endif
                               <!-- <li class="green-color">
								{{ $productdetail->reimbursement_check == "True" 
                                	? '$'.number_format($productdetail->reimbursement,2)
                                    : '-'
                                }}
								</li> -->
								@if($exclusive_option == "True")
		                        <li class="col-xs-6 col-sm-6 col-md-6 listlabel">Exclusives:</li>
		                        <li class="col-xs-6 col-sm-6 col-md-6 listvalue">
		                                @if($productdetail->exclusive_check == "True")
		                                {{$productdetail->exclusive}}
		                                @else
		                                -
		                                @endif
		                        </li>
		                        @endif
                                
                                @if($longevity_option == "True")
		                        <li class="col-xs-6 col-sm-6 col-md-6 listlabel">Longevity:</li>
		                        <li class="col-xs-6 col-sm-6 col-md-6 listvalue">
		                        		@if($productdetail->longevity_check == "True")
										{{$productdetail->longevity}} years
										@else
										-
										@endif
		                        </li>
		                        @endif


                                @if($shock_option == "True")
                        		<li class="col-xs-6 col-sm-6 col-md-6 listlabel">Shock/CT:</li>
                        		<li class="col-xs-6 col-sm-6 col-md-6 listvalue">
                                	
                                	@if($productdetail->shock_check == "True" && $shock_s_product != "")
									{{$shock_s_product}}
									@else
									-
									@endif
                        		</li>
                        		@endif

                        		@if($size_option == "True")
                        		<li class="col-xs-6 col-sm-6 col-md-6 listlabel">Size:</li>
                        		<li class="col-xs-6 col-sm-6 col-md-6 listvalue">
                                	@if($productdetail->size_check == "True" && $size_s_product != "")
									{{$size_s_product}}
									@else
									-
									@endif
                        		</li>
                        		@endif

                        		@if($research_option == "True")
		                        <li class="col-xs-6 col-sm-6 col-md-6 listlabel">Research:</li>
		                        <li class="col-xs-6 col-sm-6 col-md-6 listvalue">
		                                @if($productdetail->research_check == "True")
										{{$productdetail->research}}
										@else
										-
										@endif
		                        </li>
		                        @endif
                                
                                
                                @if($siteinfo_option == "True")
                                <li class="col-xs-6 col-sm-6 col-md-6 listlabel">Site Info:</li>
                                <li class="col-xs-6 col-sm-6 col-md-6 listvalue blue-color">
                                @if($productdetail->siteinfo_check == "True")
								<a href="{{$productdetail->url}}" target="_blank">{{$productdetail->site_info}}</a>
								@else
								-
								@endif
                                </li>
                        		@endif

                                
                               <!-- <li>
								@if($productdetail->overall_value_check == "True")
								{{$productdetail->overall_value}}
								@else
								-
								@endif
								</li> -->
								@foreach($productdetail->scustom_fields as $custom)
									
	                                @if($custom->field_check == "True")
	                                <li class="col-xs-6 col-sm-6 col-md-6 listlabel">{{$custom->field_name}}:</li>
	                                <li class="col-xs-6 col-sm-6 col-md-6 listvalue">{{$custom->field_value}}</li>
	                                @endif
	                        	@endforeach		
                    </ul>
			</div>
		</div>
		@endforeach
	</div>
	<div class="compared-right col-xs-5 col-sm-5 col-md-5">	
		@foreach($first_product as $productdetail)
		{{ Form::open(array('url' => 'purchase','id'=>'firstproductform')) }}
		<input type="hidden" value="{{$devicetype}}" name="devicetype"/>
        <input type="hidden" value="{{$productid}}" name="deviceid"/>
       
       
		<div class="delta-panel clearfix"> 
			<div class="delta-left col-xs-7 col-sm-7 col-md-7">
				<ul class="clearfix">
					<li class="col-xs-12 col-sm-12 col-md-12 delta-head">
						- Delta <img src="{{ URL::asset('frontend/images/delta.png') }}" />
					</li>
					<li class="col-xs-6 col-sm-6 col-md-6">{{$devicetype == 'New Device' ?'SystemCost:' : 'Unit Cost'}} </li><li class="col-xs-6 col-sm-6 col-md-6 green-txt">                               
                    {{ $devicetype == "New Device"  ? 
                        		(  $system_cost_diff > 0 
                                	?  "- $". number_format(abs($system_cost_diff),2)
                                    : "+ $". number_format(abs($system_cost_diff),2) )
                                   :
                                   (  $unit_cost_diff > 0 
                                	?  "- $". number_format(abs($unit_cost_diff),2)
                                    : "+ $". number_format(abs($unit_cost_diff),2) )
                                
                                    }}
</li>
					@if($devicetype == 'Changeout')

                    @if($cco_check_value == 'True')
                   	 <li class="col-xs-6 col-sm-6 col-md-6">CCO:</li><li class="col-xs-6 col-sm-6 col-md-6 green-txt">
                   
                                  		{{$cco_diff > 0 ? "- $".number_format(abs($cco_diff),2)
                                  		: "+ $".number_format(abs($cco_diff),2)}}
                    @endif
                    </li>
					@endif

					@if($repless_discount_value == 'True')
                   	 <li class="col-xs-6 col-sm-6 col-md-6">Repless:</li><li class="col-xs-6 col-sm-6 col-md-6 green-txt">
                   
                                  		{{$repless_cost_diff > 0 ? "- $".number_format(abs($repless_cost_diff),2)
                                  		: "+ $".number_format(abs($repless_cost_diff),2)}}
                    </li>
                    @endif
					
				</ul>
			</div>
			<div class="delta-right col-xs-5 col-sm-5 col-md-5">
				<h3>{{$productname}}</h3>
				<img class="comapred-img" src="upload/{{$productimage}}" />
				<input type="button" value="SELECT" id="select1"  />
			</div>	
		</div>
		{{ Form::close() }}
		@endforeach
		
	
</div>
</div>
        </div>
        
        <div id="warning-message">
	<img src="{{ URL::to('images/Neptune-bg-landscape.png')}}" />
</div>

<script>
        jQuery( document ).ready(function()  {
			$("#select2").click(function(){
			
			swal({   
					title: "Are you sure?",   
					text: "You want to order the product!",   
					type: "warning",   
					showCancelButton: true,   
					confirmButtonColor: "#DD6B55",   
					confirmButtonText: "Yes, Purchase it",   
					cancelButtonText: "No, cancel!",   
					closeOnConfirm: false,   
					closeOnCancel: false, 
				}, 
				function(isConfirm){   
					if (isConfirm) 
					{	
						swal("Purchased!", "Thanks for purchasing our product", "success"); 
						$('#secondproductform').submit();
					} 
					else 
					{     
						swal("Cancelled", "Order is cancelled", "error");   
					} 
				});
		
		});
		
		$("#select1").click(function(){
			swal({   
					title: "Are you sure?",   
					text: "You want to order the product!",   
					type: "warning",   
					showCancelButton: true,   
					confirmButtonColor: "#DD6B55",   
					confirmButtonText: "Yes, Order it",   
					cancelButtonText: "No, cancel!",   
					closeOnConfirm: false,   
					closeOnCancel: false,
				}, 
				function(isConfirm){   
					if (isConfirm) 
					{	
						swal("Ordered!", "Thanks for ordering our product", "success"); 
						$('#firstproductform').submit();
					} 
					else 
					{     
						swal("Cancelled", "Order is cancelled", "error");   
					} 
				});
		});
		
		});
		
    </script>
@endsection