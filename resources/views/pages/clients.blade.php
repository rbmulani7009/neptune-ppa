@extends ('layout.default')
@section ('content')

<div class="content-area clearfix" style="margin:0 5%;">
	<div class="top-links clearfix">
		<ul class="add-links">
			<li><a title="Add Device" href="{{ URL::to('admin/clients/add') }}" data-toggle="modal">Add Client</a></li>
		</ul>
	</div>
	<div class="table" >
		<table id='client_table'>
			<thead>
				<tr>
					<th>Item No.</th>
					<th>Client Name</th>
					<th>Street Address</th>
					<th>City</th>
					<th>State</th>
					<th>No. Of Projects</th>
					<th>Action</th>
				</tr>
				<tr>
					<td><input type='t`ext' class='search_text' data-field='clients.item_no' style="width:95px;" /></td>
					<td><input type='text' class='search_text' data-field='clients.client_name' /></td>
					<td><input type='text' class='search_text' data-field='clients.street_address' /></td>
					<td><input type='text' class='search_text' data-field='clients.city' /></td>
					<td><input type='text' class='search_text' data-field='state.state_name' /></td>
					<td></td>
					<td style="width:75px;"></td>
				</tr>
			</thead>
			<tbody id='client_result'>
				@foreach($clients as $client)
				<tr data-id='{{ $client->id}}'>
					<td>{{ $client->item_no}}</td>
					<td>{{ $client->client_name}}</td>
					<td>{{ $client->street_address}}</td>
					<td>{{ $client->city}}</td>
					<td>{{ $client->state_name}}</td>
					<td>{{$client->project_count}}</td>
					<td><a href="{{ URL::to('admin/clients/edit/'.$client->id) }}"><i class="fa fa-edit"></i></a> &nbsp; <a href="{{ URL::to('admin/clients/remove/'.$client->id) }}" onclick="return confirm(' Are you sure you want to delete client?');"><i class="fa fa-close"></i></a></td>
				</tr>
				@endforeach
			</tbody>


		</table>

	</div>

	<div class="bottom-count clearfix">
		{{$clients->count()}} of {{$count}} displayed 
		{{Form::open(array('url'=>'admin/clients','method'=>'get','id'=>'pagesize_form','style'=>'display:inline-block;'))}}
		{{Form::select('pagesize', array('10' => 'Show 10','15' => 'Show 15','20' => 'Show 20',$count=>'Show all'),$pagesize,array('id'=>'pagesize','onchange' => 'this.form.submit()'))}}

		{{Form::close()}}

	</div>
</div>
<script>
    $(document).ready(function () {

        $(".search_text").keyup(function () {
            var fieldName = $(this).data('field');
            var value = $(this).val();

            $.ajax({
                url: "{{ URL::to('admin/search_clients')}}",
                data: {
                    fieldName: fieldName,
                    value: value
                },
                success: function (data) {
					console.log(data);
                    var html_data = '';
                    if (data.status) {
                        $.each(data.value, function (i, item) {
                            console.log(item);
                            html_data += "<tr data-id=" + item.id + "><td>" + item.item_no + "</td><td>" + item.client_name + "</td><td>" + item.street_address + "</td><td>" + item.city + "</td><td>" + item.state_name + "</td><td>"+item.project_count+"</td><td><a href=clients/edit/"+item.id+"><i class='fa fa-edit'></i></a> &nbsp; <a href=clients/remove/"+item.id+" onclick=return&nbsp;confirm('Are&nbsp;you&nbsp;sure&nbsp;you&nbsp;want&nbsp;to&nbsp;delete&nbsp;client?');><i class='fa fa-close'></i></a></td></tr>";

                        });
                    } else {
                        html_data = "<tr> <td colspan='7' style='text-align:center;'> " + data.value + " </td> </tr>"
                    }

                    console.log(html_data);
                    $("#client_result").html(html_data);

                }

            });

        });

    });
</script>
@stop       