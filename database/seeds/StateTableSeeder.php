<?php

use Illuminate\Database\Seeder;
use App\state;

class StateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $state = new state();
        $state->state_name = "Alabama";
        $state->save();

        $state = new state();
        $state->state_name = "California";
        $state->save();

        $state = new state();
        $state->state_name = "Delaware";
        $state->save();

        $state = new state();
        $state->state_name = "Florida";
        $state->save();

        $state = new state();
        $state->state_name = "Kansas";
        $state->save();

        $state = new state();
        $state->state_name = "New Jersey";
        $state->save();

        $state = new state();
        $state->state_name = "New York";
        $state->save();

        $state = new state();
        $state->state_name = "Alaska";
        $state->save();

        $state = new state();
        $state->state_name = "Arizona";
        $state->save();

        $state = new state();
        $state->state_name = "Arkansas";
        $state->save();

        $state = new state();
        $state->state_name = "Colorado";
        $state->save();

        $state = new state();
        $state->state_name = "Connecticut";
        $state->save();

        $state = new state();
        $state->state_name = "Georgia";
        $state->save();

        $state = new state();
        $state->state_name = "Hawaii";
        $state->save();

        $state = new state();
        $state->state_name = "Idaho";
        $state->save();

        $state = new state();
        $state->state_name = "Illinois";
        $state->save();

        $state = new state();
        $state->state_name = "Indiana";
        $state->save();

        $state = new state();
        $state->state_name = "Iowa";
        $state->save();

        $state = new state();
        $state->state_name = "Kentucky";
        $state->save();

        $state = new state();
        $state->state_name = "Louisiana";
        $state->save();

        $state = new state();
        $state->state_name = "Maine";
        $state->save();

        $state = new state();
        $state->state_name = "Maryland";
        $state->save();

        $state = new state();
        $state->state_name = "Massachusetts";
        $state->save();

        $state = new state();
        $state->state_name = "Michigan";
        $state->save();

        $state = new state();
        $state->state_name = "Minnesota";
        $state->save();

        $state = new state();
        $state->state_name = "Mississippi";
        $state->save();

        $state = new state();
        $state->state_name = "Missouri";
        $state->save();

        $state = new state();
        $state->state_name = "Montana";
        $state->save();

        $state = new state();
        $state->state_name = "Nebraska";
        $state->save();

        $state = new state();
        $state->state_name = "Nevada";
        $state->save();

        $state = new state();
        $state->state_name = "New Hampshire";
        $state->save();

        $state = new state();
        $state->state_name = "New Mexico";
        $state->save();

        $state = new state();
        $state->state_name = "North Carolina";
        $state->save();

        $state = new state();
        $state->state_name = "North Dakota";
        $state->save();

        $state = new state();
        $state->state_name = "Ohio";
        $state->save();

        $state = new state();
        $state->state_name = "Oklahoma";
        $state->save();

        $state = new state();
        $state->state_name = "Oregon";
        $state->save();

        $state = new state();
        $state->state_name = "Pennsylvania";
        $state->save();

        $state = new state();
        $state->state_name = "Rhode Island";
        $state->save();

        $state = new state();
        $state->state_name = "South Carolina";
        $state->save();

        $state = new state();
        $state->state_name = "South Dakota";
        $state->save();

        $state = new state();
        $state->state_name = "Tennessee";
        $state->save();

        $state = new state();
        $state->state_name = "Texas";
        $state->save();

        $state = new state();
        $state->state_name = "Utah";
        $state->save();

        $state = new state();
        $state->state_name = "Vermont";
        $state->save();

        $state = new state();
        $state->state_name = "Virginia";
        $state->save();

        $state = new state();
        $state->state_name = "Washington";
        $state->save();

        $state = new state();
        $state->state_name = "West Virginia";
        $state->save();

        $state = new state();
        $state->state_name = "Wisconsin";
        $state->save();

        $state = new state();
        $state->state_name = "Wyoming";
        $state->save();

    }
}
