<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMonthIdScorecard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scorecards', function (Blueprint $table) {
            
            $table->integer('monthId')->unsigned();
            $table->foreign('monthId')->references('id')->on('month')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scorecards',function(Blueprint $table){
            $table->dropColumn('monthId');
        });
    }
}
