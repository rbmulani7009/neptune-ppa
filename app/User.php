<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $fillable = [
        'name', 'email','organization','org_type','roll','projectname','status','is_delete','is_agree', 'password','mobile','title','profilePic'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    /*get rolename*/
    public function role(){
        return $this->belongsTo('App\roll','roll');
    }

    /*get usersproject*/
    public function usersproject(){
        return $this->hasMany('App\userProjects','userId');
    }

    /*get userclient*/
    public function userclients(){
        return $this->hasMany('App\userClients','userId');
    }
    
    public function surveyuser()
    {
       return $this->hasMany('App\SurveyAnswer','user_id');
    }

    public function customcontact()
    {
        return $this->hasMany('App\customContact','order_email');
    }

    public function manufacture()
        {
              return $this->belongsTo('App\manufacturers','organization');
        }

    public static function manufaturename(){
        return static::leftjoin('manufacturers', 'manufacturers.id', '=', 'users.organization')
                ->select('users.*', 'manufacturers.manufacturer_name', 'users.organization as company');
    }

    public static function repcontact()
    {
       return static::leftjoin('rep_contact_info','rep_contact_info.repId','=','users.id')->leftjoin('manufacturers', 'manufacturers.id', '=', 'users.organization')->select('users.*','rep_contact_info.repStatus','rep_contact_info.deviceId','manufacturers.manufacturer_name', 'users.organization as company');
    }
}
