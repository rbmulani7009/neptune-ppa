<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class project extends Model
{
    protected $fillable = [
      		'project_name','is_delete'
     ];

     protected $table='projects';

     /*get projectname*/
    public function userproject(){
        return $this->hasMany('App\userProjects','projectId');
    }
}
