<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class device extends Model
{
    protected $table = 'device';

    protected $fillable = [
    	'level_name','project_name','category_name','manufacturer_name','device_name','model_name','device_image','rep_email','status','exclusive','exclusive_check','longevity','longevity_check','shock','shock_check','size','size_check','research','research_check','website_page','website_page_check','url','overall_value','overall_value_check','is_delete'
    	];  

    public function survey()
    {
    	return $this->hasMany('App\Survey','deviceId');
    }

   	public function manufacturer()
   	{
   		return $this->belongsTo('App\manufacturers','manufacturer_name');
   	}
}
