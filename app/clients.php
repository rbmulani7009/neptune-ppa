<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class clients extends Model
{

    protected $fillable = [
      		'item_no','client_name','street_address','city','state','is_active','is_delete'
     ];

     protected $table='clients';

     /*get projectname*/
    public function userclients(){
        return $this->hasMany('App\userClients','clientId');
    }

		public function survey() {

		    return $this->hasMany('App\Survey','clientId');
		} 

		public function contact() {

    		return $this->hasMany('App\customContact','clientId');
  		} 


}
