<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Devicefeatures extends Model
{
    protected $table = 'device_features';

    protected $fillable = [
    	'device_id','client_name','longevity_check','shock_check','size_check','research_check','siteinfo_check','overall_value_check'
    ];
}
