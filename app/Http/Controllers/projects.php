<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\clients;
use App\project;
use App\project_clients;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Session;
use App\User;
use Auth;

class projects extends Controller {

    public function index(Request $request) {
        $pagesize = $request->get('pagesize');
        if ($pagesize == "") {
            $pagesize = 10;
        }

        if (Auth::user()->roll == 2) {
            $userid = Auth::user()->id;
            $organization = user::where('id', '=', $userid)->value('organization');
            $projects = Project::join('project_clients', 'project_clients.project_id', '=', 'projects.id')
                    ->where('project_clients.client_name', '=', $organization)
                    ->select('projects.*')
                    ->orderBy('projects.id', 'desc')
                    ->paginate($pagesize);
					
			$count = Project::join('project_clients', 'project_clients.project_id', '=', 'projects.id')
                    ->where('project_clients.client_name', '=', $organization)
					->count();
        } 
		else 
		{
            $projects = Project::orderBy('projects.id', 'desc')->paginate($pagesize);
       		$count = Project::count();
	    }

        foreach ($projects as $project) {
            $prid = $project->id;


            $project->clients = project_clients::join('clients', 'clients.id', '=', 'project_clients.client_name')
                    			->where('project_id', '=', $prid)
								->get();
								
            $project->clients_count = $project->clients->count();
            foreach ($project->clients as $client) 
			{
                $client->clients_user_count = User::where('organization', '=', $client->id)
                        ->where('projectname', '=', $prid)
                        ->where('roll', '=', 3)                      
                        ->count();
						
				
            }

            if (Auth::user()->roll == 2) {
                $project->users_count = User::join('clients','clients.id','=','users.organization')
				        ->where('projectname', '=', $prid)
                        ->where('organization', '=', $organization)
                        ->where('roll', '=', 3)
                        ->count();
            } else {
				
                $project->users_count = User::join('clients','clients.id','=','users.organization')
						->where('users.projectname', '=', $prid)
				        ->where('users.roll', '=', 3)
                        ->count();
            }
        }

        return view('pages.projects', compact('projects', 'count', 'pagesize'));
    }

    public function add() {
        $clients = ['0' => 'Client Name'] + Clients::lists('client_name', 'id')->all();
        return view('pages.addproject', compact('clients'));
    }

    public function create(Request $request) {

        $rules = array(
            'project_name' => 'required|unique:projects,project_name'
        );


        $insertdata = array(
            'project_name' => $request->get('project_name'),
            'is_delete' => '0'
        );



        $validator = Validator::make($insertdata, $rules);

        if ($validator->fails()) {
            return Redirect::to('admin/projects/add')
                            ->withErrors($validator);
        } else {
            $check_delete_projectname = project::where('project_name', '=', $request->get('project_name'))
										->where('is_delete', '=', 0)->count();
            if ($check_delete_projectname >= 1) {
                return Redirect::to('admin/projects/add')
                                ->withErrors(['project_name' => 'Projectname already exist in database']);
            } else {

                $ck = 0;
                $ck = Project::insert($insertdata);
                if ($ck > 0) {
                    $pr_id = Project::get()->last();
                    $pr_id = $pr_id->id;
                    $client_names = $request->get('client_name');

                    if (isset($client_names) || !empty($client_names)) {
                        foreach ($client_names as $client_name) {
                            $project_client = array(
                                'project_id' => $pr_id,
                                'client_name' => $client_name
                            );
                            $pr = project_clients::insert($project_client);
                        }
                    }
                    return Redirect::to('admin/projects');
                } else {
                    return fail;
                }
            }
        }
    }

    public function edit($id) {
        $selected = Project_clients::where('project_id', '=', $id)->lists('client_name')->all();
        $client_name = ['0' => 'Client Name'] + Clients::where('is_active', '=', '1')->lists('client_name', 'id')->all();

        $projects = Project::FindOrFail($id);
        $clients = Project_clients::join('clients', 'clients.id', '=', 'project_clients.client_name')->select('project_clients.id', 'clients.client_name')->where('project_clients.project_id', '=', $id)->get();
        return view('pages.editproject', compact('projects', 'clients', 'client_name', 'selected'));
    }

    public function update($id, Request $request) {

        $rules = array(
            'project_name' => 'required'
        );

        $updatedata = array(
            'project_name' => $request->get('project_name')
        );

        $validator = Validator::make($updatedata, $rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        } else {
            $projectname = Input::get('project_name');
            $get_projectname = project::where('id', '=', $id)->value('project_name');

            if ($get_projectname != $projectname) {
                $check_projects = project::where('project_name', '=', $projectname)->count();
                if ($check_projects >= 1) {
                    return Redirect::back()
                                    ->withErrors(['project_name' => 'Project name already exist in database.']);
                }
            }

            $client_names = $request->get('client_name');

            // Delete current data
            project_clients::where('project_id', $id)->delete();

            if (isset($client_names) && !empty($client_names)) {
                foreach ($client_names as $client_name) {
                    $project_client = array(
                        'project_id' => $id,
                        'client_name' => $client_name
                    );
                    project_clients::insert($project_client);
                }
            }
            $update_projectname = DB::table('projects')->where('id', '=', $id)->update($updatedata);
            return Redirect::to('admin/projects');
        }
    }

    public function clients_remove($id) {
        $remove_project_client = project_clients::FindOrFail($id);
        $remove_project_client->delete();

        return Redirect::back();
    }

    public function search() {
        $fieldname = Input::get('fieldName');
        $fieldvalue = Input::get('value');
       
        if (Auth::user()->roll == 2) {
            $userid = Auth::user()->id;
            $organization = user::where('id', '=', $userid)->value('organization');
            $search_projects = Project::leftjoin('project_clients', 'project_clients.project_id', '=', 'projects.id')
								->where('project_clients.client_name', '=', $organization)
								->orderBy('projects.id', 'desc')
								->select('projects.*')
								->where($fieldname, 'LIKE', $fieldvalue . '%')->get();
        
        } 
		else 
		{
            $search_projects = Project::leftjoin('project_clients', 'project_clients.project_id', '=', 'projects.id')
								->leftjoin('clients', 'clients.id', '=', 'project_clients.client_name')
								->select('projects.*')
								->groupBy('projects.id')
								->orderBy('projects.id', 'desc')
								->where($fieldname, 'LIKE', $fieldvalue . '%')
								->get();

            if($fieldvalue == "")
            {
               $search_projects = Project::orderBy('projects.id', 'desc')->get();
            }
           
        }
        foreach ($search_projects as $search_project) {
            $prid = $search_project->id;
            

            $search_project->clients = project_clients::join('clients', 'clients.id', '=', 'project_clients.client_name')
									  ->where('project_id', '=', $prid)
									  ->get();
									  
            $search_project->clients_count = $search_project->clients->count();
        	foreach ($search_project->clients as $client) {
                $client->clients_user_count = User::where('organization', '=', $client->id)
                        ->where('projectname', '=', $prid)
                        ->where('roll', '=', 3)                                            
                        ->count();
            }
            if (Auth::user()->roll == 2) {
                $search_project->users_count = User::join('clients','clients.id','=','users.organization')
				        ->where('projectname', '=', $prid)
                        ->where('organization', '=', $organization)
                        ->where('roll', '=', 3)                                            
                        ->count();
            } else {

                  $search_project->users_count = User::join('clients','clients.id','=','users.organization')
						  ->where('users.projectname', '=', $prid)
				          ->where('users.roll', '=', 3)                                               
                          ->count();


            }   
        }
        $data = $search_projects;

        if (count($data))
            return [
                'value' => $data,
                'status' => TRUE
            ];
        else
            return [
                'value' => 'No Result Found',
                'status' => FALSE
            ];
    }

    public function remove($id) {
        $removedata = array('is_delete' => '1');
        $remove_projects = DB::table('projects')->where('id', '=', $id)->delete();
		$remove_project_clients = DB::table('project_clients')->where('project_id','=',$id)->delete();
        return Redirect::to('admin/projects');
    }

}
