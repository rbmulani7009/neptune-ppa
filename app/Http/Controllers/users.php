<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\project;
use App\roll;
use App\User;
use Hash;
use Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use App\clients;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\manufacturers;
use App\project_clients; 
use Mail;

// ScoreCard
use App\ScoreCard;
use App\ScoreCardImage;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Month;

use App\userClients;
use App\userProjects;


class users extends Controller {

    public function index(Request $request) {
        $pagesize = $request->get('pagesize');
        if ($pagesize == "") {
            $pagesize = 10;
        }
        if (Auth::user()->roll == 2) {
            $userid = Auth::user()->id;
            //$organization = user::where('id', '=', $userid)->value('organization');
            
            // $users = user::leftjoin('projects', 'projects.id', '=', 'users.projectname')
            //         ->join('roll', 'roll.id', '=', 'users.roll')
            //         ->select('users.*', 'roll.roll_name', 'projects.project_name')
            //         ->whereIn('organization',$organization)
            //         ->where('users.is_delete', '=', 0)
            //         ->where('users.roll', '!=', 5)
            //         ->orderby('users.id', '=', 'DESC')
            //         ->paginate($pagesize);

            /*new update*/
            $organization = userClients::where('userId',$userid)->select('clientId')->get();

            $users = User::leftJoin('user_clients','user_clients.userId','=','users.id')
                    ->whereIn('user_clients.clientId',$organization)
                    ->select('users.*')
                    ->where('users.is_delete','=',0)
                    ->where('users.roll','!=',5)
                    ->orderby('users.id','DESC')
                    ->groupBy('user_clients.userId')
                    ->paginate($pagesize);
            
            $count = count(User::leftJoin('user_clients','user_clients.userId','=','users.id')
                    ->whereIn('user_clients.clientId',$organization)
                    ->select('users.*')
                    ->where('users.is_delete','=',0)
                    ->where('users.roll','!=',5)
                    ->orderby('users.id','DESC')
                    ->groupBy('user_clients.userId')->get());

        } else {

            $users = user::select('users.*')
                    ->where('is_delete', '=', 0)
                    ->orderby('id', '=', 'DESC')
                    ->paginate($pagesize);

            $count = user::where('is_delete', '=', 0)->count();
        }
        // foreach ($users as $user) {
            

        //     // $organization = $user->organization;
        //     // if ($rollname == 3 || $rollname == 4 || $rollname == 2) {
        //     //     $clientname = clients::where('id', '=', $organization)->where('is_delete', '=', 0)->get();
                
        //     //     foreach ($clientname as $clientname) {
        //     //         $user->org_name = $clientname->client_name;
        //     //     }
        //     // } else if ($rollname == 5) {
        //     //     $user->org_name = "manufacturer";
        //     //     $manufacturers = manufacturers::where('id', '=', $organization)->where('is_delete', '=', 0)->get();
        //     //     foreach ($manufacturers as $manufacturer) {
        //     //         $user->org_name = $manufacturer->manufacturer_name;
        //     //     }
        //     // } else if ($rollname == 0 || $rollname == 1) {
        //     //     $user->org_name = "Neptune PPA";
        //     // }

        //     foreach ($user->usersproject as $row) {
        //         $row->projectname->project_name;
        //     }

        //     foreach ($user->userclients as $row) {
        //         $row->clientname->client_name;
        //     }
        // }   
        return view('pages.users', compact('users', 'count', 'pagesize'));
    }

    public function add() {
        $projects = ['0' => 'Project Name'] + Project::lists('project_name', 'id')->all();
        $rolls = [NULL => 'Roll'] + roll::lists('roll_name', 'id')->all();
        if (Auth::user()->roll == 2) {
            $userid = Auth::user()->id;
            $organization = userClients::where('userId',$userid)->select('clientId')->get();
            $clients = ['0' => 'Organization'] + clients::whereIn('id', $organization)->where('is_delete', '=', 0)->lists('client_name', 'id')->all();
        } else {
            $clients = ['0' => 'Organization'] + clients::where('is_delete', '=', 0)->lists('client_name', 'id')->all();
        }

        $manufacturers = [NULL => 'Organization'] + manufacturers::where('is_delete', '=', 0)->lists('manufacturer_name', 'id')->all();

        return view('pages.adduser', compact('projects', 'rolls', 'clients', 'manufacturers'));
    }

    public function create(Request $request) {
        $role = $request->get('roll');

        $rules = array(
            'name' => 'required',
            'email' => 'required|unique:users,email|email',
            'roll' => 'required|not_in:0',
            'projectname' => 'required|not_in:0',
            'status' => 'required|not_in:0',
            'password' => 'required|alpha_dash|min:6|max:14',
            'password_confirmation' => 'required|same:password',
        );

        switch ($role) {
            case 0 :
            case 1 :
                unset($rules['projectname']);
                $validator = Validator::make($request->all(), $rules);

                $insertdata = array(
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    'roll' => $request->get('roll'),
                    'status' => $request->get('status'),
                    'password' => Hash::make($request->get('password')),
                    'is_delete' => '0'
                );

                break;
            case 2 :
                
                 // $rules['clients'] = 'required|not_in:0';
                 $rules['client_name'] = 'required|not_in:0';
                 unset($rules['clients']);
                 unset($rules['projectname']);
                 $validator = Validator::make($request->all(), $rules);

                
                $insertdata = array(
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    //'organization' => $request->get('clients'),
                    'roll' => $request->get('roll'),
                    'status' => $request->get('status'),
                    'password' => Hash::make($request->get('password')),
                    'is_delete' => '0'
                );


                break;
            case 3 :
                //$rules['clients'] = 'required|not_in:0';
                $rules['client_name'] = 'required|not_in:0';
                $rules['project_name'] = 'required|not_in:0';
                unset($rules['clients']);
                unset($rules['projectname']);
                $validator = Validator::make($request->all(), $rules);

                $insertdata = array(
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    //'organization' => $request->get('clients'),
                    //'projectname' => $request->get('projectname'),
                    'roll' => $request->get('roll'),
                    'status' => $request->get('status'),
                    'password' => Hash::make($request->get('password')),
                    'is_delete' => '0'
                );

            break;
            case 4 :

                $rules['clients'] = 'required|not_in:0';
                unset($rules['projectname']);

                $validator = Validator::make($request->all(), $rules);

                $insertdata = array(
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    'organization' => $request->get('clients'),
                    'roll' => $request->get('roll'),
                    'status' => $request->get('status'),
                    'password' => Hash::make($request->get('password')),
                    'is_delete' => '0'
                );

            break;
            case 5:

                $rules['manufacturer'] = 'required|not_in:0';
                $rules['mobile'] = 'required|numeric';
                $rules['title'] = 'required';
                $rules['profilePic'] = 'required';


            $validator = Validator::make($request->all(), $rules);

                

                $insertdata = array(
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    'organization' => $request->get('manufacturer'),
                    'roll' => $request->get('roll'),
                    'projectname' => $request->get('projectname'),
                    'status' => $request->get('status'),
                    'password' => Hash::make($request->get('password')),
                    'is_delete' => '0',
                    'mobile' => $request->get('mobile'),
                    'title' => $request->get('title')
                );

            break;
            default :
            break;
        }

        if ($validator->fails()) {
            return Redirect::to('admin/users/add')
            ->withErrors($validator)
            ->withInput($request->except('password'));
        } else {

            

            $userid = $newUser->id;

            /*upload profilepic */
            if($request->hasFile('profilePic'))
            {
                $image = $request->file('profilePic');
                $destinationpath = public_path().'/upload/user';
                $extension = $image->getClientOriginalExtension();
                $filename = 'user' . '_' . date('m-d-y_hia') . '_'. rand(11111, 99999) .'.' . $extension;
                $image->move($destinationpath,$filename);
                $insertdata['profilePic'] = $filename;
            }

            $newUser = new user();
            $newUser->fill($insertdata);
            $newUser->save();

            /*order client insert*/
            if(!empty($request->get('clients')))
            {
                $userclient = array('userId' => $userid , 'clientId' => $request->get('clients') );
                $userclient = userClients::insert($userclient);
            }
            
            /* multi client insert */
            $client_names = $request->get('client_name'); 
            if(!empty($client_names))
            {
                foreach ($client_names as $row) {
                    $user_clients = array(
                        'userId' => $userid,
                        'clientId' => $row,

                        );
                    $user_clients = userClients::insert($user_clients);
                }
            }

            /* multi project insert */
            $project_names = $request->get('project_name'); 
            if(!empty($project_names))
            {
                foreach ($project_names as $row) {
                    $user_projects = array(
                        'userId' => $userid,
                        'projectId' => $row,

                        );
                    $user_projects = userProjects::insert($user_projects);
                }
            }



            if ($newUser) {
                $title = "Welcome to Neptune-PPA - Login Details ";
                $email = $request->get('email');
                $password = $request->get('password');
                $data = array('title' => $title, 'password' => $password,
                    'email' => $email);

                Mail::send('emails.createuser', $data, function($message) use ($data) {
                    $message->from('admin@neptuneppa.com')->subject($data['title']);
                    $message->to($data['email']);
                });


                
            }
            return Redirect::to('admin/users');
        }
    }

    public function edit($id) {

        $projects = [NULL => 'Project Name'] + Project::lists('project_name', 'id')->all();
        $rolls = roll::lists('roll_name', 'id')->all();
        if (Auth::user()->roll == 2) {
            $userid = Auth::user()->id;
            $organization = user::where('id', '=', $userid)->value('organization');

            $organization = userClients::where('userId',$userid)->select('clientId')->get();
            $clients = ['0' => 'Organization'] + clients::whereIn('id', $organization)->where('is_delete', '=', 0)->lists('client_name', 'id')->all();

        } else {
            $clients = clients::where('is_delete', '=', 0)->lists('client_name', 'id')->all();

        }
    $manufacturers = manufacturers::where('is_delete', '=', 0)->lists('manufacturer_name', 'id')->all();

        $users = user::FindOrFail($id);
        $selectedclients = userClients::where('userId',$id)->lists('clientId')->all();
        $selectedprojects = userProjects::where('userId',$id)->lists('projectId')->all();

        
        

        return view('pages.edituser', compact('users', 'clients', 'rolls', 'users', 'projects', 'manufacturers','selectedclients','selectedprojects'));
    }

    public function update($id, Request $request) {
        $role = $request->get('roll');
        $password = $request->get('password');

        $rules = array(
            'name' => 'required',
            'email' => 'required',
            'roll' => 'required|not_in:0',
            'projectname' => 'required|not_in:0',
            'status' => 'required|not_in:0'
            );

        if ($password) {
            $rules['password'] = 'required|alpha_dash|min:6|max:14';
            $rules['password_confirmation'] = 'required|same:password';
        }

        switch ($role) {
            case 0 :
            case 1 :

                unset($rules['projectname']);
                $updatedata = array(
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    'organization' => "",
                    'projectname' => "",
                    'roll' => $request->get('roll'),
                    'status' => $request->get('status'),
                    'is_delete' => '0'
                );
            $validator = Validator::make($request->all(), $rules);

            break;
            case 2 :


                // $rules['clients'] = 'required|not_in:0';
                 $rules['client_name'] = 'required|not_in:0';
                 unset($rules['clients']);
                 unset($rules['projectname']);
                $updatedata = array(
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                   // 'organization' => $request->get('clients'),
                    'roll' => $request->get('roll'),
                    'status' => $request->get('status'),
                    'is_delete' => '0'
                );
            $validator = Validator::make($request->all(), $rules);

            break;

            case 3:
                //$rules['clients'] = 'required|not_in:0';
                $rules['client_name'] = 'required|not_in:0';
                $rules['project_name'] = 'required|not_in:0';
                unset($rules['clients']);
                unset($rules['projectname']);
                $updatedata = array(
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    //'organization' => $request->get('clients'),
                    //'projectname' => $request->get('projectname'),
                    'roll' => $request->get('roll'),
                    'status' => $request->get('status'),
                    'is_delete' => '0'
                );
            $validator = Validator::make($request->all(), $rules);

            break;
            case 4 :
                $rules['clients'] = 'required|not_in:0';
                unset($rules['projectname']);
                $updatedata = array(
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    'organization' => $request->get('clients'),
                   // 'projectname' => "",
                    'roll' => $request->get('roll'),
                    'status' => $request->get('status'),
                    'is_delete' => '0'
                );
            $validator = Validator::make($request->all(), $rules);

            break;
            case 5:
               
                $rules['manufacturer'] = 'required|not_in:0';
                $rules['mobile'] = 'required|numeric';
                $rules['title'] = 'required';
                $rules['profilePic'] = 'required';

                
                $validator = Validator::make($request->all(), $rules);

                

                $updatedata = array(
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    'organization' => $request->get('manufacturer'),
                    'roll' => $request->get('roll'),
                    'projectname' => $request->get('projectname'),
                    'status' => $request->get('status'),
                    'is_delete' => '0',
                    'mobile' => $request->get('mobile'),
                    'title' => $request->get('title')
                );

            break;
            default :

            break;
        }

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        } else {

            if ($password) {
                $updatedata['password'] = Hash::make($request->get('password'));
            }

            /*upload image*/
            if($request->hasFile('profilePic'))
            {
                $image = $request->file('profilePic');
                $destinationpath = public_path().'/upload/user';
                $extension = $image->getClientOriginalExtension();
                $filename = 'user' . '_' . date('m-d-y_hia') . '_'. rand(11111, 99999) .'.' . $extension;
                $image->move($destinationpath,$filename);
                $updatedata['profilePic'] = $filename;
                
            }

             /*order client insert*/
            if(!empty($request->get('clients')))
            {
                // Delete current data
                userClients::where('userId', $id)->delete();


                $userclient = array('userId' => $id , 'clientId' => $request->get('clients') );
                $userclient = userClients::insert($userclient);
            }
            
            /* multi client insert */
            $client_names = $request->get('client_name'); 
            if(!empty($client_names))
            {
                 // Delete current data
                userClients::where('userId', $id)->delete();

                foreach ($client_names as $row) {
                    $user_clients = array(
                        'userId' => $id,
                        'clientId' => $row,

                        );
                    $user_clients = userClients::insert($user_clients);
                }
            }

            /* multi project insert */
            $project_names = $request->get('project_name'); 

            if(!empty($project_names))
            {
                 // Delete current data
                userProjects::where('userId', $id)->delete();


                foreach ($project_names as $row) {
                    $user_projects = array(
                        'userId' => $id,
                        'projectId' => $row,

                        );
                    $user_projects = userProjects::insert($user_projects);
                }
            }

            $email = Input::get('email');
            $get_email = user::where('id', '=', $id)->value('email');
            if ($get_email == $email) {
                $get_username = DB::table('users')->where('id', '=', $id)->update($updatedata);
                return Redirect::to('admin/users');
            } else {
                $check_email = user::where('email', '=', $email)->count();
                if ($check_email >= 1) {
                    return Redirect::back()
                    ->withErrors(['email' => 'Email already exist in database.',]);
                } else {
                    $update_user = DB::table('users')->where('id', '=', $id)->update($updatedata);
                    return Redirect::to('admin/users');
                }
            }
        }
    }

    public function search() {
        $fieldname = Input::get('fieldName');
        $fieldvalue = Input::get('value');
        if (Auth::user()->roll == 2) {
            $userid = Auth::user()->id;
            $organization = user::where('id', '=', $userid)->value('organization');
            $search_users = user::leftjoin('projects', 'projects.id', '=', 'users.projectname')
                    ->join('roll', 'roll.id', '=', 'users.roll')
                    ->select('users.*', 'roll.roll_name', 'projects.project_name')
                    ->where('organization', '=', $organization)
                    ->where($fieldname, 'LIKE', $fieldvalue . '%')
                    ->orderby('users.id', '=', 'DESC')
                    ->where('users.is_delete', '=', 0)
                    ->where('users.roll', '!=', 5)
                    ->get();
        } else {

            if($fieldvalue != "")
            {
                $query = user::leftjoin('projects', 'projects.id', '=', 'users.projectname')
                    ->join('roll', 'roll.id', '=', 'users.roll')
                    ->where($fieldname, 'LIKE', $fieldvalue . '%')
                    ->orderby('users.id', '=', 'DESC')
                    ->where('users.is_delete', '=', 0);

            }
            else
            {
                $query = user::leftjoin('projects', 'projects.id', '=', 'users.projectname')
                    ->join('roll', 'roll.id', '=', 'users.roll')
                    ->orderby('users.id', '=', 'DESC')
                    ->where('users.is_delete', '=', 0);

            }
            switch ($fieldname) {
                case "manufacturers.manufacturer_name":
                    $query->select('users.*', 'roll.roll_name', 'manufacturers.manufacturer_name', 'projects.project_name');
                    $query->whereIn('users.roll', [5]);
                    $query->leftjoin('manufacturers', 'manufacturers.id', '=', 'users.organization');

                break;
                case "clients.client_name":
                    $query->select('users.*', 'roll.roll_name', 'clients.client_name', 'projects.project_name');
                    $query->whereIn('users.roll', [2, 3, 4]);
                    $query->leftjoin('clients', 'clients.id', '=', 'users.organization');

                break;
                case "users.organization":
                    $query->select('users.*', 'roll.roll_name', 'projects.project_name');
                    $query->whereIn('users.roll', [1]);

                    break;
                default :
                    $query->select('users.*', 'roll.roll_name', 'clients.client_name', 'projects.project_name');
                    $query->leftjoin('manufacturers', 'manufacturers.id', '=', 'users.organization');
                    $query->leftjoin('clients', 'clients.id', '=', 'users.organization');

                    break;
            }
            $search_users = $query->get();
        }

        foreach ($search_users as $user) {
            $rollname = $user->roll;
            $organization = $user->organization;
            if ($rollname == 3 || $rollname == 4 || $rollname == 2) {
                $clientnames = clients::where('id', '=', $organization)->where('is_delete', '=', 0)->get();
                foreach ($clientnames as $clientname) {
                    $user->org_name = $clientname->client_name;
                }
            } else if ($rollname == 5) {
                $user->org_name = "manufacturer";
                $manufacturers = manufacturers::where('id', '=', $organization)->where('is_delete', '=', 0)->get();
                foreach ($manufacturers as $manufacturer) {
                    $user->org_name = $manufacturer->manufacturer_name;
                }
            } else if ($rollname == 1) {
                $user->org_name = "Neptune PPA";
            }
        }

        $data = $search_users;

        if (count($data))
            return [
        'value' => $data,
        'status' => TRUE
        ];
        else
            return [
        'value' => 'No Result Found',
        'status' => FALSE
        ];
    }

    public function updateall() {
        $hiddenid = Input::get('hiddenid');
        $status = Input::get('status');
        foreach (array_combine($hiddenid, $status) as $userid => $user) {
            $updatedata = array('status' => $user);
            $ck = 0;
            $ck = DB::table('users')->where('id', '=', $userid)->update($updatedata);
        }

        return Redirect::to('admin/users');
    }

    public function remove($id) {
        $removedata = array('is_delete' => '1');
        $remove = DB::table('users')->where('id', '=', $id)->delete();
        return Redirect::to('admin/users');
    }

    public function getprojectname() {
        $clientid = Input::get('clientid');

        if ($clientid == "") {
            $getprojectname =  Project::select('project_name', 'id')->get();

        }
        else {
            $getprojectname = project_clients::join('projects', 'projects.id', '=', 'project_clients.project_id')->distinct('projects.id')->where('project_clients.client_name', '=', $clientid)->select('projects.project_name', 'projects.id')->get();

        }

        $data = $getprojectname;

            if (count($data))
                return [
                    'value' => $data,
                    'status' => TRUE
                ];
            else
                return [
                    'value' => 'No Result Found',
                    'status' => FALSE
                ];
    }

    public function getprojectnames(){

        $clientid = Input::get('clientid');
        $getprojectname =  Project::select('project_name', 'id')->get();

        if(!empty($clientid))
        {
            $getprojectname = project_clients::join('projects', 'projects.id', '=', 'project_clients.project_id')->distinct('projects.id')->whereIn('project_clients.client_name',$clientid)->select('projects.project_name', 'projects.id')->get();
        }

        $data = $getprojectname;

        if (count($data))
            return [
                'value' => $data,
                'status' => TRUE
            ];
        else
            return [
                'value' => 'No Result Found',
                'status' => FALSE
            ];
    }

    
    /*Physician ScoreCard Start*/

    public function phyScoreCard($id,Request $request)
    {   
        $pagesize = $request->get('pagesize');
        if ($pagesize == "") {
            $pagesize = 10;
        }

        $user = User::where('id',$id)->first();
        
        $scorecard = ScoreCard::where('userId',$id)->paginate($pagesize);

        $count = ScoreCard::where('userId',$id)->count();

        return view('pages.scorecard.scorecard', compact('user', 'scorecard','count','pagesize'));
        
    }

    public function phyScoreCardCreate(Request $request,$id)
    {
       $user = User::where('id',$id)->first();

        $check_month = ScoreCard::where('userId',$user['id'])->get();
        
        $monthName = [];

        foreach ($check_month as $mon) {
            $monthName[] = $mon->monthId;

        }

       $month = Month::orderBy('id','asc')->whereNotIn('id', $monthName)->pluck('month','id')->all();

       return view('pages.scorecard.addScoreCard', compact('user','month'));
   }

   public function phyScoreCardStore(Request $request,$id)
   {
    $rules = array(
        'monthId' => 'required',
        'year'=>'required',
        'scorecardImage'=>'required',
        );

    $validator = Validator::make($request->all(), $rules);

    if ($validator->fails()) {
        return redirect()->back()
        ->withErrors($validator)
        ->withInput();

    }


    $data = $request->all();


    if(!($request->hasFile('scorecardImage'))){
        return redirect()->back()
        ->withErrors(['scorecardImage' => 'Scorecard Image Required.!!',])
        ->withInput();
    }
    $scorecardImage = $request['scorecardImage'];

    $data['monthId']=$request['monthId'];
    $data['year']=$request['year'];
    $data['userId']= $id;

    $scorecard = new ScoreCard;
    $scorecard->fill($data);

    if ($scorecard->save()) {

        foreach ($scorecardImage as $image) {

          $extension = $image->getClientOriginalExtension();

          $destinationpath = public_path().'/upload/scorecard';
          $filename = 'scorecard' . '_' .rand (1,9999). '.' . $extension;
          $move = $image->move($destinationpath, $filename);

          $dishesdata['scorecardId'] = $scorecard->id;
          $dishesdata['scorecardImage'] ='upload/scorecard/'. $filename;

          $dishimage = new ScoreCardImage();
          $dishimage->fill($dishesdata);
          $dishimage->save();
      }

      return redirect('admin/users/scorecard/'.$id);  
  } 

}
    public function phyScoreCardEdit($id)
  {
        $score = ScoreCard::where('id',$id)->first();
        $scoreImage = ScoreCardImage::where('scorecardId',$id)->get();

         $check_month = ScoreCard::where('userId',$score['userId'])->where('monthId', '!=', $score->monthId)->get();
       
        $monthName = [];

        foreach ($check_month as $mon) {
            $monthName[] = $mon->monthId;

        }

       $month = Month::orderBy('id','asc')->whereNotIn('id', $monthName)->pluck('month','id')->all();

        // $month = Month::orderBy('id','asc')->pluck('month','id')->all();

       return view('pages.scorecard.editScoreCard',compact('score','scoreImage','month'));
  }

  public function phyScoreCardView($id)
  {
        $score = ScoreCard::where('id',$id)->first();
        $user = User::where('id',$score['userId'])->first();
        $scoreImage = ScoreCardImage::where('scorecardId',$id)->get();
        return view('pages.scorecard.viewScoreCard',compact('score','scoreImage','user'));
  }

  public function phyScoreCardUpdate($id,Request $request)
  {
     $rules = array(
        'monthId' => 'required',
        'year'=>'required',
        'scorecardImage'=>'required',
        );

    $validator = Validator::make($request->all(), $rules);

    if ($validator->fails()) {
        return redirect()->back()
        ->withErrors($validator)
        ->withInput();

    }
    $uid = $request['userId'];
    $scorecardImage = $request['scorecardImage'];
    $data = $request->all();

       $data = $request->except('scorecardImage','_method','_token');
    // dd($data);

         if($request->hasFile('scorecardImage'))
         {
                foreach ($scorecardImage as $image) {
                      
                      $extension = $image->getClientOriginalExtension();

                      $destinationpath = public_path().'/upload/scorecard';
                      $filename = 'scorecard' . '_' .rand (1,9999). '.' . $extension;
                      $move = $image->move($destinationpath, $filename);

                      $dishesdata['scorecardId'] = $id;
                      $dishesdata['scorecardImage'] ='upload/scorecard/'. $filename;
                      // dd($dishesdata['scorecardImage']);

                      $dishimage = new ScoreCardImage();
                      $dishimage->fill($dishesdata);
                      $dishimage->save();

                }

        }
        // dd($data);
        $updateProfile = ScoreCard::where('id', '=', $id)->update($data);

      return redirect('admin/users/scorecard/'.$uid);  
  }

  public function phyScoreCardImageRemove(Request $request,$id)
  {
     $data = $request->all();
     // dd($request['chekImage']);
        $getimage = ScoreCardImage::whereIn('id',$request['chekImage'])->get();
           
        if($getimage !=""){
            foreach ($getimage as $key) {
                $explodeimage = explode('/', $key['scorecardImage']);
                $filename = public_path().'/upload/scorecard/'.$explodeimage[2];
                \File::delete($filename); 
            }
        }
        $remove = ScoreCardImage::whereIn('id',$request['chekImage'])->delete();
        return Redirect::to('admin/users/scorecard/view/'.$id);
     
  }

  public function phyScoreCardRemove(Request $request,$id)
  {
      $data = $request->all();

      $getimage = ScoreCardImage::whereIn('scorecardId',$request['scorecard'])->get();
        if($getimage !=""){
            foreach ($getimage as $key) {
                $explodeimage = explode('/', $key['scorecardImage']);
                $filename = public_path().'/upload/scorecard/'.$explodeimage[2];
                \File::delete($filename); 
            }
        }

        $remove = ScoreCard::whereIn('id',$request['scorecard'])->delete();
        return Redirect::to('admin/users/scorecard/'.$id);
  }

  public function phySCoreCardSearch(Request $request)
  {
      $data = $request->all();
      $userId = $data['userId'];
      $month = $data['search'][0];
      $year =$data['search'][1];

      $searchScorecard = ScoreCard::month()->where('scorecards.userId',$userId);
        if(!empty($month)){
            $searchScorecard = $searchScorecard->where('month.month', 'LIKE', $month . '%');
        }

        if(!empty($year)){
            $searchScorecard = $searchScorecard->where('year', 'LIKE', $year . '%');
        }
        $searchScorecard = $searchScorecard->orderBy('scorecards.id', 'DESC')->get();
      
      $data = $searchScorecard;

        if (count($data))
            return [
                'value' => $data,
                'status' => TRUE
            ];
        else
            return [
                'value' => 'No result Found',
                'status' => FALSE
            ];
  }
/*Physicain Scorecard End*/



     /*migrate data*/
     public function migratedata(){
        /*get organization column*/
        $user_organization = User::whereIn('roll',[2,3,4])->select('id','organization','name','roll')->get();
        
        foreach ($user_organization as $row) {
            echo $row->id .' '.$row->name . ' '. $row->organization . ' '. $row->roll  .'<br/>';

             /*check existing data*/
             $user_clients = userClients::where('userId',$row->id)->first();
             echo $user_clients;
             if($user_clients != "" && $row->organization != "")
             {
                 $userclient_insert = array('userId' => $row->id,
                                            'clientId'=> $row->organization);
                 $newclient = new userClients();
                 $newclient->fill($userclient_insert);
                 $newclient->save();
             }
            
        }


     }

}




