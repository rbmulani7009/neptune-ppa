<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\manufacturers;

use App\order;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Input;

use Auth;
use App\user;
use App\device;
use Response;

class marketshare extends Controller
{
    public function index(Request $request)
	{
		$pagesize = $request->get('pagesize');
 		if ($pagesize == "") {
             $pagesize = 10;
         }
 		$count_orders = order::where('status','=','Complete')->count();
 		if(Auth::user()->roll == 2)
 		{
 				$userid = Auth::user()->id;
 				$organization = user::where('id','=',$userid)->value('organization');
 			
				$count_orders = order::join('users','users.id','=','orders.orderby')
								->where('users.organization','=',$organization)
								->where('orders.status','=','Complete')
								->count();
								
 				$marketshares = manufacturers::leftJoin('orders','orders.manufacturer_name','=','manufacturers.id')
								->join('users','users.id','=','orders.orderby')
								->where('users.organization','=',$organization)
								->select('manufacturers.manufacturer_name',DB::raw('count(orders.manufacturer_name)*100/'.$count_orders.' as percentage'),DB::raw('count(orders.manufacturer_name) as no_of_order'))
								->groupBy('orders.manufacturer_name')
								->where('orders.status','=','Complete')
								->paginate($pagesize);
 		}
 		else if(Auth::user()->roll == 1)
 		{
 				$marketshares = manufacturers::join('orders','orders.manufacturer_name','=','manufacturers.id')
								->select('manufacturers.manufacturer_name',DB::raw('count(orders.manufacturer_name)*100/'.$count_orders.' as percentage'),DB::raw('count(orders.manufacturer_name) as no_of_order'))
								->groupBy('orders.manufacturer_name')
								->where('orders.status','=','Complete')
								->paginate($pagesize);
 				
 		}
 		else if(Auth::user()->roll == 5)
 		{
 			$userid = Auth::user()->id;
 			$organization = user::where('id','=',$userid)->value('organization');
 			$count_orders = order::join('users','users.id','=','orders.orderby')
							->where('orders.manufacturer_name','=',$organization)
							->where('orders.status','=','Complete')
							->count();
							
 			$marketshares = manufacturers::leftJoin('orders','orders.manufacturer_name','=','manufacturers.id')
							->where('manufacturers.id','=',$organization)
							->select('manufacturers.manufacturer_name',DB::raw('count(orders.manufacturer_name)*100/'.$count_orders.' as percentage'),DB::raw('count(orders.manufacturer_name) as no_of_order'))
							->groupBy('orders.manufacturer_name')
							->where('orders.status','=','Complete')
							->where('orders.is_delete','=',0)
							->paginate($pagesize);
 		}
 	
 	$count = count($marketshares);
 		return view('pages.marketshare',compact('marketshares','count','pagesize'));
	
	}
	
	
	public function search()
	{
		$fieldname = Input::get('fieldName');
		$fieldvalue = Input::get('value');
		
		$count_orders = order::where('status','=','Complete')->where('orders.is_delete','=',0)->count();
		if(Auth::user()->roll == 2)
		{
				$userid = Auth::user()->id;
				$organization = user::where('id','=',$userid)->value('organization');
				$count_orders = order::join('users','users.id','=','orders.orderby')
								->where('users.organization','=',$organization)
								->where('orders.is_delete','=',0)
								->where('orders.status','=','Complete')
								->count();
								
				$search_marketshares = manufacturers::join('orders','orders.manufacturer_name','=','manufacturers.id')
										->join('users','users.id','=','orders.orderby')
										->where('users.organization','=',$organization)
										->select('manufacturers.manufacturer_name',DB::raw('count(orders.manufacturer_name)*100/'.$count_orders.' as percentage'),DB::raw('count(orders.manufacturer_name) as no_of_order'))
										->groupBy('orders.manufacturer_name')
										->where('orders.status','=','Complete')
										->where('orders.is_delete','=',0)
										->where($fieldname,'LIKE',$fieldvalue.'%')
										->get();
		}
		else if(Auth::user()->roll == 1)
		{
				
				$search_marketshares = manufacturers::join('orders','orders.manufacturer_name','=','manufacturers.id')
										->select('manufacturers.manufacturer_name',DB::raw('count(orders.manufacturer_name)*100/'.$count_orders.' as percentage'),DB::raw('count(orders.manufacturer_name) as no_of_order'))
										->groupBy('orders.manufacturer_name')
										->where('orders.status','=','Complete')
										->where('orders.is_delete','=',0)
										->where($fieldname,'LIKE',$fieldvalue.'%')
										->get();
		}
		else if(Auth::user()->roll == 5)
		{
			$userid = Auth::user()->id;
			$organization = user::where('id','=',$userid)->value('organization');
			$count_orders = order::join('users','users.id','=','orders.orderby')
							->where('orders.manufacturer_name','=',$organization)
							->where('orders.is_delete','=',0)
							->where('orders.status','=','Complete')
							->count();
							
			$search_marketshares = manufacturers::join('orders','orders.manufacturer_name','=','manufacturers.id')
									->where('manufacturers.id','=',$organization)
									->select('manufacturers.manufacturer_name',DB::raw('count(orders.manufacturer_name)*100/'.$count_orders.' as percentage'),DB::raw('count(orders.manufacturer_name) as no_of_order'))
									->groupBy('orders.manufacturer_name')
									->where('orders.status','=','Complete')
									->where('orders.is_delete','=',0)
									->where($fieldname,'LIKE',$fieldvalue.'%')
									->get();
		}
		
		$data = $search_marketshares;
		
		if(count($data))
			return [
						'value' => $data,
						'status' => TRUE
				   ];
		else
			return [
						'value'=>'No Result Found',
						'status' => FALSE
				   ];
	}
	
	
	public function export()
	{
		if(Auth::user()->roll == 5)
		{
			$userid = Auth::user()->id;
			$organization = user::where('id','=',$userid)->value('organization');
			$count_orders = order::join('users','users.id','=','orders.orderby')
							->where('orders.manufacturer_name','=',$organization)
							->where('orders.is_delete','=',0)
							->where('orders.status','=','Complete')
							->count();
							
			$marketshares = manufacturers::join('orders','orders.manufacturer_name','=','manufacturers.id')
							->where('manufacturers.id','=',$organization)
							->select('manufacturers.manufacturer_name',DB::raw('count(orders.manufacturer_name)*100/'.$count_orders.' as percentage'),DB::raw('count(orders.manufacturer_name) as no_of_order'))
							->groupBy('orders.manufacturer_name')
							->where('orders.is_delete','=',0)
							->where('orders.status','=','Complete')
							->get();
		}
		else if(Auth::user()->roll == 2)
		{
				$userid = Auth::user()->id;
				$organization = user::where('id','=',$userid)->value('organization');
				$count_orders = order::join('users','users.id','=','orders.orderby')
								->where('users.organization','=',$organization)
								->where('orders.is_delete','=',0)
								->where('orders.status','=','Complete')->count();
								
				$marketshares = manufacturers::join('orders','orders.manufacturer_name','=','manufacturers.id')
								->join('users','users.id','=','orders.orderby')
								->where('users.organization','=',$organization)
								->select('manufacturers.manufacturer_name',DB::raw('count(orders.manufacturer_name)*100/'.$count_orders.' as percentage'),DB::raw('count(orders.manufacturer_name) as no_of_order'))
								->groupBy('orders.manufacturer_name')
								->where('orders.is_delete','=',0)
								->where('orders.status','=','Complete')->get();
		}
		else
		{
		
		$count_orders = order::where('status','=','Complete')->where('is_delete','=',0)->count();
		$marketshares = manufacturers::join('orders','orders.manufacturer_name','=','manufacturers.id')
						->select('manufacturers.manufacturer_name',DB::raw('count(orders.manufacturer_name)*100/'.$count_orders.' as percentage'),DB::raw('count(orders.manufacturer_name) as no_of_order'))
						->groupBy('orders.manufacturer_name')
						->where('orders.is_delete','=',0)
						->where('orders.status','=','Complete')
						->get();
		
		}
						
		$filename = "marketshare.csv";
			$handle = fopen($filename, 'w+');
			fputcsv($handle, array('Manufacturer Name', 'Marketshare','No. Of Orders'));
		
			foreach($marketshares as $row) {
				fputcsv($handle, array($row['manufacturer_name'], $row['percentage'], $row['no_of_order']));
			}
		
			fclose($handle);
		
			$headers = array(
				'Content-Type' => 'text/csv',
			);
		
			return Response::download($filename, 'marketshare.csv', $headers);
			return Redirect::to('admin/marketshare');
	}
}
