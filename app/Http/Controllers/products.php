<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\device;
use App\order;
use Illuminate\Support\Facades\Input;
use Auth;
use Illuminate\Support\Facades\Redirect;
use App\user;
use Mail;
use App\device_custom_field;
use App\clients;
use Illuminate\Support\Facades\DB;
use App\category;
use App\client_price;
use Session;
use App\Devicefeatures;
use App\Clientcustomfeatures;
use Carbon\Carbon;
use App\customContact;
use App\Survey;
use App\SurveyAnswer;
use App\userClients;


class products extends Controller {

	public function newdevice($id) {
		$categoryid =  $id;
       // $user_organization = Auth::user()->organization;


		/*get selected clientdetails*/
		$clientdetails =  session('details');
		$clients = $clientdetails['clients'];

		$clientname = clients::where('id', '=', $clients)->value('client_name');
		$entry_levels = device::leftjoin('client_price', 'client_price.device_id', '=', 'device.id')
		->leftjoin('device_features','device_features.device_id','=','device.id')
		->join('manufacturers', 'manufacturers.id', '=', 'device.manufacturer_name')
		->select('device.id','device.device_name','device.device_image','device.longevity','device.shock','device.size','device.research','device.website_page as site_info','device.url','device.overall_value','device_features.longevity_check','device_features.shock_check','device_features.size_check','device_features.research_check','device_features.siteinfo_check','device_features.overall_value_check', 'client_price.device_id', 'client_price.unit_cost', 'client_price.bulk_unit_cost', 'client_price.bulk_unit_cost_check', 'client_price.bulk', 'client_price.bulk_check', 'client_price.cco_discount', 'client_price.cco_discount_check', 'client_price.unit_rep_cost', 'client_price.unit_rep_cost_check', 'client_price.unit_repless_discount', 'client_price.unit_repless_discount_check', 'client_price.system_cost', 'client_price.system_cost_check', 'client_price.bulk_system_cost', 'client_price.bulk_system_cost', 'client_price.bulk_system_cost_check', 'client_price.system_repless_cost', 'client_price.system_repless_cost_check', 'client_price.system_repless_discount', 'client_price.system_repless_discount_check', 'client_price.reimbursement', 'client_price.reimbursement_check','client_price.system_bulk','client_price.system_bulk_check', 'client_price.order_email', 'manufacturers.manufacturer_logo')
		->where('client_price.client_name', '=', $clients)
		->where('device_features.client_name','=',$clients)
		->where('device.category_name', '=', $id)
		->where('device.level_name', '=', 'Entry Level')
		->where('device.is_delete', '=', 0)
		->where('device.status', '=', 'Enabled')
		->where('client_price.system_cost_check', '=', 'True')
		->where('client_price.is_delete', '=', 0)
		->get();

		$productid = [];
		$exclusivecheck = [];
		$sizecheck = [];
		$longevitycheck = [];
		$shockcheck = [];
		$bulkcheck = [];
		$siteinfocheck = [];
		$researchcheck = [];
		$replessdiscountcheck = [];
		$replesscostcheck = [];

		foreach ($entry_levels as $entry_level) {


			if($entry_level->shock != "")
			{
				$entry_level->shock = explode('/', $entry_level->shock);
				$entry_level->shock[0] == "" ? "0" : $entry_level->shock[0];
				$entry_level->shock[0] == "" ? "0" : $entry_level->shock[1];
			}

			if($entry_level->size != "")
			{
				$entry_level->size = explode('/', $entry_level->size);
				$entry_level->size[0] == "" ? "0" : $entry_level->size[0];
				$entry_level->size[0] == "" ? "0" : $entry_level->size[1];
			}

			$bulk_count = Order::join('users', 'users.id', '=', 'orders.orderby')
			->where('orders.model_name', '=', $entry_level->device_name)
			->where('users.organization', '=', $clients)
			->where('orders.status', '!=', 'Cancelled')
			->where('orders.bulk_check','=','True')
			->where('orders.system_cost','!=',"")
			->count();

			$bulk = $entry_level->system_bulk;
			$entry_level->remain_bulk = $bulk - $bulk_count;
			$deviceid = $entry_level->id;
			$entry_level->custom_fields = device_custom_field::leftJoin('client_custom_field','client_custom_field.c_id','=','device_custom_field.id')
			->select('device_custom_field.id','device_custom_field.field_name','device_custom_field.field_value','client_custom_field.field_check')
			->where('client_custom_field.device_id','=',$deviceid)
			->where('client_custom_field.client_name','=',$clients)->get();


			$system_cost = $entry_level->system_cost;

			if ($entry_level->bulk_system_cost_check == "True" && $entry_level->system_bulk_check == "True" && $bulk > $bulk_count) 			{

				$system_cost -= ($system_cost * $entry_level->bulk_system_cost) / 100;
				$entry_level->system_cost = $system_cost;
			}


			$entry_level->repless_cost = "-";

			if ($entry_level->system_repless_discount_check == "True") {
				$system_cost -= ($system_cost * $entry_level->system_repless_discount) / 100;
				$entry_level->repless_cost = $system_cost;
			}

			if ($entry_level->system_repless_cost_check == "True") {
				$system_cost -= $entry_level->system_repless_cost;
				$entry_level->repless_cost = $system_cost;
			}


			$replessdiscountcheck[] = $entry_level->system_repless_discount_check;
			$replesscostcheck[] = $entry_level->system_repless_cost_check;  
			$exclusivecheck[] = $entry_level->exclusive_check;
			$sizecheck[] = $entry_level->size_check;
			$longevitycheck[] = $entry_level->longevity_check;
			$shockcheck[] = $entry_level->shock_check;
			$bulkcheck[] = $entry_level->system_bulk_check;
			$siteinfocheck[] = $entry_level->siteinfo_check;
			$researchcheck[] = $entry_level->research_check;



			
			$productid[] = $entry_level->id; 
		}


		$entry_custom_fields = device_custom_field::whereIn('device_id', $productid)
		->get();


		$replessdiscount_option = "False";
		$replesscost_option = "False";
		$exclusive_option =  "False";
		$longevity_option = "False";
		$shock_option = "False";
		$siteinfo_option = "False";
		$size_option = "False";
		$bulk_option = "False";
		$research_option = "False";



		if((count(array_unique($replessdiscountcheck)) == 1 && end($replessdiscountcheck) == "True") || count(array_unique($replessdiscountcheck)) > 1)
		{
			$replessdiscount_option = "True";

		}	

		if((count(array_unique($replesscostcheck)) == 1 && end($replesscostcheck) == "True") || count(array_unique($replesscostcheck)) > 1)
		{

			$replesscost_option = "True";
		}

		if( (count(array_unique($sizecheck)) == 1 && end($sizecheck) == "True") || count(array_unique($sizecheck)) > 1)
		{
			$size_option = "True";


		} 

		if((count(array_unique($exclusivecheck)) == 1 && end($exclusivecheck) == "True")  || count(array_unique($exclusivecheck)) > 1)
		{
			$exclusive_option = "True";
		}

		if((count(array_unique($longevitycheck)) == 1 && end($longevitycheck) == "True" ) || count(array_unique($longevitycheck)) > 1)
		{
			$longevity_option = "True";

		}

		if((count(array_unique($shockcheck)) == 1 && end($shockcheck) == "True" ) || count(array_unique($shockcheck)) > 1)
		{
			$shock_option = "True";
		}

		if((count(array_unique($bulkcheck)) == 1 && end($bulkcheck) == "True") || count(array_unique($bulkcheck)) > 1)
		{
			$bulk_option = "True";
		}

		if((count(array_unique($siteinfocheck)) == 1 && end($siteinfocheck) == "True")  || count(array_unique($siteinfocheck)) > 1)
		{
			$siteinfo_option = "True";
		}

		if((count(array_unique($researchcheck)) == 1 && end($researchcheck) == "True")  || count(array_unique($researchcheck)) > 1)
		{
			$research_option = "True";
		}

		

		$advance_levels = device::join('client_price', 'client_price.device_id', '=', 'device.id')
		->leftjoin('device_features','device_features.device_id','=','device.id')
		->join('manufacturers', 'manufacturers.id', '=', 'device.manufacturer_name')
		->select('device.id','device.device_name','device.device_image','device.longevity','device.shock','device.size','device.research','device.website_page as site_info','device.url','device.overall_value','device_features.longevity_check','device_features.shock_check','device_features.size_check','device_features.research_check','device_features.siteinfo_check','device_features.overall_value_check', 'client_price.device_id', 'client_price.unit_cost', 'client_price.bulk_unit_cost', 'client_price.bulk_unit_cost_check', 'client_price.bulk', 'client_price.bulk_check', 'client_price.cco_discount', 'client_price.cco_discount_check', 'client_price.unit_rep_cost', 'client_price.unit_rep_cost_check', 'client_price.unit_repless_discount', 'client_price.unit_repless_discount_check', 'client_price.system_cost', 'client_price.system_cost_check', 'client_price.bulk_system_cost', 'client_price.bulk_system_cost', 'client_price.bulk_system_cost_check', 'client_price.system_repless_cost', 'client_price.system_repless_cost_check', 'client_price.system_repless_discount', 'client_price.system_repless_discount_check', 'client_price.reimbursement', 'client_price.reimbursement_check','client_price.system_bulk','client_price.system_bulk_check', 'client_price.order_email', 'manufacturers.manufacturer_logo')
		->where('client_price.client_name', '=', $clients)
		->where('device_features.client_name','=',$clients)
		->where('device.category_name', '=', $id)
		->where('device.level_name', '=', 'Advanced Level')
		->where('device.is_delete', '=', 0)
		->where('device.status', '=', 'Enabled')
		->where('client_price.system_cost_check', '=', 'True')
		->where('client_price.is_delete', '=', 0)
		->get();
		$advance_productid = [];
		$advanceexclusivecheck = [];			
		$advancesizecheck = [];
		$advancelongevitycheck = [];
		$advanceshockcheck = [];
		$advancebulkcheck = [];
		$advancesiteinfocheck = [];
		$advancereplessdiscountcheck = [];
		$advancereplesscostcheck = [];
		$advanceresearchcheck = [];

		foreach ($advance_levels as $advance_level) {

			if($advance_level->shock != "")
			{
				$advance_level->shock = explode('/', $advance_level->shock);
				$advance_level->shock[0] == "" ? "0" : $advance_level->shock[0];
				$advance_level->shock[0] == "" ? "0" : $advance_level->shock[1];
			}


			if($advance_level->size != "")
			{
				$advance_level->size = explode('/', $advance_level->size);
				$advance_level->size[0] == "" ? "0" : $advance_level->size[0];
				$advance_level->size[0] == "" ? "0" : $advance_level->size[1];
			}




			$bulk_count = Order::join('users', 'users.id', '=', 'orders.orderby')
			->where('orders.model_name', '=', $advance_level->device_name)
			->where('users.organization', '=', $clients)
			->where('orders.status', '!=', 'Cancelled')
			->where('orders.bulk_check','=','True')
			->where('orders.system_cost','!=',"")
			->count();

			$bulk = $advance_level->system_bulk;
			$advance_level->remain_bulk = $bulk - $bulk_count;
			$deviceid = $advance_level->id;
			$advance_level->custom_fields = device_custom_field::leftJoin('client_custom_field','client_custom_field.c_id','=','device_custom_field.id')
			->select('device_custom_field.id','device_custom_field.field_name','device_custom_field.field_value','client_custom_field.field_check')
			->where('client_custom_field.device_id','=',$deviceid)
			->where('client_custom_field.client_name','=',$clients)->get();

			$system_cost = $advance_level->system_cost;

			if ($advance_level->bulk_system_cost_check == "True" && $advance_level->system_bulk_check == "True" && $bulk > $bulk_count) {

				$system_cost -= ($system_cost * $advance_level->bulk_system_cost) / 100;
				$advance_level->system_cost = $system_cost;
			}


			$advance_level->repless_cost = "-";

			if ($advance_level->system_repless_discount_check == "True") {

				$system_cost -= ($system_cost * $advance_level->system_repless_discount) / 100;
				$advance_level->repless_cost = $system_cost;
			}

			if ($advance_level->system_repless_cost_check == "True") {
				$system_cost -= $advance_level->system_repless_cost;
				$advance_level->repless_cost = $system_cost;
			}

			$advancereplessdiscountcheck[] = $advance_level->system_repless_discount_check;
			$advancereplesscostcheck[] = $advance_level->system_repless_cost_check;  
			$advanceexclusivecheck[] = $advance_level->exclusive_check;
			$advancesizecheck[] = $advance_level->size_check;
			$advancelongevitycheck[] = $advance_level->longevity_check;
			$advanceshockcheck[] = $advance_level->shock_check;
			$advancebulkcheck[] = $advance_level->system_bulk_check;
			$advancesiteinfocheck[] = $advance_level->siteinfo_check;
			$advanceresearchcheck[] = $advance_level->research_check;

			$advance_productid[] = $advance_level->id; 
		}


		$advance_custom_fields = device_custom_field::whereIn('device_id', $advance_productid)
		->get();


		$advance_replessdiscount_option = "False";
		$advance_replesscost_option = "False";
		$advance_exclusive_option =  "False";
		$advance_longevity_option = "False";
		$advance_shock_option = "False";
		$advance_siteinfo_option = "False";
		$advance_size_option = "False";
		$advance_bulk_option = "False";
		$advance_research_option = "False";


		if((count(array_unique($advancereplessdiscountcheck)) == 1 && end($advancereplessdiscountcheck) == "True") || count(array_unique($advancereplessdiscountcheck)) > 1)
		{
			$advance_replessdiscount_option = "True";

		}	

		if((count(array_unique($advancereplesscostcheck)) == 1 && end($advancereplesscostcheck) == "True") || count(array_unique($advancereplesscostcheck)) > 1)
		{

			$advance_replesscost_option = "True";
		}

		if(count(array_unique($advancesizecheck)) == 1 && end($advancesizecheck) == "True" || count(array_unique($advancesizecheck)) > 1)
		{
			$advance_size_option = "True";
		}

		if(count(array_unique($advanceexclusivecheck)) == 1 && end($advanceexclusivecheck) == "True" || count(array_unique($advanceexclusivecheck)) > 1)
		{
			$advance_exclusive_option = "True";
		}


		if(count(array_unique($advancelongevitycheck)) == 1 && end($advancelongevitycheck) == "True" || count(array_unique($advancelongevitycheck)) > 1)
		{
			$advance_longevity_option = "True";
		}

		if(count(array_unique($advanceshockcheck)) == 1 && end($advanceshockcheck) == "True" || count(array_unique($advanceshockcheck)) > 1)
		{
			$advance_shock_option = "True";
		}


		if(count(array_unique($advancebulkcheck)) == 1 && end($advancebulkcheck) == "True" || count(array_unique($advancebulkcheck)) > 1)
		{
			$advance_bulk_option = "True";
		}


		if(count(array_unique($advancesiteinfocheck)) == 1 && end($advancesiteinfocheck) == "True" || count(array_unique($advancesiteinfocheck)) > 1)
		{
			$advance_siteinfo_option = "True";
		}

		if(count(array_unique($advanceresearchcheck)) == 1 && end($advanceresearchcheck) == "True" || count(array_unique($advanceresearchcheck)) > 1)
		{
			$advance_research_option = "True";
		}

		
		

		$devicetype = "New Device";
		$categoryname = category::where('id', '=', $id)->value('category_name');
		$updatedate = client_price::orderby(DB::raw('date(is_updated)'), 'desc')
		->where('client_name','=',$clients)
		->value('is_updated');
		$updatedate = Carbon::parse($updatedate)->format('m/d/Y');


		if(count($entry_levels) == "0" &&  count($advance_levels) == "0") 
		{
			if (strpos(url()->previous(), 'changeout') !== false) {
				Session::flash('message', 'There is no device in this category!');
				return Redirect::to('changeout/mainmenu');

			}
			return Redirect::to('changeout/devices/'.$categoryid);
			
		}
		
		return view('pages/frontend/products', compact('entry_levels', 'advance_levels', 'clientname', 'categoryname', 'devicetype', 'system_cost', 'repless_cost', 'updatedate','size_option','exclusive_option','bulk_option','longevity_option','shock_option','siteinfo_option','advance_size_option','advance_exclusive_option','advance_bulk_option','advance_longevity_option','advance_shock_option','advance_siteinfo_option','entry_custom_fields','advance_custom_fields','replesscost_option','replessdiscount_option','advance_replesscost_option','advance_replessdiscount_option','research_option','advance_research_option'));
		
	}

	public function changeout($id) {

		$categoryid = $id;
        //$user_organization = Auth::user()->organization;


		/*get selected clientdetails*/
		$clientdetails =  session('details');
		$clients = $clientdetails['clients'];

		$clientname = clients::where('id', '=', $clients)->value('client_name');


		$entry_levels = device::join('client_price', 'client_price.device_id', '=', 'device.id')
		->leftjoin('device_features','device_features.device_id','=','device.id')
		->join('manufacturers', 'manufacturers.id', '=', 'device.manufacturer_name')
		->select('device.id','device.device_name','device.device_image','device.longevity','device.shock','device.size','device.research','device.website_page as site_info','device.url','device.overall_value','device_features.longevity_check','device_features.shock_check','device_features.size_check','device_features.research_check','device_features.siteinfo_check','device_features.overall_value_check', 'client_price.device_id', 'client_price.unit_cost', 'client_price.bulk_unit_cost', 'client_price.bulk_unit_cost_check', 'client_price.bulk', 'client_price.bulk_check', 'client_price.cco_discount', 'client_price.cco_discount_check','client_price.cco','client_price.cco_check', 'client_price.unit_rep_cost', 'client_price.unit_rep_cost_check', 'client_price.unit_repless_discount', 'client_price.unit_repless_discount_check', 'client_price.system_cost', 'client_price.system_cost_check', 'client_price.bulk_system_cost', 'client_price.bulk_system_cost', 'client_price.bulk_system_cost_check', 'client_price.system_repless_cost', 'client_price.system_repless_cost_check', 'client_price.system_repless_discount', 'client_price.system_repless_discount_check', 'client_price.reimbursement', 'client_price.reimbursement_check','client_price.system_bulk','client_price.system_bulk_check', 'client_price.order_email', 'manufacturers.manufacturer_logo')
		->where('client_price.client_name', '=', $clients)
		->where('device_features.client_name','=',$clients)
		->where('device.category_name', '=', $id)
		->where('device.level_name', '=', 'Entry Level')
		->where('device.is_delete', '=', 0)
		->where('device.status', '=', 'Enabled')
		->where('client_price.unit_cost_check', '=', 'True')
		->get();



		$productid =[];
		$sizecheck = [];
		$exclusivecheck = [];
		$longevitycheck = [];
		$shockcheck = [];
		$bulkcheck = [];
		$siteinfocheck = [];
		$replessdiscountcheck = [];
		$replesscostcheck = [];
		$ccocostcheck = [];
		$ccodiscountcheck = [];
		$researchcheck = [];

		foreach ($entry_levels as $entry_level) {
			
			
			if($entry_level->shock != "")
			{
				$entry_level->shock = explode('/', $entry_level->shock);
				$entry_level->shock[0] == "" ? "0" : $entry_level->shock[0];
				$entry_level->shock[0] == "" ? "0" : $entry_level->shock[1];
			}

			if($entry_level->size != "")
			{ 
				$entry_level->size = explode('/', $entry_level->size);
				$entry_level->size[0] == "" ? "0" : $entry_level->size[0];
				$entry_level->size[0] == "" ? "0" : $entry_level->size[1];
			}
			
			$bulk_count = Order::join('users', 'users.id', '=', 'orders.orderby')
			->where('orders.model_name', '=', $entry_level->device_name)
			->where('users.organization', '=', $clients)
			->where('orders.status', '!=', 'Cancelled')
			->where('orders.bulk_check','=','True')
			->where('orders.unit_cost','!=',"")
			->count();

			$bulk = $entry_level->bulk;
			$entry_level->remain_bulk = $bulk - $bulk_count;
			$deviceid = $entry_level->id;
			$entry_level->custom_fields = device_custom_field::leftJoin('client_custom_field','client_custom_field.c_id','=','device_custom_field.id')
			->select('device_custom_field.id','device_custom_field.field_name','device_custom_field.field_value','client_custom_field.field_check')
			->where('client_custom_field.device_id','=',$deviceid)
			->where('client_custom_field.client_name','=',$clients)->get();

			$unit_cost = $entry_level->unit_cost;

			if ($entry_level->bulk_unit_cost_check == "True" && $entry_level->bulk_check == "True" && $bulk > $bulk_count) {

				$unit_cost -= ($entry_level->unit_cost * $entry_level->bulk_unit_cost) / 100;

				$entry_level->unit_cost = $unit_cost;
			}

			if ($entry_level->cco_discount_check == "True") {
				$unit_cost -= ($unit_cost * $entry_level->cco_discount) / 100;
				$entry_level->cco_discount = $unit_cost;
			}
			else if ($entry_level->cco_check == "True") {
				$unit_cost -= $entry_level->cco;
				$entry_level->cco_discount = $unit_cost;
			}	
			else {
				$entry_level->cco_discount = "-";
			}

			$entry_level->unit_repless_cost = "-";

			if ($entry_level->unit_repless_discount_check == "True") {

				$unit_cost -= ($unit_cost * $entry_level->unit_repless_discount ) / 100;
				$entry_level->unit_repless_cost = $unit_cost;
			}

			if ($entry_level->unit_rep_cost_check == "True") {
				$unit_cost -= $entry_level->unit_rep_cost;
				$entry_level->unit_repless_cost = $unit_cost;
			}
			
			
			$exclusivecheck[] = $entry_level->exclusive_check;
			$sizecheck[] = $entry_level->size_check;
			$longevitycheck[] = $entry_level->longevity_check;
			$shockcheck[] = $entry_level->shock_check;
			$bulkcheck[] = $entry_level->bulk_check;
			$siteinfocheck[] = $entry_level->siteinfo_check;
			$replessdiscountcheck[] = $entry_level->unit_repless_discount_check;
			$replesscostcheck[] = $entry_level->unit_rep_cost_check; 
			$ccocostcheck[] = $entry_level->cco_check;
			$ccodiscountcheck[] = $entry_level->cco_discount_check;
			$researchcheck[] = $entry_level->research_check;

			$productid[] = $entry_level->id;


		}

		$entry_custom_fields = device_custom_field::whereIn('device_id', $productid)
		->get();

		$replessdiscount_option = "False";
		$replesscost_option = "False";
		$exclusive_option =  "False";
		$longevity_option = "False";
		$shock_option = "False";
		$siteinfo_option = "False";
		$size_option = "False";
		$bulk_option = "False";
		$ccocost_option = "False";
		$ccodiscount_option = "False";
		$research_option = "False";



		if((count(array_unique($ccodiscountcheck)) == 1 && end($ccodiscountcheck) == "True") || count(array_unique($ccodiscountcheck)) > 1)
		{
			$ccodiscount_option = "True";

		}	

		if((count(array_unique($ccocostcheck)) == 1 && end($ccocostcheck) == "True") || count(array_unique($ccocostcheck)) > 1)
		{

			$ccocost_option = "True";
		}


		if((count(array_unique($replessdiscountcheck)) == 1 && end($replessdiscountcheck) == "True") || count(array_unique($replessdiscountcheck)) > 1)
		{
			$replessdiscount_option = "True";

		}	

		if((count(array_unique($replesscostcheck)) == 1 && end($replesscostcheck) == "True") || count(array_unique($replesscostcheck)) > 1)
		{

			$replesscost_option = "True";
		}

		if( (count(array_unique($sizecheck)) == 1 && end($sizecheck) == "True") || count(array_unique($sizecheck)) > 1)
		{
			$size_option = "True";
		} 

		if((count(array_unique($exclusivecheck)) == 1 && end($exclusivecheck) == "True")  || count(array_unique($exclusivecheck)) > 1)
		{
			$exclusive_option = "True";
		}

		if((count(array_unique($longevitycheck)) == 1 && end($longevitycheck) == "True" ) || count(array_unique($longevitycheck)) > 1)
		{
			$longevity_option = "True";
		}

		if((count(array_unique($shockcheck)) == 1 && end($shockcheck) == "True" ) || count(array_unique($shockcheck)) > 1)
		{
			$shock_option = "True";
		}

		if((count(array_unique($bulkcheck)) == 1 && end($bulkcheck) == "True") || count(array_unique($bulkcheck)) > 1)
		{
			$bulk_option = "True";
		}

		if((count(array_unique($siteinfocheck)) == 1 && end($siteinfocheck) == "True")  || count(array_unique($siteinfocheck)) > 1)
		{
			$siteinfo_option = "True";
		}

		if((count(array_unique($researchcheck)) == 1 && end($researchcheck) == "True")  || count(array_unique($researchcheck)) > 1)
		{
			$research_option = "True";
		}
		
		
		
		$advance_levels = device::join('client_price', 'client_price.device_id', '=', 'device.id')
		->leftjoin('device_features','device_features.device_id','=','device.id')
		->join('manufacturers', 'manufacturers.id', '=', 'device.manufacturer_name')
		->select('device.id','device.device_name','device.device_image','device.longevity','device.shock','device.size','device.research','device.website_page as site_info','device.url','device.overall_value','device_features.longevity_check','device_features.shock_check','device_features.size_check','device_features.research_check','device_features.siteinfo_check','device_features.overall_value_check', 'client_price.device_id', 'client_price.unit_cost', 'client_price.bulk_unit_cost', 'client_price.bulk_unit_cost_check', 'client_price.bulk', 'client_price.bulk_check','client_price.cco','client_price.cco_check', 'client_price.cco_discount', 'client_price.cco_discount_check', 'client_price.unit_rep_cost', 'client_price.unit_rep_cost_check', 'client_price.unit_repless_discount', 'client_price.unit_repless_discount_check', 'client_price.system_cost', 'client_price.system_cost_check', 'client_price.bulk_system_cost', 'client_price.bulk_system_cost', 'client_price.bulk_system_cost_check', 'client_price.system_repless_cost', 'client_price.system_repless_cost_check', 'client_price.system_repless_discount', 'client_price.system_repless_discount_check', 'client_price.reimbursement', 'client_price.reimbursement_check','client_price.system_bulk','client_price.system_bulk_check', 'client_price.order_email', 'manufacturers.manufacturer_logo')
		->where('client_price.client_name', '=', $clients)
		->where('device_features.client_name','=',$clients)
		->where('device.category_name', '=', $id)
		->where('device.level_name', '=', 'Advanced Level')
		->where('device.is_delete', '=', 0)
		->where('device.status', '=', 'Enabled')
		->where('client_price.unit_cost_check', '=', 'True')
		->get();

		$advance_productid = [];
		$advanceexclusivecheck = [];
		$advancesizecheck = [];
		$advancelongevitycheck = [];
		$advanceshockcheck = [];
		$advancebulkcheck = [];
		$advancesiteinfocheck = [];
		$advancereplesscostcheck = [];
		$advancereplessdiscountcheck = [];
		$advanceccocostcheck = [];
		$advanceccodiscountcheck = [];
		$advanceresearchcheck = [];



		foreach ($advance_levels as $advance_level) {

			if($advance_level->shock != "")
			{
				$advance_level->shock = explode('/', $advance_level->shock);
				$advance_level->shock[0] == "" ? "0" : $advance_level->shock[0];
				$advance_level->shock[0] == "" ? "0" : $advance_level->shock[1];
			}

			if($advance_level->size != "")
			{ 
				$advance_level->size = explode('/', $advance_level->size);
				$advance_level->size[0] == "" ? "0" : $advance_level->size[0];
				$advance_level->size[0] == "" ? "0" : $advance_level->size[1];
			} 
			
			$bulk_count = Order::join('users', 'users.id', '=', 'orders.orderby')
			->where('orders.model_name', '=', $advance_level->device_name)
			->where('users.organization', '=', $clients)
			->where('orders.status', '!=', 'Cancelled')
			->where('orders.unit_cost','!=',"")
			->where('orders.bulk_check','=','True')
			->count();
			$bulk = $advance_level->bulk;

			$advance_level->remain_bulk = $bulk - $bulk_count;
			$deviceid = $advance_level->id;
			$advance_level->custom_fields = device_custom_field::leftJoin('client_custom_field','client_custom_field.c_id','=','device_custom_field.id')
			->select('device_custom_field.id','device_custom_field.field_name','device_custom_field.field_value','client_custom_field.field_check')
			->where('client_custom_field.device_id','=',$deviceid)
			->where('client_custom_field.client_name','=',$clients)->get();

			$unit_cost = $advance_level->unit_cost;

			if ($advance_level->bulk_unit_cost_check == "True" && $advance_level->bulk_check == "True" && $bulk > $bulk_count) {

				$unit_cost -= ($advance_level->unit_cost * $advance_level->bulk_unit_cost) / 100;

				$advance_level->unit_cost = $unit_cost;
			}

			if ($advance_level->cco_discount_check == "True") {
				$unit_cost -= ($unit_cost * $advance_level->cco_discount) / 100;
				$advance_level->cco_discount = $unit_cost;
			} 
			else if ($advance_level->cco_check == "True") {
				$unit_cost -= $advance_level->cco;
				$advance_level->cco_discount = $unit_cost;
			}	
			else {
				$advance_level->cco_discount = "-";
			}

			$advance_level->unit_repless_cost = "-";

			if ($advance_level->unit_repless_discount_check == "True") {

				$unit_cost -= ($unit_cost * $advance_level->unit_repless_discount ) / 100;
				$advance_level->unit_repless_cost = $unit_cost;
			}

			if ($advance_level->unit_rep_cost_check == "True") {
				$unit_cost -= $advance_level->unit_rep_cost;
				$advance_level->unit_repless_cost = $unit_cost;
			}
			
			
			$advanceexclusivecheck[] = $advance_level->exclusive_check;
			$advancesizecheck[] = $advance_level->size_check;
			$advancelongevitycheck[] = $advance_level->longevity_check;
			$advanceshockcheck[] = $advance_level->shock_check;
			$advancebulkcheck[] = $advance_level->bulk_check;
			$advancesiteinfocheck[] = $advance_level->siteinfo_check;
			$advancereplessdiscountcheck[] = $advance_level->unit_repless_discount_check;
			$advancereplesscostcheck[] = $advance_level->unit_rep_cost_check;
			$advanceccocostcheck[] = $advance_level->cco_check;
			$advanceccodiscountcheck[] = $advance_level->cco_discount_check;
			$advanceresearchcheck[] = $advance_level->research_check;
			
			$advance_productid[] = $advance_level->id;
			
		}

		$advance_custom_fields = device_custom_field::whereIn('device_id', $advance_productid)->get();

		$advance_ccodiscount_option = "False";
		$advance_ccocost_option = "False";
		$advance_replessdiscount_option = "False";
		$advance_replesscost_option = "False";
		$advance_exclusive_option =  "False";
		$advance_longevity_option = "False";
		$advance_shock_option = "False";
		$advance_siteinfo_option = "False";
		$advance_size_option = "False";
		$advance_bulk_option = "False";
		$advance_research_option = "False";


		if((count(array_unique($advanceccodiscountcheck)) == 1 && end($advanceccodiscountcheck) == "True") || count(array_unique($advanceccodiscountcheck)) > 1)
		{
			$advance_ccodiscount_option = "True";

		}	

		if((count(array_unique($advanceccocostcheck)) == 1 && end($advanceccocostcheck) == "True") || count(array_unique($advanceccocostcheck)) > 1)
		{

			$advance_ccocost_option = "True";
		}
		if((count(array_unique($advancereplessdiscountcheck)) == 1 && end($advancereplessdiscountcheck) == "True") || count(array_unique($advancereplessdiscountcheck)) > 1)
		{
			$advance_replessdiscount_option = "True";

		}	

		if((count(array_unique($advancereplesscostcheck)) == 1 && end($advancereplesscostcheck) == "True") || count(array_unique($advancereplesscostcheck)) > 1)
		{

			$advance_replesscost_option = "True";
		}

		if(count(array_unique($advancesizecheck)) == 1 && end($advancesizecheck) == "True" || count(array_unique($advancesizecheck)) > 1)
		{
			$advance_size_option = "True";
		}

		if(count(array_unique($advanceexclusivecheck)) == 1 && end($advanceexclusivecheck) == "True" || count(array_unique($advanceexclusivecheck)) > 1)
		{
			$advance_exclusive_option = "True";
		}


		if(count(array_unique($advancelongevitycheck)) == 1 && end($advancelongevitycheck) == "True" || count(array_unique($advancelongevitycheck)) > 1)
		{
			$advance_longevity_option = "True";
		}

		if(count(array_unique($advanceshockcheck)) == 1 && end($advanceshockcheck) == "True" || count(array_unique($advanceshockcheck)) > 1)
		{
			$advance_shock_option = "True";
		}


		if(count(array_unique($advancebulkcheck)) == 1 && end($advancebulkcheck) == "True" || count(array_unique($advancebulkcheck)) > 1)
		{
			$advance_bulk_option = "True";
		}


		if(count(array_unique($advancesiteinfocheck)) == 1 && end($advancesiteinfocheck) == "True" || count(array_unique($advancesiteinfocheck)) > 1)
		{
			$advance_siteinfo_option = "True";
		}

		if(count(array_unique($advanceresearchcheck)) == 1 && end($advanceresearchcheck) == "True" || count(array_unique($advanceresearchcheck)) > 1)
		{
			$advance_research_option = "True";
		}

		
		
		$devicetype = "Changeout";
		$categoryname = category::where('id', '=', $id)->value('category_name');
		$updatedate = client_price::orderby(DB::raw('date(is_updated)'), 'desc')
		->where('client_name','=',$clients)
		->value('is_updated');
		$updatedate = Carbon::parse($updatedate)->format('m/d/Y');

		if(count($entry_levels) == "0" &&  count($advance_levels) == "0") 
		{
			if (strpos(url()->previous(), 'newdevice') !== false) {
				Session::flash('message', 'There is no device in this category!');
				return Redirect::to('newdevice/mainmenu');

			}
			return Redirect::to('newdevice/devices/'.$categoryid);
		}
		return view('pages/frontend/products', compact('entry_levels', 'advance_levels', 'clientname', 'categoryname', 'devicetype', 'unit_cost', 'cco_discount', 'unit_repless_cost', 'updatedate','size_option','exclusive_option','bulk_option','longevity_option','shock_option','siteinfo_option','advance_size_option','advance_exclusive_option','advance_bulk_option','advance_longevity_option','advance_shock_option','advance_siteinfo_option','entry_custom_fields','advance_custom_fields','replesscost_option','replessdiscount_option','advance_replessdiscount_option','advance_replesscost_option','advance_ccocost_option','advance_ccodiscount_option','ccocost_option','ccodiscount_option','research_option','advance_research_option'));

	}

	public function compareproduct() {
		$user_organization = Auth::user()->organization;


		/*get selected clientdetails*/
		$clientdetails =  session('details');
		$clients = $clientdetails['clients'];

		$clientname = clients::where('id', '=', $clients)->value('client_name');

		$products = Input::get('product_chk');
		$devicetype = Input::get('devicetype');

		$first_product = device::join('client_price', 'client_price.device_id', '=', 'device.id')
		->leftjoin('device_features','device_features.device_id','=','device.id')
		->join('manufacturers', 'manufacturers.id', '=', 'device.manufacturer_name')
		->select('device.id','device.device_name','device.device_image','device.longevity','device.shock','device.size','device.research','device.website_page as site_info','device.url','device.overall_value','device_features.longevity_check','device_features.shock_check','device_features.size_check','device_features.research_check','device_features.siteinfo_check','device_features.overall_value_check', 'client_price.device_id', 'client_price.unit_cost', 'client_price.bulk_unit_cost', 'client_price.bulk_unit_cost_check', 'client_price.bulk', 'client_price.bulk_check','client_price.cco','client_price.cco_check', 'client_price.cco_discount', 'client_price.cco_discount_check', 'client_price.unit_rep_cost', 'client_price.unit_rep_cost_check', 'client_price.unit_repless_discount', 'client_price.unit_repless_discount_check', 'client_price.system_cost', 'client_price.system_cost_check', 'client_price.bulk_system_cost', 'client_price.bulk_system_cost', 'client_price.bulk_system_cost_check', 'client_price.system_repless_cost', 'client_price.system_repless_cost_check', 'client_price.system_repless_discount', 'client_price.system_repless_discount_check', 'client_price.reimbursement', 'client_price.reimbursement_check','client_price.system_bulk','client_price.system_bulk_check', 'client_price.order_email', 'manufacturers.manufacturer_logo')
		->where('device.id', '=', $products[0])
		->where('client_price.client_name', '=', $clients)
		->where('device_features.client_name','=',$clients)
		->get();

		foreach ($first_product as $f_product) {
			




			if ($devicetype == "Changeout") {

				$bulk_count = Order::join('users', 'users.id', '=', 'orders.orderby')
				->leftJoin('user_clients','user_clients.userId','=','users.id')
				->where('orders.model_name', '=', $f_product->device_name)
				->where('user_clients.clientId', '=', $clients)
				->where('orders.status', '!=', 'Cancelled')
				->where('orders.unit_cost','!=',"")
				->where('orders.bulk_check','=','True')
				->count();




				$bulk = $f_product->bulk;
				$f_product->remain_bulk = $bulk - $bulk_count;
				$deviceid = $f_product->id;
				$f_product->custom_fields = device_custom_field::leftJoin('client_custom_field','client_custom_field.c_id','=','device_custom_field.id')
				->select('device_custom_field.id','device_custom_field.field_name','device_custom_field.field_value','client_custom_field.field_check')
				->where('client_custom_field.device_id','=',$deviceid)
				->where('client_custom_field.client_name','=',$clients)->get();

				$first_unit_cost = $f_product->unit_cost;

				$unit_cost = $f_product->unit_cost;
				if ($f_product->bulk_unit_cost_check == "True" && $f_product->bulk_check == "True" && $bulk > $bulk_count) {

					$unit_cost -= ($f_product->unit_cost * $f_product->bulk_unit_cost) / 100;

					$f_product->unit_cost = $unit_cost;
				}

				if ($f_product->cco_discount_check == "True") {
					$unit_cost -= ($unit_cost * $f_product->cco_discount) / 100;

					$f_product->cco_discount = $unit_cost;
				}
				else if ($f_product->cco_check == "True") {
					$unit_cost -= $f_product->cco;
					$f_product->cco_discount = $unit_cost;
				}
				else {
					$f_product->cco_discount = "-";
				}


				if ($f_product->unit_repless_discount_check == "True") {

					$unit_cost -= ($unit_cost * $f_product->unit_repless_discount ) / 100;
					$f_product->unit_repless_cost = $unit_cost;
				} else if ($f_product->unit_rep_cost_check == "True") {
					$unit_cost -= $f_product->unit_rep_cost;
					$f_product->unit_repless_cost = $unit_cost;
				} else {
					$f_product->unit_repless_cost = '-';
				}
			} 
			else 
			{
				$first_unit_cost = $f_product->sytem_cost;
				$system_cost = $f_product->system_cost;
				
				

				$bulk_count = Order::join('users', 'users.id', '=', 'orders.orderby')
				->leftJoin('user_clients','user_clients.userId','=','users.id')
				->where('orders.model_name', '=', $f_product->device_name)
				->where('user_clients.clientId', '=', $clients)
				->where('orders.status', '!=', 'Cancelled')
				->where('orders.system_cost','!=',"")
				->where('orders.bulk_check','=','True')
				->count();


				$bulk = $f_product->system_bulk;
				$f_product->remain_bulk = $bulk - $bulk_count;
				$deviceid = $f_product->id;
				$f_product->custom_fields = device_custom_field::leftJoin('client_custom_field','client_custom_field.c_id','=','device_custom_field.id')
				->select('device_custom_field.id','device_custom_field.field_name','device_custom_field.field_value','client_custom_field.field_check')
				->where('client_custom_field.device_id','=',$deviceid)
				->where('client_custom_field.client_name','=',$clients)->get();

				if ($f_product->bulk_system_cost_check == "True" && $f_product->system_bulk_check == "True" && $bulk > $bulk_count) 				{

					$system_cost -= ($f_product->system_cost * $f_product->bulk_system_cost) / 100;

					$f_product->system_cost = $system_cost;
				}
				if ($f_product->system_repless_discount_check == "True") {

					$system_cost -= ($system_cost * $f_product->system_repless_discount ) / 100;
					$f_product->system_repless_cost = $system_cost;
				} else if ($f_product->system_repless_cost_check == "True") {

					$system_cost -= $f_product->system_repless_cost;
					$f_product->system_repless_cost = $system_cost;
				} else {
					$f_product->system_repless_cost = '-';
				}
			}

			
		}


		$second_product = device::join('client_price', 'client_price.device_id', '=', 'device.id')
		->leftjoin('device_features','device_features.device_id','=','device.id')
		->join('manufacturers', 'manufacturers.id', '=', 'device.manufacturer_name')
		->select('device.id','device.device_name','device.device_image','device.longevity','device.shock','device.size','device.research','device.website_page as site_info','device.url','device.overall_value','device_features.longevity_check','device_features.shock_check','device_features.size_check','device_features.research_check','device_features.siteinfo_check','device_features.overall_value_check', 'client_price.device_id', 'client_price.unit_cost', 'client_price.bulk_unit_cost', 'client_price.bulk_unit_cost_check', 'client_price.bulk', 'client_price.bulk_check', 'client_price.cco_discount', 'client_price.cco_discount_check','client_price.cco','client_price.cco_check', 'client_price.unit_rep_cost', 'client_price.unit_rep_cost_check', 'client_price.unit_repless_discount', 'client_price.unit_repless_discount_check', 'client_price.system_cost', 'client_price.system_cost_check', 'client_price.bulk_system_cost', 'client_price.bulk_system_cost', 'client_price.bulk_system_cost_check', 'client_price.system_repless_cost', 'client_price.system_repless_cost_check', 'client_price.system_repless_discount', 'client_price.system_repless_discount_check', 'client_price.reimbursement', 'client_price.reimbursement_check','client_price.system_bulk','client_price.system_bulk_check', 'client_price.order_email', 'manufacturers.manufacturer_logo')
		->where('device.id', '=', $products[1])
		->where('client_price.client_name', '=', $clients)
		->where('device_features.client_name','=',$clients)
		->get();

		foreach ($second_product as $s_product) {
			$deviceid = $s_product->id;
			$s_product->scustom_fields = device_custom_field::leftJoin('client_custom_field','client_custom_field.c_id','=','device_custom_field.id')
			->select('device_custom_field.id','device_custom_field.field_name','device_custom_field.field_value','client_custom_field.field_check')
			->where('client_custom_field.device_id','=',$deviceid)
			->where('client_custom_field.client_name','=',$clients)->get();

			$categoryname = category::where('id', '=', $s_product->category_name)->value('category_name');



			if ($devicetype == "Changeout") {
				
				$bulk_count = Order::join('users', 'users.id', '=', 'orders.orderby')
				->leftJoin('user_clients','user_clients.userId','=','users.id')
				->where('orders.model_name', '=', $s_product->device_name)
				->where('user_clients.clientId', '=', $clients)
				->where('orders.status', '!=', 'Cancelled')
				->where('orders.unit_cost','!=',"")
				->where('orders.bulk_check','=','True')
				->count();



				$bulk = $s_product->bulk;
				$s_product->remain_bulk = $bulk - $bulk_count;

				$second_unit_cost = $s_product->unit_cost;
				$deviceid = $s_product->id;
				$s_product->custom_fields = device_custom_field::where('device_id', $deviceid)->get();

				$unit_cost = $s_product->unit_cost;
				if ($s_product->bulk_unit_cost_check == "True" && $s_product->bulk_check == "True" && $bulk > $bulk_count) {

					$unit_cost -= ($s_product->unit_cost * $s_product->bulk_unit_cost) / 100;

					$s_product->unit_cost = $unit_cost;
				}

				if ($s_product->cco_discount_check == "True") {
					$unit_cost -= ($unit_cost * $s_product->cco_discount) / 100;

					$s_product->cco_discount = $unit_cost;
				}else if ($s_product->cco_check == "True") {
					$unit_cost -= $s_product->cco;
					$s_product->cco_discount = $unit_cost;
				} else {
					$s_product->cco_discount = "-";
				}


				if ($s_product->unit_repless_discount_check == "True") {

					$unit_cost -= ($unit_cost * $s_product->unit_repless_discount ) / 100;
					$s_product->unit_repless_cost = $unit_cost;
				} else if ($s_product->unit_rep_cost_check == "True") {
					$unit_cost -= $s_product->unit_rep_cost;
					$s_product->unit_repless_cost = $unit_cost;
				} else {
					$s_product->unit_repless_cost = '-';
				}
			} else {

				$second_unit_cost = $s_product->system_cost;
				$system_cost = $s_product->system_cost;
				

				
				$bulk_count = Order::join('users', 'users.id', '=', 'orders.orderby')
				->leftJoin('user_clients','user_clients.userId','=','users.id')
				->where('orders.model_name', '=', $S_product->device_name)
				->where('user_clients.clientId', '=', $clients)
				->where('orders.status', '!=', 'Cancelled')
				->where('orders.system_cost','!=',"")
				->where('orders.bulk_check','=','True')
				->count();


				$bulk = $s_product->system_bulk;
				$s_product->remain_bulk = $bulk - $bulk_count;
				$deviceid = $s_product->id;

				if ($s_product->bulk_system_cost_check == "True" && $s_product->system_bulk_check == "True" && $bulk > $bulk_count) 				{

					$system_cost -= ($s_product->system_cost * $s_product->bulk_system_cost) / 100;

					$s_product->system_cost = $system_cost;
				}

				if ($s_product->system_repless_discount_check == "True") {

					$system_cost -= ($system_cost * $s_product->system_repless_discount ) / 100;
					$s_product->system_repless_cost = $system_cost;
				} else if ($s_product->system_repless_cost_check == "True") {
					$system_cost -= $s_product->system_repless_cost;
					$s_product->system_repless_cost = $system_cost;
				} else {
					$s_product->system_repless_cost = '-';
				}
			}
		}
		
		
		
		$first_cost = $devicetype == "Changeout" ? $f_product->unit_cost : $f_product->system_cost;
		$second_cost = $devicetype == "Changeout" ? $s_product->unit_cost : $s_product->system_cost;
		
		$check_unit_cost = min($first_cost,$second_cost);
		$first_cost = $check_unit_cost == $first_cost ? 'True' : 'False';
		
		
		
		$productid = $first_cost == 'True' ? $f_product->id : $s_product->id;
		$productname = $first_cost == 'True' ? $f_product->device_name : $s_product->device_name;
		
		$productimage = $first_cost == 'True' ? $f_product->device_image : $s_product->device_image;
		
		$f_repless_cost = $devicetype == "Changeout" ? $f_product->unit_repless_cost : $f_product->system_repless_cost;
		
		$s_repless_cost = $devicetype == "Changeout" ? $s_product->unit_repless_cost : $s_product->system_repless_cost;
		


		if($first_cost == "True")
		{
			$unit_cost_diff = $s_product->unit_cost - $f_product->unit_cost;
			$cco_diff = $s_product->cco_discount - $f_product->cco_discount;
			$system_cost_diff = $s_product->system_cost - $f_product->system_cost;
			$reimbursement_diff = $s_product->reimbursement - $f_product->reimbursement;
			$longevity_diff = $s_product->longevity - $f_product->longevity;
			$repless_cost_diff = $s_repless_cost - $f_repless_cost;
			
			// $shock_diff = 0;
			// $ct_diff = 0;
			// $size_g_diff = 0;
			// $size_cc_diff = 0;
			// if ($f_product->shock != "" && $s_product->shock != "") {

			// 	$shock_ct_f_product = explode('/', $f_product->shock);
			// 	$shock_ct_s_product = explode('/', $s_product->shock);

			// 	$shock_f_product = $shock_ct_f_product[0];
			// 	$ct_f_product = $shock_ct_f_product[1];
			// 	$shock_s_product = $shock_ct_s_product[0];
			// 	$ct_s_product = $shock_ct_s_product[1];
			// 	$shock_diff = $shock_s_product - $shock_f_product;
			// 	$ct_diff = $ct_s_product - $ct_f_product;
			// 	$shock_ct = "True";
			// 	if($f_product->shock_check != "True" || $s_product->shock_check != "True")
			// 	{	

			// 		$shock_ct = "False";
			// 		$shock_f_product = $shock_f_product ."J/".$ct_f_product."s";
			// 		$shock_s_product = $shock_s_product ."J/".$ct_s_product."s";

			// 	}
			// }

			// if ($f_product->size != "" && $s_product->size != "") {
			// 	$size_f_product = explode('/', $f_product->size);
			// 	$size_s_product = explode('/', $s_product->size);
			// 	$size_g_f_product = $size_f_product[0];
			// 	$size_cc_f_product = $size_f_product[1];
			// 	$size_g_s_product = $size_s_product[0];
			// 	$size_cc_s_product = $size_s_product[1];
			// 	$size_g_diff = $size_g_s_product - $size_g_f_product;
			// 	$size_cc_diff = $size_cc_s_product - $size_cc_f_product;
			// }

			// $size_diff = $s_product->size - $f_product->size;

			/*07/02/2017*/
			$shock_f_product = "";
			$shock_s_product = "";
			if($f_product->shock != "")
			{
				$shock_ct_f_product = explode('/', $f_product->shock);
				$shock_f_product = $shock_ct_f_product[0];
				$ct_f_product = $shock_ct_f_product[1];
				$shock_f_product = $shock_f_product ."J/".$ct_f_product."s";
			}

			if($s_product->shock != "")
			{
				$shock_ct_s_product = explode('/', $s_product->shock);
				$shock_s_product = $shock_ct_s_product[0];
				$ct_s_product = $shock_ct_s_product[1];
				$shock_s_product = $shock_s_product ."J/".$ct_s_product."s";
			}

			$size_f_product = "";
			$size_s_product = "";

			if($f_product->size != "")
			{
				$size_f_product = explode('/', $f_product->size);
				$size_g_f_product = $size_f_product[0];
				$size_cc_f_product = $size_f_product[1];
				$size_f_product = $size_g_f_product . "g/" .$size_cc_f_product . "cc";
			}

			if($s_product->size != "")
			{
				$size_s_product = explode('/', $s_product->size);
				$size_g_s_product = $size_s_product[0];
				$size_cc_s_product = $size_s_product[1];
				$size_s_product = $size_g_s_product . "g/" .$size_cc_s_product . "cc";

			}

		}
		else
		{
			$unit_cost_diff = $f_product->unit_cost - $s_product->unit_cost;
			$cco_diff = $f_product->cco_discount - $s_product->cco_discount;
			$system_cost_diff = $f_product->system_cost - $s_product->system_cost;
			$reimbursement_diff = $f_product->reimbursement - $s_product->reimbursement;
			$longevity_diff = $f_product->longevity - $s_product->longevity;
			$repless_cost_diff = $f_repless_cost - $s_repless_cost;
			
			$shock_diff = 0;
			$ct_diff = 0;
			$size_g_diff = 0;
			$size_cc_diff = 0;

			$shock_ct_f_product = "";
			$shock_ct_s_product = "";

			/*07/02/2017*/
			$shock_f_product = "";
			$shock_s_product = "";
			if($f_product->shock != "")
			{
				$shock_ct_f_product = explode('/', $f_product->shock);
				$shock_f_product = $shock_ct_f_product[0];
				$ct_f_product = $shock_ct_f_product[1];
				$shock_f_product = $shock_f_product ."J/".$ct_f_product."s";
			}

			if($s_product->shock != "")
			{
				$shock_ct_s_product = explode('/', $s_product->shock);
				$shock_s_product = $shock_ct_s_product[0];
				$ct_s_product = $shock_ct_s_product[1];
				$shock_s_product = $shock_s_product ."J/".$ct_s_product."s";
			}






		/*	if ($f_product->shock != "" && $s_product->shock != "") {
				
				$shock_ct_f_product = explode('/', $f_product->shock);
				$shock_ct_s_product = explode('/', $s_product->shock);
				$shock_f_product = $shock_ct_f_product[0];
				$ct_f_product = $shock_ct_f_product[1];
				$shock_s_product = $shock_ct_s_product[0];
				$ct_s_product = $shock_ct_s_product[1];
				$shock_diff = $shock_f_product - $shock_s_product;
				$ct_diff = $ct_f_product - $ct_s_product;
				$shock_ct = "True";
				if($f_product->shock_check != "True" || $s_product->shock_check != "True")
				{	
					
					$shock_ct = "False";
					$shock_f_product = $shock_f_product ."J/".$ct_s_product."s";
					$shock_s_product = $shock_f_product ."J/".$ct_s_product."s";
					
				}
			}*/

			$size_f_product = "";
			$size_s_product = "";

			if($f_product->size != "")
			{
				$size_f_product = explode('/', $f_product->size);
				$size_g_f_product = $size_f_product[0];
				$size_cc_f_product = $size_f_product[1];
				$size_f_product = $size_g_f_product . "g/" .$size_cc_f_product . "cc";
			}

			if($s_product->size != "")
			{
				$size_s_product = explode('/', $s_product->size);
				$size_g_s_product = $size_s_product[0];
				$size_cc_s_product = $size_s_product[1];
				$size_s_product = $size_g_s_product . "g/" .$size_cc_s_product . "cc";

			}


			/*if ($f_product->size != "" && $s_product->size != "") {
				$size_f_product = explode('/', $f_product->size);
				$size_s_product = explode('/', $s_product->size);
				$size_g_f_product = $size_f_product[0];
				$size_cc_f_product = $size_f_product[1];
				$size_g_s_product = $size_s_product[0];
				$size_cc_s_product = $size_s_product[1];
				$size_g_diff = $size_g_f_product - $size_g_s_product;
				$size_cc_diff = $size_cc_f_product - $size_cc_s_product;
			}
	
			$size_diff = $f_product->size - $s_product->size;
			*/
		}	
		
		$first_replesscost_check = $devicetype == "Changeout" ? $f_product->unit_rep_cost_check : $f_product->system_repless_cost_check;
		$first_replessdiscount_check = $devicetype == "Changeout" ? $f_product->unit_repless_discount_check :$f_product->system_repless_discount_check;
		$first_ccocost_check = $f_product->cco_check;
		$first_ccodiscount_check = $f_product->cco_discount_check;

		$first_exclusivecheck = $f_product->exclusive_check;
		$first_sizecheck = $f_product->size_check;
		$first_longevitycheck = $f_product->longevity_check;
		$first_shockcheck = $f_product->shock_check;
		$first_sizecheck = $f_product->size_check;
		$first_bulkcheck = $devicetype == "Changeout" ? $f_product->bulk_check : $f_product->system_bulk_check;
		$first_siteinfocheck = $f_product->siteinfo_check;
		$first_researchcheck = $f_product->research_check;
		
		
		$second_replesscost_check = $devicetype == "Changeout" ? $s_product->unit_rep_cost_check : $s_product->system_repless_cost_check;
		$second_replessdiscount_check = $devicetype == "Changeout" ? $s_product->unit_repless_discount_check :$s_product->system_repless_discount_check;
		$second_ccocost_check = $s_product->cco_check;
		$second_ccodiscount_check = $s_product->cco_discount_check;
		$second_exclusivecheck = $s_product->exclusive_check;
		$second_sizecheck = $s_product->size_check;
		$second_longevitycheck = $s_product->longevity_check;
		$second_shockcheck = $s_product->shock_check;
		$second_sizecheck = $s_product->size_check;
		$second_bulkcheck = $devicetype == "Changeout" ? $s_product->bulk_check : $s_product->system_bulk_check;
		$second_siteinfocheck = $s_product->siteinfo_check;
		$second_researchcheck = $s_product->research_check;
		
		
		
		$exclusive_option = "True";
		$size_option = "True";
		$longevity_option = "True";
		$shock_option = "True";
		$size_option = "True";
		$bulk_option = "True";
		$siteinfo_option = "True";
		$cco_discount_option = "True";
		$repless_discount_option = "True";
		$research_option = "True";
		
		
		if(($first_replesscost_check == "True" && $second_replesscost_check == "True") && ($first_replessdiscount_check == "True" && $second_replessdiscount_check == "True"))
		{
			$repless_discount_option = "True";
		}
		else if(($first_replesscost_check == "False" && $second_replesscost_check == "False") && ( $first_replessdiscount_check == "False" && $second_replessdiscount_check == "False"))
		{
			$repless_discount_option = "False";
		}


		if(($first_ccocost_check == "True" && $second_ccocost_check == "True") && ($first_ccodiscount_check == "True" && $second_ccodiscount_check == "True"))
		{
			$cco_discount_option = "True";
		}
		else if(($first_ccocost_check == "False" && $second_ccocost_check == "False") && ($first_ccodiscount_check == "False" && $second_ccodiscount_check == "False"))
		{
			$cco_discount_option = "False";

		}



		if($first_exclusivecheck == "True" && $second_exclusivecheck == "True")
		{
			$exclusive_option = "True";
		}
		else if($first_exclusivecheck == "" && $second_exclusivecheck == "")
		{
			$exclusive_option = "False";
		}
		

		if($first_sizecheck == "True" && $second_sizecheck == "True")
		{
			$size_option = "True";
		}
		else if($first_sizecheck == "" && $second_sizecheck == "")
		{
			$size_option = "False";
		}
		
		
		if($first_longevitycheck == "True" && $second_longevitycheck == "True")
		{
			$longevity_option = "True";
		}
		else if($first_longevitycheck == "" && $second_longevitycheck == "")
		{
			$longevity_option = "False";
		}


		if($first_shockcheck == "True" && $second_shockcheck == "True")
		{
			$shock_option = "True";
		}
		else if($first_shockcheck == "" && $second_shockcheck == "")
		{
			$shock_option = "False";
		}
		
		
		
		if($first_bulkcheck == "True" && $second_bulkcheck == "True")
		{
			$bulk_option = "True";
		}
		else if($first_bulkcheck == "False" && $second_bulkcheck == "False")
		{
			$bulk_option = "False";
		}
		
		if($first_siteinfocheck == "True" && $second_siteinfocheck == "True")
		{
			$siteinfo_option = "True";
		}
		else if($first_siteinfocheck == "" && $second_siteinfocheck == "")
		{
			$siteinfo_option = "False";
		}

		if($first_researchcheck == "True" && $second_researchcheck == "True")
		{
			$research_option = "True";
		}
		else if($first_researchcheck == "" && $second_researchcheck == "")
		{
			$research_option = "False";
		}
		
		
		$cco_check_value = "False";
		if(($f_product->cco_discount_check == "True" || $f_product->cco_check == "True") && ($s_product->cco_discount_check == "True" || $s_product->cco_check == "True"))
		{
			$cco_check_value = "True";
		}


		$repless_discount_value = "False";
		if(($first_replesscost_check == "True" || $first_replessdiscount_check == "True") && ($second_replesscost_check == "True" || $second_replessdiscount_check == "True"))
		{
			$repless_discount_value = "True";
		}


		$updatedate = client_price::orderby(DB::raw('date(is_updated)'), 'desc')
		->where('client_name','=',$clients)
		->value('is_updated');
		$updatedate = Carbon::parse($updatedate)->format('m/d/Y');

		return view('pages/frontend/compare', compact('first_product', 'second_product', 'check_unit_cost', 'clientname', 'categoryname', 'updatedate', 'devicetype', 'system_cost_diff', 'reimbursement_diff', 'longevity_diff', 'shock_diff', 'ct_diff', 'cco_diff', 'unit_cost_diff', 'size_g_diff', 'size_cc_diff','shock_ct_f_product','shock_ct_s_product','size_f_product','size_s_product','shock_ct','shock_f_product','shock_s_product','productid','productname','productimage','exclusive_option','size_option','longevity_option','shock_option','size_option','bulk_option','siteinfo_option','repless_discount_option','cco_discount_option','cco_check_value','repless_cost_diff','repless_discount_value','research_option'));
	}

	public function purchase() {

		$devicetype = Input::get('devicetype');
		$deviceid = Input::get('deviceid');
		$surveyId = Input::get('surveyId');

		$user_organization = Auth::user()->organization;
		/*get selected clientdetails*/
		$clientdetails =  session('details');
		$clients = $clientdetails['clients'];


		$orderdate = date('Y-m-d');
		$orderby = Auth::user()->id;
		$sento = Auth::user()->email;
		$devicedetails = device::join('client_price', 'client_price.device_id', '=', 'device.id')
		->select('device.*', 'client_price.device_id', 'client_price.unit_cost', 'client_price.bulk_unit_cost', 'client_price.bulk_unit_cost_check', 'client_price.bulk', 'client_price.bulk_check', 'client_price.cco_discount', 'client_price.cco_discount_check', 'client_price.unit_rep_cost', 'client_price.unit_rep_cost_check', 'client_price.unit_repless_discount', 'client_price.unit_repless_discount_check', 'client_price.system_cost', 'client_price.system_cost_check', 'client_price.bulk_system_cost', 'client_price.bulk_system_cost', 'client_price.bulk_system_cost_check', 'client_price.system_repless_cost', 'client_price.system_repless_cost_check', 'client_price.system_repless_discount', 'client_price.system_repless_discount_check', 'client_price.reimbursement', 'client_price.reimbursement_check', 'client_price.order_email','client_price.cco','client_price.cco_check','client_price.system_bulk','client_price.system_bulk_check')
		->where('device.id', '=', $deviceid)
		->where('client_price.client_name', '=', $clients)
		->get();

		foreach ($devicedetails as $devicedetail) 
		{
			

			$user_organization = $clients;
			$devicerep = $devicedetail->rep_email;
			$orderuser = $devicedetail->order_email;
			

			
		}
		
		if ($devicetype == "Changeout") {


			$bulk_count = Order::join('users', 'users.id', '=', 'orders.orderby')
			->leftJoin('user_clients','user_clients.userId','=','users.id')
			->where('orders.model_name', '=', $devicedetail->device_name)
			->where('user_clients.clientId', '=', $clients)
			->where('orders.status', '!=', 'Cancelled')
			->where('orders.unit_cost','!=',"")
			->where('orders.bulk_check','=','True')
			->count();

			$bulk = $devicedetail->bulk;
			$devicedetail->remain_bulk = $bulk - $bulk_count;

			$second_unit_cost = $devicedetail->unit_cost;

			$unit_cost = $devicedetail->unit_cost;
			if ($devicedetail->bulk_unit_cost_check == "True" && $devicedetail->bulk_check == "True" && $bulk > $bulk_count) {

				$unit_cost -= ($devicedetail->unit_cost * $devicedetail->bulk_unit_cost) / 100;

				$devicedetail->unit_cost = $unit_cost;
			}

			if ($devicedetail->cco_discount_check == "True") {
				$unit_cost -= ($unit_cost * $devicedetail->cco_discount) / 100;

				$devicedetail->cco_discount = $unit_cost;
			}else if ($devicedetail->cco_check == "True") {
				$unit_cost -= $devicedetail->cco;
				$devicedetail->cco_discount = $unit_cost;
			} else {
				$devicedetail->cco_discount = "-";
			}


			if ($devicedetail->unit_repless_discount_check == "True") {

				$unit_cost -= ($unit_cost * $devicedetail->unit_repless_discount ) / 100;
				$devicedetail->unit_repless_cost = $unit_cost;
			} else if ($devicedetail->unit_rep_cost_check == "True") {
				$unit_cost -= $devicedetail->unit_rep_cost;
				$devicedetail->unit_repless_cost = $unit_cost;
			} else {
				$devicedetail->unit_repless_cost = '-';
			}
		} else {

			$second_unit_cost = $devicedetail->system_cost;
			$system_cost = $devicedetail->system_cost;


			$bulk_count = Order::join('users', 'users.id', '=', 'orders.orderby')
			->leftJoin('user_clients','user_clients.userId','=','users.id')
			->where('orders.model_name', '=', $devicedetail->device_name)
			->where('user_clients.clientId', '=', $clients)
			->where('orders.status', '!=', 'Cancelled')
			->where('orders.system_cost','!=',"")
			->where('orders.bulk_check','=','True')
			->count();

			$bulk = $devicedetail->system_bulk;
			$devicedetail->remain_bulk = $bulk - $bulk_count;
			$deviceid = $devicedetail->id;
			$devicedetail->custom_fields = device_custom_field::where('device_id', $deviceid)->get();

			if ($devicedetail->bulk_system_cost_check == "True" && $devicedetail->system_bulk_check == "True" && $bulk > $bulk_count) 	{

				$system_cost -= ($devicedetail->system_cost * $devicedetail->bulk_system_cost) / 100;

				$devicedetail->system_cost = $system_cost;
			}

			if ($devicedetail->system_repless_discount_check == "True") {

				$system_cost -= ($system_cost * $devicedetail->system_repless_discount ) / 100;
				$devicedetail->system_repless_cost = $system_cost;
			} else if ($devicedetail->system_repless_cost_check == "True") {
				$system_cost -= $devicedetail->system_repless_cost;
				$devicedetail->system_repless_cost = $system_cost;
			} else {
				$devicedetail->system_repless_cost = '-';
			}
		}

		if($devicedetail->bulk_check == "True" && $devicedetail->bulk_unit_cost_check == "True")
		{
			$unit_bulk = "True";
		}
		else
		{
			$unit_bulk = "False";
		}
		if($devicedetail->system_bulk_check == "True" && $devicedetail->bulk_system_cost_check == "True")
		{
			$system_bulk = "True";
		}
		else
		{
			$system_bulk = "False";
		}

		$manufacturer_name = $devicedetail->manufacturer_name;
		$device_name = $devicedetail->device_name;
		$model_no = $devicedetail->model_name;
		$unit_cost = $devicetype == "Changeout" ? $devicedetail->unit_cost : '0';
		$system_cost = $devicetype == "New Device" ? $devicedetail->system_cost : '0';
		$cco = $devicetype == "New Device" ? '0' : $devicedetail->cco_discount;
		$bulk_check = $devicetype == "New Device" ? $system_bulk : $unit_bulk;
		
		$order_detail = array(
			'manufacturer_name' => $manufacturer_name,
			'model_name' => $device_name,
			'model_no' => $model_no,
			'unit_cost' => $unit_cost,
			'system_cost' => $system_cost,
			'cco' => $cco,
			'order_date' => $orderdate,
			'orderby' => $orderby,
			'rep' => $devicerep,
			'sent_to' => $sento,
			'status' => 'New',
			'bulk_check' => $bulk_check,
			'is_delete' => 0
			);

		$placeorder = order::insert($order_detail);
		if ($placeorder) {


			// survey answer flag update
			if($surveyId != "null"){
				$survey['flag'] = 'True';

				$SurveyAnswer = SurveyAnswer::where('id',$surveyId)->update($survey);
			}

			
			/* get clients for administrator email*/
			$physician_client = user::where('id', '=', $orderby)
			->where('status','=','Enabled')->first();

			$clients = [];
			foreach ($physician_client->userclients as $row) {
				$clients[] = $row->clientId;
			}

			$administrators = user::leftJoin('user_clients','user_clients.userId','=','users.id')
							 ->where('users.roll', '=', 2)
							 ->where('users.status','=','Enabled')
							 ->whereIn('user_clients.clientId',$clients)
							 ->get();


			$physicianemail = $sento;
			$orderemail = user::where('id', '=', $orderuser)
			->where('status','=','Enabled')
			->value('email');

			$repemail = user::where('id', '=', $devicerep)
			->where('status','=','Enabled')
			->value('email');

			$title = "Welcome to Neptune-PPA - Order Details ";
			$orderid = order::get()->last();
			$orderid = $orderid->id;
			$order_details = order::leftjoin('manufacturers', 'manufacturers.id', '=', 'orders.manufacturer_name')
			->leftjoin('users', 'users.id', '=', 'orders.rep')
			->leftjoin('users as ob', 'ob.id', '=', 'orders.orderby')
			->select('orders.*', 'users.name', 'ob.name as ob_name', 'manufacturers.manufacturer_name')
			->where('orders.id', '=', $orderid)->get();

			foreach ($order_details as $order_detail) {
				$manufacturer = $order_detail->manufacturer_name;
				$device_name = $order_detail->model_name;
				$model_no = $order_detail->model_no;
				$unit_cost = $order_detail->unit_cost;
				$system_cost = $order_detail->system_cost;
				$cco = $order_detail->cco;
				$orderdate = $order_detail->order_date;
				$orderby = $order_detail->ob_name;
				$devicerep = $order_detail->name;
				$sento = $order_detail->sent_to;
			}


            // Custom Contact Mail sending

			$deviceId = $devicedetail->id;
			$clientId = $clients;

			$maildata = customContact::where('clientId',$clientId)->where('deviceId',$deviceId)->first();
			if(empty($maildata)){
			$userMail = "";
			$cc1 = "";
			$cc2 = "";
			$cc3 = "";
			$cc4 = "";
			$cc5 = "";
			} else{
			$userMail = $maildata->user->email ;
			$cc1 = $maildata['cc1'];
			$cc2 = $maildata['cc2'];
			$cc3 = $maildata['cc3'];
			$cc4 = $maildata['cc4'];
			$cc5 = $maildata['cc5'];
			}

			$data = array(
				'title' => $title,
				'physician' => $physicianemail,
				'rep_email' => $repemail,
				'order_email' => $userMail,
				'administrator_email' => $administrators,
				'manufacturer_name' => $manufacturer,
				'model_name' => $device_name,
				'model_no' => $model_no,
                //'unit_cost' => $unit_cost,
                //'system_cost' => $system_cost,
                //'cco' => $cco,
				'order_date' => $orderdate,
				'orderby' => $orderby,
				'rep' => $devicerep,
				'sent_to' => $sento,
				'cc1' => $cc1,
				'cc2' => $cc2,
				'cc3' => $cc3,
				'cc4' => $cc4,
				'cc5' => $cc5
				);
			Mail::send('emails.createorder', $data, function($message) use ($data) {
				$message->from('admin@neptuneppa.com')->subject($data['title']);
				if($data['physician'] != "")
				{
					$message->to($data['physician']);
				}
				
				if($data['rep_email'] != "")
				{
					$message->to($data['rep_email']);
				}
				if($data['order_email'] != ""){
					$message->to($data['order_email']);
				}
				if($data['cc1'] != ""){
					$message->to($data['cc1']);
				}
				if($data['cc2'] != ""){
					$message->to($data['cc2']);
				}
				if($data['cc3'] != ""){
					$message->to($data['cc3']);
				}
				if($data['cc4'] != ""){
					$message->to($data['cc4']);
				}
				if($data['cc5'] != ""){
					$message->to($data['cc5']);
				}
				
				foreach ($data['administrator_email'] as $administrator) {
					if ($administrator->email != "") {
						$message->to($administrator->email);
					}
				}
			});
		}
		return Redirect::to('menu');
	}

    // Rep Contact information Function

	public function repContactInfo(Request $request)
	{

		$id=$request->get('id'); 

		$rep = User::repcontact()->where('rep_contact_info.deviceId',$id)->where('rep_contact_info.repStatus','Yes')->get();
		$manufacturer = device::where('id',$id)->first();
		$manufacturerName = $manufacturer->manufacturer->manufacturer_name;
		$clientdetails =  session('details');
		$clients = $clientdetails['clients'];
		$clientname = clients::where('id', '=', $clients)->value('client_name');
		if(count($rep)){
			$rep = [
			'data'=>$rep,
			'status' => TRUE
			];
		} else {
			$rep = [
			'data'=> "No Data Found",
			'status' => False
			];
		}

		$data = ['rep'=>$rep,'manufacturer'=>$manufacturerName,'clientName'=>$clientname];

		return [
		'value' => $data,
		'status' => TRUE
		];

	}

	// Survey question get Function

	public function surveyQuestion(Request $request)
	{
		$id=$request->get('id');
		$clientdetails =  session('details');
		$clients = $clientdetails['clients'];

		$survey = Survey::where('deviceId',$id)->where('clientId',$clients)->where('status','True')->first();
		
		$data = $survey;	
		

		if (count($data))
			return [
		'value' => $data,
		'status' => TRUE
		];
		else
			return [
		'value' => $id,
		'status' => FALSE
		];


	}

	public function surveyQuestionAnswer(Request $request)
	{
		$data = $request->all();
		
		$data['deviceId'] = $request['deviceId'];
		$data['surveyId'] = $request['surveyId'];
		$data = $request->except('_token');
		$data['user_id'] = Auth::user()->id;	

		$data['que_1_answer'] = $request['que_1_answer'] == 'True' ? 'True' : 'False';
		$data['que_2_answer'] = $request['que_2_answer'] == 'True' ? 'True' : 'False';
		$data['que_3_answer'] = $request['que_3_answer'] == 'True' ? 'True' : 'False';
		$data['que_4_answer'] = $request['que_4_answer'] == 'True' ? 'True' : 'False';
		$data['que_5_answer'] = $request['que_5_answer'] == 'True' ? 'True' : 'False';
		$data['que_6_answer'] = $request['que_6_answer'] == 'True' ? 'True' : 'False';
		$data['que_7_answer'] = $request['que_7_answer'] == 'True' ? 'True' : 'False';
		$data['que_8_answer'] = $request['que_8_answer'] == 'True' ? 'True' : 'False';
		
		$survey  = Survey::where('id',$data['surveyId'])->first();

		$data['que_1'] = $survey['que_1'];
		$data['que_1_check'] = $survey['que_1_check'];
		$data['que_2'] = $survey['que_2'];
		$data['que_2_check'] = $survey['que_2_check'];
		$data['que_3'] = $survey['que_3'];
		$data['que_3_check'] = $survey['que_3_check'];
		$data['que_4'] = $survey['que_4'];
		$data['que_4_check'] = $survey['que_4_check'];
		$data['que_5'] = $survey['que_5'];
		$data['que_5_check'] = $survey['que_5_check'];
		$data['que_6'] = $survey['que_6'];
		$data['que_6_check'] = $survey['que_6_check'];
		$data['que_7'] = $survey['que_7'];
		$data['que_7_check'] = $survey['que_7_check'];
		$data['que_8'] = $survey['que_8'];
		$data['que_8_check'] = $survey['que_8_check'];
		$data['flag'] = "False";

		$surveyans = new SurveyAnswer;
		$surveyans->fill($data);
		if ($surveyans->save()) {

			$checkvalue['surveyId'] = $surveyans->id;
			$checkvalue['deviceid'] = $surveyans->deviceId;
			if (count($checkvalue))
				return [
			'value' => $checkvalue,
			'status' => TRUE
			];
			else
				return [
			'value' => 'No result Found',
			'status' => FALSE
			];
		}



	}

}
