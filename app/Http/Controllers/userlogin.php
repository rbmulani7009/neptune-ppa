<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Auth;
use DB;
use Cookie;
use App\userClients;

class userlogin extends Controller
{
	
	public function showlogin()
	{
            $value = "False";
            if(isset($_COOKIE['neptune']))
            {
                    $value = $_COOKIE['neptune'];
            }
            else
            {
                    Cookie::queue(Cookie::make('neptune', 'True'));

            }
			
		return view('auth/userlogin',compact('value'));
	}
	
	public function dologin()
	{
		$rules = array(
			'email' => 'required|email',
			'password' => 'required'
		);
		
		$validator = Validator::make(Input::all(),$rules);
		
		if($validator->fails())
		{
			return Redirect::to('login')->withErrors($validator);
		}
		else
		{
			$userdata = array(
			'email' => Input::get('email'),
			'password' => Input::get('password')
			);
			
			if(Auth::attempt($userdata))
			{
				
				if(Auth::user()->status == "Enabled")
				{
					$user = Auth::user()->roll;
					$agree = Auth::user()->is_agree;
					if($user == 1 || $user == 4 || $user == 5)
					{
						Auth::logout();
						return Redirect::to('login')
						->withErrors(['email' =>'This type of users can not able to login.',]);
					}
					else
					{
						if($agree == 1)
						{
					 		//return Redirect('menu');
							
							/* -- 03/05/2017 -- */
						
							$clients = ['0' => 'Select Client'] + userClients::leftJoin('clients','clients.id','=','user_clients.clientId')
								->where('user_clients.userId','=',Auth::user()->id)
								->lists('clients.client_name','clients.id')
								->all();
							return view('pages.frontend.login.selectclients',compact('clients'));
						}
						else
						{
							return Redirect('agree');
						}
					}
				}
				else
				{
					Auth::logout();
					return Redirect::to('login')
					->withErrors(['email' =>'This users is disabled from Neptune-PPA.',]);
				}
			}
			else
			{
				return Redirect::to('login')
					->withErrors(['email' =>'Login details are not correct.',]);
			}
		}
	}
	
	public function logout()
	{
		Auth::logout();
		return Redirect::to('login');
		
	}
	
	public function agree()
	{
		return view('pages.frontend.agree');
	}
	
	public function agreeconditions()
	{
		$agreebutton = Input::get('agreebutton');
		if($agreebutton == "Agree")
		{
			$id = Auth::user()->id;
			$updatedata = array('is_agree'=>'1');
			DB::table('users')->where('id','=',$id)->update($updatedata);
			return Redirect::to('menu');
		}
		else
		{
			Auth::logout();
			return Redirect::to('login');
		}
	}


	/*After login continue with next step 03/05/2017 */
	public function logincontinue(Request $request){

		$data['clients'] = $request['clients'];

		if(Auth::user()->roll == 2)
		{
 		  $data['projects'] = $request['projects'];
 		}
 		$details = $data;

 		$request->session()->put('details', $details);
		return redirect::to('menu');



	}
}
