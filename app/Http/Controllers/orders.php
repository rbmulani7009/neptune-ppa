<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\order;

use App\manufacturers;

use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\DB;
use Auth;
use App\user;

use Illuminate\Support\Facades\Validator;
use Response;


class orders extends Controller
{
    public function index(Request $request)
	{
		$pagesize = $request->get('pagesize');
		if($pagesize == "")
		{
			$pagesize = 10;
		}
		if(Auth::user()->roll == 2 || Auth::user()->roll == 4)
		{
			$userid = Auth::user()->id;
			$organization = user::where('id','=',$userid)->value('organization');
			$physicians = user::where('organization','=',$organization)->where('roll','=',3)->get();
			
			$physicians_id = [];
		   foreach($physicians as $physician)
		   {
			 $physicians_id[] = $physician->id;
			
		   }
		   $orders = order::leftjoin('manufacturers','manufacturers.id','=','orders.manufacturer_name')
                           ->leftjoin('users','users.id','=','orders.rep')
                           ->leftjoin('users as ob','ob.id','=','orders.orderby')
                           ->whereIn('orders.orderby',$physicians_id)
                           ->where('orders.is_delete','=','0')
                           ->where('orders.is_archive','=','0')
                           ->select('orders.*','users.name','ob.name as ob_name','manufacturers.manufacturer_name')
                           ->orderby('orders.id','DESC')
                           ->paginate($pagesize);
						   
			$count = order::where('orders.is_delete','=','0')
			         ->whereIn('orders.orderby',$physicians_id)
					 ->where('orders.is_archive','=','0')
					 ->count();
                   
		}	
		else
		{
		 $orders = order::leftjoin('manufacturers','manufacturers.id','=','orders.manufacturer_name')
                         ->leftjoin('users','users.id','=','orders.rep')
                         ->leftjoin('users as ob','ob.id','=','orders.orderby')
                         ->where('orders.is_delete','=','0')
                         ->where('orders.is_archive','=','0')
                         ->select('orders.*','users.name','ob.name as ob_name','manufacturers.manufacturer_name')
                         ->orderby('orders.id','DESC')->paginate($pagesize);
			$count = order::where('orders.is_delete','=','0')->where('orders.is_archive','=','0')->count();
		}
		
		return view('pages.order',compact('orders','count','pagesize'));
	}
	
	public function edit($id)
	{
		$manufacturer = ['0' => 'Select Manufacturer'] + Manufacturers::where('is_delete','=',0)->lists('manufacturer_name','id')->all(); 
		$orders = order::leftjoin('users','users.id','=','orders.rep')->leftjoin('users as ob','ob.id','=','orders.orderby')->select('orders.*','ob.name as obname','users.name')->FindOrFail($id);
		return view('pages.editorder',compact('orders','manufacturer'));
	}
	
	public function update(Request $request,$id)
	{
		if(Auth::user()->roll == 1)
		{
			$updatedata = array(
					'manufacturer_name' => $request->get('manufacturer_name'),
					'model_name' => $request->get('model_name'),
					'unit_cost' => $request->get('unit_cost'),
					'system_cost' => $request->get('system_cost'),
					'cco' => $request->get('cco'),
					'reimbrusement' => $request->get('reimbrusement'),
					'order_date' => $request->get('order_date'),
					'orderby' => $request->get('orderby'),
					'status' => $request->get('status'),
					'is_delete' => "0"
				);
		$update_order = DB::table('orders')->where('id','=',$id)->update($updatedata);
		return Redirect::to('admin/orders');
		}
		else
		{
			$updatedata = array(
					'status' => $request->get('status')
				);
		$update_order = DB::table('orders')->where('id','=',$id)->update($updatedata);
		return Redirect::to('admin/orders');
		}
		
        		
		
	
			
			
	
	}
	
	
	public function updateall()
	{
		$hiddenid = Input::get('hiddenid');
		$status = Input::get('status');
		
		foreach(array_combine($hiddenid,$status) as $orderid => $order)
		{
			$updatedata = array('status' => $order);
			$updatestatus = DB::table('orders')->where('id','=',$orderid)->update($updatedata);
		}
		return Redirect::to('admin/orders');
		
	}
	
	public function search()
	{
		$fieldname = Input::get('fieldName');
		$fieldvalue = Input::get('value');
		if(Auth::user()->roll == 2 || Auth::user()->roll == 4)
		{
			$userid = Auth::user()->id;
			$organization = user::where('id','=',$userid)->value('organization');
			$physicians = user::where('organization','=',$organization)->where('roll','=',3)->get();
			
			$physicians_id = [];
		   foreach($physicians as $physician)
		   {
			 $physicians_id[] = $physician->id;
			
		   }
		   
		   $search_orders = order::leftjoin('manufacturers','manufacturers.id','=','orders.manufacturer_name')
                           ->leftjoin('users','users.id','=','orders.rep')
                           ->leftjoin('users as ob','ob.id','=','orders.orderby')
                           ->whereIn('orders.orderby',$physicians_id)
                           ->where('orders.is_delete','=','0')
                           ->where('orders.is_archive','=','0')
                           ->select('orders.*','users.name','ob.name as ob_name','manufacturers.manufacturer_name')
                           ->orderby('orders.id','DESC');

             if($fieldvalue != "")
             {
             	$search_orders = $search_orders->where($fieldname,'LIKE',$fieldvalue.'%');
             }

             $search_orders = $search_orders->get();
			
		}	
		else
		{
			$search_orders = order::leftjoin('manufacturers','manufacturers.id','=','orders.manufacturer_name')
                                ->leftjoin('users','users.id','=','orders.rep')
                                ->leftjoin('users as ob','ob.id','=','orders.orderby')
                                ->where('orders.is_delete','=','0')
                                ->where('orders.is_archive','=','0')
                                ->select('orders.*','ob.name as ob_name','manufacturers.manufacturer_name','users.name')
                                ->orderby('orders.id','DESC');

             if($fieldvalue != "")
             {
             	$search_orders = $search_orders->where($fieldname,'LIKE',$fieldvalue.'%');
             }

             $search_orders = $search_orders->get();
		}
		$data = $search_orders;
		
		if(count($data))
			return [
						'value' => $data,
						'status' => TRUE
				   ];
		else
			return [
						'value'=>'No Result Found',
						'status' => FALSE
				   ];
	}
	
	public function remove($id)
	{
		$removedata = array('is_delete'=>'1');
		$remove = DB::table('orders')->where('id','=',$id)->delete();
		return Redirect::to('admin/orders');
	}
	
	public function archive()
	{
		$orderchecks = Input::get('order_check');
		for($i = 0; $i < count($orderchecks); $i++)
		{
			$setarchive = array('is_archive'=>1);
			$add_acrchive = DB::table('orders')->where('id','=',$orderchecks[$i])->update($setarchive);
		
		}	
		return Redirect::to('admin/orders');
	}
	
	public function viewarchive(Request $request)
	{
		$pagesize = $request->get('pagesize');
		if($pagesize == "")
		{
			$pagesize = 10;
		}
		if(Auth::user()->roll == 2 || Auth::user()->roll == 4)
		{
			$userid = Auth::user()->id;
			$organization = user::where('id','=',$userid)->value('organization');
			$physicians = user::where('organization','=',$organization)->where('roll','=',3)->get();
			
			$physicians_id = [];
		   foreach($physicians as $physician)
		   {
			 $physicians_id[] = $physician->id;
			
		   }
		   $orders = order::leftjoin('manufacturers','manufacturers.id','=','orders.manufacturer_name')
                           ->leftjoin('users','users.id','=','orders.rep')
                           ->leftjoin('users as ob','ob.id','=','orders.orderby')
                           ->whereIn('orders.orderby',$physicians_id)
                           ->where('orders.is_delete','=','0')
                           ->where('orders.is_archive','=','1')
                           ->select('orders.*','users.name','ob.name as ob_name','manufacturers.manufacturer_name')
                           ->orderby('orders.id','DESC')
                           ->paginate($pagesize);
                   $count = order::where('orders.is_delete','=','0')->whereIn('orders.orderby',$physicians_id)->where('orders.is_archive','=','1')->count();
			
		}	
		else
		{
			$orders = order::leftjoin('manufacturers','manufacturers.id','=','orders.manufacturer_name')
                         ->leftjoin('users','users.id','=','orders.rep')
                         ->leftjoin('users as ob','ob.id','=','orders.orderby')
                         ->where('orders.is_delete','=','0')
                         ->where('orders.is_archive','=','1')
                         ->select('orders.*','users.name','ob.name as ob_name','manufacturers.manufacturer_name')
                         ->orderby('orders.id','DESC')->paginate($pagesize);
			$count = order::where('orders.is_delete','=','0')->where('orders.is_archive','=','1')->count();
		}
		
		return view('pages.viewarchive',compact('orders','count','pagesize'));
	}
	
	
	public function archiveordersearch()
	{
		$fieldname = Input::get('fieldName');
		$fieldvalue = Input::get('value');
		if(Auth::user()->roll == 2 || Auth::user()->roll == 4)
		{
			$userid = Auth::user()->id;
			$organization = user::where('id','=',$userid)->value('organization');
			$physicians = user::where('organization','=',$organization)->where('roll','=',3)->get();
			
			$physicians_id = [];
		   foreach($physicians as $physician)
		   {
			 $physicians_id[] = $physician->id;
			
		   }
		   
		   $search_orders = order::leftjoin('manufacturers','manufacturers.id','=','orders.manufacturer_name')
                           ->leftjoin('users','users.id','=','orders.rep')
                           ->leftjoin('users as ob','ob.id','=','orders.orderby')
                           ->whereIn('orders.orderby',$physicians_id)
                           ->where('orders.is_delete','=','0')
                           ->where('orders.is_archive','=','1')
                           ->select('orders.*','users.name','ob.name as ob_name','manufacturers.manufacturer_name')
                           ->orderby('orders.id','DESC')
						   ->where($fieldname,'LIKE',$fieldvalue.'%')
                           ->get();
            if($fieldvalue == "")
            {
            	 $search_orders = order::leftjoin('manufacturers','manufacturers.id','=','orders.manufacturer_name')
                           ->leftjoin('users','users.id','=','orders.rep')
                           ->leftjoin('users as ob','ob.id','=','orders.orderby')
                           ->whereIn('orders.orderby',$physicians_id)
                           ->where('orders.is_delete','=','0')
                           ->where('orders.is_archive','=','1')
                           ->select('orders.*','users.name','ob.name as ob_name','manufacturers.manufacturer_name')
                           ->orderby('orders.id','DESC')
						   ->get();
          
            }
			
		}	
		else
		{
			$search_orders = order::leftjoin('manufacturers','manufacturers.id','=','orders.manufacturer_name')
                                ->leftjoin('users','users.id','=','orders.rep')
                                ->leftjoin('users as ob','ob.id','=','orders.orderby')
                                ->where('orders.is_delete','=','0')
                                ->where('orders.is_archive','=','1')
                                ->select('orders.*','ob.name as ob_name','manufacturers.manufacturer_name','users.name')
                                ->where($fieldname,'LIKE',$fieldvalue.'%')
                                ->orderby('orders.id','DESC')->get();
            if($fieldvalue == "")
            {
            	$search_orders = order::leftjoin('manufacturers','manufacturers.id','=','orders.manufacturer_name')
                                ->leftjoin('users','users.id','=','orders.rep')
                                ->leftjoin('users as ob','ob.id','=','orders.orderby')
                                ->where('orders.is_delete','=','0')
                                ->where('orders.is_archive','=','1')
                                ->select('orders.*','ob.name as ob_name','manufacturers.manufacturer_name','users.name')
                                ->orderby('orders.id','DESC')->get();
            }
		}
		$data = $search_orders;
		
		if(count($data))
			return [
						'value' => $data,
						'status' => TRUE
				   ];
		else
			return [
						'value'=>'No Result Found',
						'status' => FALSE
				   ];
	}


	public function export()
	{
	
		$orderid = Input::get('order_check');
			
			$order_id = [];
			foreach($orderid as $order)
			{
				$order_id[] = $order;
				
			}
			
		$orders = order::leftjoin('manufacturers','manufacturers.id','=','orders.manufacturer_name')
                         ->leftjoin('users','users.id','=','orders.rep')
                         ->leftjoin('users as ob','ob.id','=','orders.orderby')
                         ->where('orders.is_delete','=','0')
                         ->select('orders.*','users.name','ob.name as ob_name','manufacturers.manufacturer_name')
        				 ->whereIn('orders.id',$order_id)
						 ->get();
		
	
		$filename = "orders.csv";
			$handle = fopen($filename, 'w+');
			fputcsv($handle, array('ID', 'Manufacturer Name', 'Device Name','Model No.','Unit Cost','System cost','CCO','Reimbursement','Order Date','Ordered By','Rep','Sent to','Status'));
		
			foreach($orders as $row) {
				fputcsv($handle, array($row['id'], $row['manufacturer_name'], $row['model_name'], $row['model_no'], $row['unit_cost'], $row['system_cost'], $row['cco'], $row['reimbrusement'], $row['order_date'], $row['ob_name'], $row['name'], $row['sent_to'], $row['status']));
			}
		
			fclose($handle);
		
			$headers = array(
				'Content-Type' => 'text/csv',
			);
		
			return Response::download($filename, 'orders.csv', $headers);
			return Redirect::to('admin/orders');
	}
	
	
	
	
	
	
}
