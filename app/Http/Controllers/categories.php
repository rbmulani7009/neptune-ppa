<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\category;

use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Validator;

use App\clients;

use App\project;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Input;

use App\project_clients;

use Auth;

use App\user;

use App\Categorysort;

class categories extends Controller
{
    public function index(Request $request)
	{
		$pagesize = $request->get('pagesize');
		if($pagesize == "")
		{
			$pagesize = 10;
		}
		if(Auth::user()->roll == 2)
		{
			$userid = Auth::user()->id;
			$organization = user::where('id','=',$userid)->value('organization');
			$categories = category::leftjoin('projects','category.project_name','=','projects.id')
						 ->join('project_clients','project_clients.project_id','=','category.project_name')
						 ->where('project_clients.client_name','=',$organization)
						 ->orderBy('category.id','desc')
						 ->select('category.id','projects.project_name','category.category_name','category.project_name as prname')				 						 ->where('category.is_active','=','1')
						 ->paginate($pagesize);

			$count = category::leftjoin('projects','category.project_name','=','projects.id')
					->join('project_clients','project_clients.project_id','=','category.project_name')
					->where('project_clients.client_name','=',$organization)
					->where('category.is_active','=',1)
					->count();
		}
		else
		{
			$categories = category::leftjoin('projects','category.project_name','=','projects.id')
						  ->select('category.id','projects.project_name','category.category_name','category.project_name as prname')						  ->where('category.is_active','=','1')
						  ->orderBy('category.id','desc')
						  ->paginate($pagesize);
		
			$count = category::leftjoin('projects','category.project_name','=','projects.id')
					 ->where('category.is_active','=',1)
					 ->count();
		}
		
		
		foreach($categories as $client)
		{
			$projectid = $client->prname;
			$client->client_count = project_clients::join('clients','clients.id','=','project_clients.client_name')
									->where('project_id','=',$projectid)
									->count();
		}
		return view('pages.category',compact('categories','count','pagesize'));
	}
	
	public function add()
	{
		$projects = ['0' => 'Project Name'] + Project::lists('project_name','id')->all();
		return view('pages.addcategory',compact('projects'));
	}
	
	public function create(Request $request)
	{
		$rules = array(
			'project_name'=> 'required|not_in:0',
			'category_name'=> 'required|unique:category,category_name'
		);
		$insertdata = array(
			'project_name' => $request->get('project_name'),
			'category_name' => $request->get('category_name'),
			'is_active' => '1',
			'is_delete' => '0'
		);


		
		$validator = Validator::make($insertdata,$rules);
		
		if($validator->fails())
		{
			return Redirect::to('admin/category/add')
			->withErrors($validator);
		}
		else
		{
			$ck =0;
			$category = category::insert($insertdata);
			$category = category::orderBy('id','desc')->value('id');
			$getclientname = project_clients::where('project_id','=',$request->get('project_name'))->get();
			foreach ($getclientname as $row) {
				$get_last_sort_number = Categorysort::where('client_name','=',$row->client_name)->orderBy('sort_number','desc')->value('sort_number');
				$get_last_sort_number = $get_last_sort_number == "" ? 0 : $get_last_sort_number;
				$get_last_sort_number = $get_last_sort_number + 1;

				$sortdata['sort_number'] =  $get_last_sort_number;
				$sortdata['client_name'] = $row->client_name;
				$sortdata['category_name'] = $category;

				$categorysort = new Categorysort();
				$categorysort->fill($sortdata);
				$categorysort->save();
			}


			return Redirect::to('admin/category');
		}
	}
	
	public function edit($id)
	{
		$clients = ['0' => 'Client Name'] + Clients::lists('client_name','id')->all();
		$projects = Project::where('is_delete','=',0)->lists('project_name','id')->all();
		$category = category::FindOrFail($id);
		return view('pages.editcategory',compact('projects','clients','category'));
	}
	
	public function update($id, Request $request)
	{
		$rules = array(
			'project_name'=> 'required|not_in:0',
			'category_name'=> 'required'
		);
		$updatedata = array(
			'project_name' => $request->get('project_name'),
			'category_name' => $request->get('category_name')
		);
		
		$validator = Validator::make($updatedata,$rules);
		
		if($validator->fails())
		{
			return Redirect::back()->withErrors($validator);
		}
		else
		{
			$categoryname = Input::get('category_name');
			$get_categoryname = category::where('id','=',$id)->value('category_name');
			if($categoryname == $get_categoryname)
			{
				$update_categoryname = DB::table('category')->where('id','=',$id)->update($updatedata);
				return Redirect::to('admin/category');
			}
			else
			{
				$check_category = category::where('category_name','=',$categoryname)->count();
				if($check_category >= 1)
				{
					return Redirect::back()
			->withErrors(['category_name' =>'Category name already exist in database.',]);
				}
				else
				{
					$update_category = DB::table('category')->where('id','=',$id)->update($updatedata);
					return Redirect::to('admin/category');
				}
			}


			
		}
		
	}
	
	public function search()
	{
		$fieldname = Input::get('fieldName');
		$fieldvalue = Input::get('value');
		
		if(Auth::user()->roll == 2)
		{
			$userid = Auth::user()->id;
			$organization = user::where('id','=',$userid)->value('organization');
			$search_category = category::leftjoin('projects','category.project_name','=','projects.id')
							  ->join('project_clients','project_clients.project_id','=','category.project_name')
							  ->where('project_clients.client_name','=',$organization)
							  ->orderBy('category.id','desc')
							  ->select('category.id','projects.project_name','category.category_name','category.project_name as prname')->where('category.is_active','=','1')
							  ->where($fieldname,'LIKE',$fieldvalue.'%')
							  ->get();
		}
		else
		{
			$search_category = category::leftjoin('projects','category.project_name','=','projects.id')
								->orderBy('category.id','desc')
								->select('category.id','projects.project_name','category.category_name','category.project_name as prname')						->where('category.is_active','=','1')
								->where($fieldname,'LIKE',$fieldvalue.'%')->get();
		}
		
		foreach($search_category as $client)
		{
			$projectid = $client->prname;
			$client->client_count = project_clients::join('clients','clients.id','=','project_clients.client_name')
									->where('project_id','=',$projectid)
									->count();
		
		}
		$data = $search_category;
		
		if(count($data))
			return [
						'value' => $data,
						'status' => TRUE
				   ];
		else
			return [
						'value'=>'No Result Found',
						'status' => FALSE
				   ];
	}
	
	public function viewclient($id)
	{	
		
		$viewclients = project_clients::where('project_id','=',$id)
						->leftjoin('clients','clients.id','=','project_clients.client_name')
						->join('state','state.id','=','clients.state')
						->select('clients.*','state.state_name')
						->get();
						
		return view('pages.viewclient',compact('viewclients'));
	}
	public function viewclientsearch()
	{
		$projectid = Input::get('projectid');
		$fieldname = Input::get('fieldName');
		$fieldvalue = Input::get('value');
		$viewclients = project_clients::leftjoin('clients','clients.id','=','project_clients.client_name')
						->join('state','state.id','=','clients.state')
						->select('clients.*','state.state_name')
						->where('project_id','=',$projectid)
						->where($fieldname,'LIKE','%'.$fieldvalue.'%')
						->get();
		$data = $viewclients;
		
		if(count($data))
			return [
						'value' => $data,
						'status' => TRUE
				   ];
		else
			return [
						'value'=>'No Result Found',
						'status' => FALSE
				   ];
	}public function remove($id)
	{
		$removedata = array('is_delete'=>'1','is_active'=>'0');
		$remove = DB::table('category')->where('id','=',$id)->delete();
		return Redirect::to('admin/category');
		
	}
}
