<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\roll;
use App\User;
use Auth;
use App\ScoreCard;
use App\ScoreCardImage;
use App\Month;


class ScoreCardController extends Controller
{
    public function index()
    {	
    	$year = ['' => 'Select Year'] + ScoreCard::where('userId',Auth::user()->id)->orderBy('id','asc')->groupBy('year')->pluck('year','id')->all();

    	return view('pages.frontend.scorecard',compact('year'));
    }

    public function getMonth()
    {
    	$scoreId = Input::get('scoreId');
    	$year = ScoreCard::where('id',$scoreId)->where('userId',Auth::user()->id)->first();
    	$data = ScoreCard::month()->where('year',$year['year'])->where('userId',Auth::user()->id)->get();
    	
    	if (count($data))
            return [
                'value' => $data,
                'status' => TRUE
            ];
        else
            return [
                'value' => 'No result Found',
                'status' => FALSE
            ];

    }

    public function getImage($id)
    {
    	$image = ScoreCardImage::where('scorecardId',$id)->get();
    	dd($image);
    }
}
