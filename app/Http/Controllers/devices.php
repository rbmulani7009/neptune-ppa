<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\project;
use App\category;
use App\manufacturers;
use App\device;
use App\User;
use App\device_custom_field;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\client_price;
use App\clients;
use App\project_clients;
use Auth;
use Response;
use Excel;
use View;
use App\order;
use App\Devicefeatures;
use App\Clientcustomfeatures;
use Carbon\Carbon;

// survey 
use App\Survey;
use App\SurveyAnswer;

// Custom Contact
use App\customContact;

// rep contact info
use App\RepContact;


class devices extends Controller
{

    public function index(Request $request)
    {
        $sort = $request->get('sortvalue');
        $pagesize = $request->get('pagesize');
        if ($pagesize == "") {
            $pagesize = 10;
        }

        $projects = ['0' => 'Select Project'] + Project::lists('project_name', 'id')->all();
        $category = ['0' => 'Select Category'] + Category::lists('category_name', 'id')->all();
        $manufacturer = ['0' => 'Select Manufacturer'] + Manufacturers::lists('manufacturer_name', 'id')->all();
        if (Auth::user()->roll == 2) {
            $userid = Auth::user()->id;
            $organization = user::where('id', '=', $userid)->value('organization');
            if ($sort == "") {
                $device_view = device::leftjoin('manufacturers', 'manufacturers.id', '=', 'device.manufacturer_name')
                    ->leftjoin('projects', 'projects.id', '=', 'device.project_name')
                    ->leftjoin('project_clients', 'project_clients.project_id', '=', 'projects.id')
                    ->leftjoin('category', 'category.id', '=', 'device.category_name')
                    ->where('project_clients.client_name', '=', $organization)
                    ->select('device.id', 'device.model_name', 'device.status', 'device.device_name', 'manufacturers.manufacturer_name as manu_name', 'projects.project_name', 'category.category_name')
                    ->where('device.is_delete', '=', '0')
                    ->orderBy('device.id', 'DESC')
                    ->paginate($pagesize);
            } else {
                $device_view = device::leftjoin('manufacturers', 'manufacturers.id', '=', 'device.manufacturer_name')
                    ->leftjoin('projects', 'projects.id', '=', 'device.project_name')
                    ->leftjoin('category', 'category.id', '=', 'device.category_name')
                    ->leftjoin('project_clients', 'project_clients.project_id', '=', 'projects.id')
                    ->select('device.id', 'device.model_name', 'device.status', 'device.device_name', 'manufacturers.manufacturer_name as manu_name', 'projects.project_name', 'category.category_name')
                    ->where('project_clients.client_name', '=', $organization)
                    ->where('device.is_delete', '=', '0')->orderBy($sort, 'asc')->paginate($pagesize);
            }


            $count = device::leftjoin('manufacturers', 'manufacturers.id', '=', 'device.manufacturer_name')
                ->leftjoin('projects', 'projects.id', '=', 'device.project_name')
                ->leftjoin('category', 'category.id', '=', 'device.category_name')
                ->leftjoin('project_clients', 'project_clients.project_id', '=', 'projects.id')
                ->where('project_clients.client_name', '=', $organization)
                ->where('device.is_delete', '=', '0')
                ->count();
        } else {
            if ($sort == "") {
                $device_view = device::leftjoin('manufacturers', 'manufacturers.id', '=', 'device.manufacturer_name')
                    ->leftjoin('projects', 'projects.id', '=', 'device.project_name')
                    ->leftjoin('category', 'category.id', '=', 'device.category_name')
                    ->select('device.id', 'device.model_name', 'device.status', 'device.device_name', 'manufacturers.manufacturer_name as manu_name', 'projects.project_name', 'category.category_name')
                    ->where('device.is_delete', '=', '0')
                    ->orderBy('device.id', 'DESC')
                    ->paginate($pagesize);
            } else {
                $device_view = device::leftjoin('manufacturers', 'manufacturers.id', '=', 'device.manufacturer_name')
                    ->leftjoin('projects', 'projects.id', '=', 'device.project_name')
                    ->leftjoin('category', 'category.id', '=', 'device.category_name')
                    ->select('device.id', 'device.model_name', 'device.status', 'device.device_name', 'manufacturers.manufacturer_name as manu_name', 'projects.project_name', 'category.category_name')
                    ->where('device.is_delete', '=', '0')
                    ->orderBy($sort, 'asc')
                    ->paginate($pagesize);
            }


            $count = device::leftjoin('manufacturers', 'manufacturers.id', '=', 'device.manufacturer_name')
                ->leftjoin('projects', 'projects.id', '=', 'device.project_name')
                ->leftjoin('category', 'category.id', '=', 'device.category_name')
                ->where('device.is_delete', '=', '0')
                ->count();
        }
        return view('pages.device', compact('device_view', 'projects', 'category', 'manufacturer', 'count', 'pagesize', 'sort'));
    }

    public function add()
    {
        $projects = ['0' => 'Select Project'] + Project::where('is_delete', '=', '0')->lists('project_name', 'id')->all();
        $manufacturer = ['0' => 'Select Manufacturer'] + Manufacturers::where('is_delete', '=', '0')->lists('manufacturer_name', 'id')->all();
        $users = ['0' => 'Select Rep'] + user::where('roll', 5)->where('is_delete', '=', '0')->lists('email', 'id')->all();
        return view('pages.adddevice', compact('projects', 'manufacturer', 'users'));
    }

    public function create()
    {
        $rules = array(
            'level_name' => 'required|not_in:0',
            'project_name' => 'required|not_in:0',
            'category_name' => 'required|not_in:0',
            'manufacturer_name' => 'required|not_in:0',
            'device_name' => 'required',
            'model_name' => 'required|unique:device,model_name',
            'device_image' => 'required',
            'rep_email' => 'required|not_in:0',
            'shock' => 'regex:/^[0-9]{1,3}([\/][0-9]{1,3})?$/',
            'size' => 'regex:/^[0-9]{1,3}([\/][0-9]{1,3})?$/',
            'status' => 'required|not_in:0'
        );


        /*  $exclusive_check = Input::get('chk_exclusive');
          $longevity_check = Input::get('chk_longevity');
          $size_check = Input::get('chk_size');
          $shock_check = Input::get('chk_shock');
          $research_check = Input::get('chk_research');
          $website_page_check = Input::get('chk_websitepage');
          $overall_value_check = Input::get('chk_overallvalue'); */

        $checkdevice = device::where('device_name', '=', Input::get('devicename'))->where('is_delete', '=', 0)->count();
        if ($checkdevice >= 1) {
            return Redirect::back()->withErrors(['device_name' => 'Device name already exist in database.']);
        } else {

            /*   if ($exclusive_check != "True") {
                   $exclusive_check = "False";
               }

               if ($longevity_check != "True") {
                   $longevity_check = "False";
               }

               if ($shock_check != "True") {
                   $shock_check = "False";
               }

               if ($size_check != "True") {
                   $size_check = "False";
               }

               if ($research_check != "True") {
                   $research_check = "False";
               }

               if ($website_page_check != "True") {
                   $website_page_check = "False";
               }

               if ($overall_value_check != "True") {
                   $overall_value_check = "False";
               } */

            $insertdata = array(
                "level_name" => Input::get('level'),
                "project_name" => Input::get('project_name'),
                "category_name" => Input::get('category_name'),
                "manufacturer_name" => Input::get('manufacturer_name'),
                "device_name" => Input::get('devicename'),
                "model_name" => Input::get('modelname'),
                "device_image" => Input::get('image_name'),
                "rep_email" => Input::get('rep_email'),
                "status" => Input::get('status'),
                "exclusive" => Input::get('exclusive'),
                //     "exclusive_check" => $exclusive_check,
                "longevity" => Input::get('longevity'),
                //    "longevity_check" => $longevity_check,
                "shock" => Input::get('shock'),
                //    "shock_check" => $shock_check,
                "size" => Input::get('size'),
                //    "size_check" => $size_check,
                "research" => Input::get('research'),
                //    "research_check" => $research_check,
                "website_page" => Input::get('websitepage'),
                //    "website_page_check" => $website_page_check,
                "url" => Input::get('url'),
                "overall_value" => Input::get('overall_value'),
                //    "overall_value_check" => $overall_value_check,
                "is_delete" => "0"
            );

            $validator = Validator::make($insertdata, $rules);
            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator);
            } else {

                $destinationpath = 'upload';
                $extenstion = Input::file('image')->getClientOriginalExtension();
                $filename = 'device' . '_' . rand(11111, 99999) . '.' . $extenstion;

                $insertdata = array(
                    "level_name" => Input::get('level'),
                    "project_name" => Input::get('project_name'),
                    "category_name" => Input::get('category_name'),
                    "manufacturer_name" => Input::get('manufacturer_name'),
                    "device_name" => Input::get('devicename'),
                    "model_name" => Input::get('modelname'),
                    "device_image" => $filename,
                    "rep_email" => Input::get('rep_email'),
                    "status" => Input::get('status'),
                    "exclusive" => Input::get('exclusive'),
                    //  "exclusive_check" => $exclusive_check,
                    "longevity" => Input::get('longevity'),
                    //  "longevity_check" => $longevity_check,
                    "shock" => Input::get('shock'),
                    //  "shock_check" => $shock_check,
                    "size" => Input::get('size'),
                    //  "size_check" => $size_check,
                    "research" => Input::get('research'),
                    //  "research_check" => $research_check,
                    "website_page" => Input::get('websitepage'),
                    //  "website_page_check" => $website_page_check,
                    "url" => Input::get('url'),
                    "overall_value" => Input::get('overall_value'),
                    //  "overall_value_check" => $overall_value_check,
                    "is_delete" => "0"
                );

                $insert_device = 0;
                $insert_device = device::insert($insertdata);
                $device_id = device::get()->last();
                $device_id = $device_id->id;
                $insertcustomfields = Input::get('fieldname');
                $insertcustomfieldvalue = Input::get('fieldvalue');
                $insertcustomfieldcheck = Input::get('chk_fieldname');
                for ($i = 0; $i < count($insertcustomfields); $i++) {
                    if ($insertcustomfields[$i] != "") {

                        if (!isset($insertcustomfieldcheck[$i])) {
                            $insertcustomfieldcheck[$i] = "False";
                        }

                        $insertrecord = array(
                            "device_id" => $device_id,
                            "field_name" => $insertcustomfields[$i],
                            "field_value" => $insertcustomfieldvalue[$i],
                            "field_check" => $insertcustomfieldcheck[$i]
                        );
                        $insert_custom_field = device_custom_field::insert($insertrecord);
                    }
                }
                if ($insert_device >= 0) {
                    $move = Input::file('image')->move($destinationpath, $filename);

                    return Redirect::to('admin/devices');
                }
            }
        }
    }

    public function view($id,Request $request)
    {
        $pagesize = $request->get('repPageSize');
        if ($pagesize == "") {
            $pagesize = 10;
        }

        $device_view = device::leftjoin('manufacturers', 'manufacturers.id', '=', 'device.manufacturer_name')
            ->leftjoin('projects', 'projects.id', '=', 'device.project_name')
            ->leftjoin('category', 'category.id', '=', 'device.category_name')
            ->leftjoin('users', 'users.id', '=', 'device.rep_email')
            ->select('device.*', 'users.email', 'manufacturers.manufacturer_name as manu_name', 'projects.project_name', 'category.category_name')
            ->where('device.id', '=', $id)
            ->first();

        if (Auth::user()->roll == 2) {
            $userid = Auth::user()->id;
            $organization = user::where('id', '=', $userid)->value('organization');


            $client_prices = client_price::join('clients', 'clients.id', '=', 'client_price.client_name')->select('client_price.*', 'clients.client_name')->where('client_price.device_id', '=', $id)->where('client_price.is_delete', '=', 0)
                ->where('client_price.client_name', '=', $organization)
                ->orderBy('client_price.id', 'DESC')->get();
        } else {
            $client_prices = client_price::join('clients', 'clients.id', '=', 'client_price.client_name')
                ->select('client_price.*', 'clients.client_name', 'client_price.client_name as client')
                ->where('client_price.device_id', '=', $id)
                ->where('client_price.is_delete', '=', 0)
                ->orderBy('client_price.id', 'DESC')
                ->get();
        }
        foreach ($client_prices as $client_price) {

            $deviceid = device::where('id', '=', $client_price->device_id)->value('device_name');


            $bulk_count = Order::join('users', 'users.id', '=', 'orders.orderby')
                ->where('orders.model_name', '=', $deviceid)
                ->where('users.organization', '=', $client_price->client)
                ->where('orders.bulk_check', '=', "True")
                ->where('orders.status', '!=', 'Cancelled')
                ->where('orders.unit_cost', '!=', "")
                ->count();

            $system_bulk_count = Order::join('users', 'users.id', '=', 'orders.orderby')
                ->where('orders.model_name', '=', $deviceid)
                ->where('users.organization', '=', $client_price->client)
                ->where('orders.bulk_check', '=', "True")
                ->where('orders.status', '!=', 'Cancelled')
                ->where('orders.system_cost', '!=', "")
                ->count();
            $client_price->remain_bulk = $client_price->bulk - $bulk_count;
            $client_price->remain_system_bulk = $client_price->system_bulk - $system_bulk_count;

        }


        $custom_fields = device_custom_field::join('device', 'device.id', '=', 'device_custom_field.device_id')
            ->select('device_custom_field.*')->where('device_id', '=', $id)->get();

            // device Features Tab
        $device_features = Devicefeatures::leftjoin('clients', 'clients.id', '=', 'device_features.client_name')
            ->leftjoin('device', 'device.id', '=', 'device_features.device_id')
            ->select('device_features.*', 'clients.client_name', 'device_features.client_name as client', 'device.longevity', 'device.shock', 'device.size', 'device.research', 'device.website_page', 'device.url', 'device.overall_value')
            ->where('device_features.device_id', '=', $id)
            ->orderBy('device_features.id', 'DESC')
            ->get();
            // Device Survey Tab
        $device_survey = Survey::where('deviceId', '=', $id)
            ->orderBy('id', 'DESC')
            ->get();
            // Device Contact Tab
        $deviceCustomContact = customContact::where('deviceId',$id)
                                            ->orderBy('id','DESC')
                                            ->get();

        // Device Rep User Tab
            $organization = device::where('id',$id)->first();
            $deviceRepUser = User::repcontact()->where('roll','5')->where('organization',$organization->manufacturer_name)->paginate($pagesize);
            foreach ($deviceRepUser as $row) {
               
                $row->repStatus =$row->repStatus==""?"No":"Yes";
            }
            // dd($deviceRepUser);
            $count= User::where('roll','5')->where('organization',$organization->manufacturer_name)->count();
            


            
        if (Auth::user()->roll == 2) {
        // device Features Tab
            $device_features = Devicefeatures::leftjoin('clients', 'clients.id', '=', 'device_features.client_name')
                ->select('device_features.*', 'clients.client_name', 'device_features.client_name as client', 'device.longevity', 'device.shock', 'device.size', 'device.research', 'device.website_page', 'device.url', 'device.overall_value')
                ->where('device_features.device_id', '=', $id)
                ->where('device_features.client_name', '=', Auth::user()->organization)
                ->orderBy('device_features.id', 'DESC')
                ->get();
        // Device Survey Tab
            $device_survey = Survey::where('deviceId', '=', $id)
                ->orderBy('id', 'DESC')
                ->get();
        // Device Contact Tab
            $deviceCustomContact = customContact::where('deviceId',$id)
                                            ->orderBy('id','DESC')
                                            ->get();
        // Device Rep User Tab
            $organization = device::where('id',$id)->first();

           $deviceRepUser = User::repcontact()->where('roll','5')->where('organization',$organization->manufacturer_name)->paginate($pagesize);

            foreach ($deviceRepUser as $row) {
               
                $row->repStatus =$row->repStatus==""?"No":"Yes";
            }

            $count= User::where('roll','5')->where('organization',$organization->manufacturer_name)->count();

        }


        return view('pages.viewdevice', compact('device_view', 'client_prices', 'custom_fields', 'device_features', 'device_survey','deviceCustomContact','deviceRepUser','count','pagesize'));
    }

    public function edit($id)
    {
        $projects = Project::where('is_delete', '=', 0)->lists('project_name', 'id')->all();
        $category = Category::where('is_delete', '=', 0)->lists('category_name', 'id')->all();
        $manufacturer = Manufacturers::where('is_delete', '=', '0')->lists('manufacturer_name', 'id')->all();
        $users = user::where('roll', 5)->where('is_delete', '=', '0')->lists('email', 'id')->all();
        $custom_fields = device_custom_field::join('device', 'device.id', '=', 'device_custom_field.device_id')->select('device_custom_field.*')->where('device_id', '=', $id)->get();
        $devices = device::FindOrFail($id);
        return view('pages.editdevice', compact('projects', 'manufacturer', 'devices', 'users', 'custom_fields', 'category'));
    }

    public function update($id)
    {
        $rules = array(
            'level_name' => 'required|not_in:0',
            'project_name' => 'required|not_in:0',
            'category_name' => 'required|not_in:0',
            'manufacturer_name' => 'required|not_in:0',
            'device_name' => 'required|not_in:0',
            'model_name' => 'required',
            'rep_email' => 'required|not_in:0',
            'shock' => 'regex:/^[0-9]{1,3}([\/][0-9]{1,3})?$/',
            'size' => 'regex:/^[0-9]{1,3}([\/][0-9]{1,3})?$/',
            'status' => 'required|not_in:0'
        );

        /*$exclusive_check = Input::get('chk_exclusive');
        $longevity_check = Input::get('chk_longevity');
        $size_check = Input::get('chk_size');
        $shock_check = Input::get('chk_shock');
        $research_check = Input::get('chk_research');
        $website_page_check = Input::get('chk_websitepage');
        $overall_value_check = Input::get('chk_overallvalue');

        if ($exclusive_check != "True") {
            $exclusive_check = "False";
        }

        if ($longevity_check != "True") {
            $longevity_check = "False";
        }

        if ($shock_check != "True") {
            $shock_check = "False";
        }

        if ($size_check != "True") {
            $size_check = "False";
        }

        if ($research_check != "True") {
            $research_check = "False";
        }

        if ($website_page_check != "True") {
            $website_page_check = "False";
        }

        if ($overall_value_check != "True") {
            $overall_value_check = "False";
        }*/

        $updatedata = array(
            "level_name" => Input::get('level_name'),
            "project_name" => Input::get('project_name'),
            "category_name" => Input::get('category_name'),
            "manufacturer_name" => Input::get('manufacturer_name'),
            "device_name" => Input::get('device_name'),
            "model_name" => Input::get('model_name'),
            "rep_email" => Input::get('rep_email'),
            "status" => Input::get('status'),
            "exclusive" => Input::get('exclusive'),
            //    "exclusive_check" => $exclusive_check,
            "longevity" => Input::get('longevity'),
            //    "longevity_check" => $longevity_check,
            "shock" => Input::get('shock'),
            //    "shock_check" => $shock_check,
            "size" => Input::get('size'),
            //    "size_check" => $size_check,
            "research" => Input::get('research'),
            //    "research_check" => $research_check,
            "website_page" => Input::get('website_page'),
            //    "website_page_check" => $website_page_check,
            "url" => Input::get('url'),
            "overall_value" => Input::get('overall_value'),
            //    "overall_value_check" => $overall_value_check,
            "is_delete" => "0"
        );

        if (Input::hasFile('image')) {
            $destinationpath = 'upload';
            $extenstion = Input::file('image')->getClientOriginalExtension();
            $filename = 'device' . '_' . rand(11111, 99999) . '.' . $extenstion;
            $updatedata["device_image"] = $filename;
        }

        $validator = Validator::make($updatedata, $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        } else {

            $insertcustomfields = Input::get('fieldname');
            $insertcustomfieldvalue = Input::get('fieldvalue');
            $insertcustomfieldcheck = Input::get('chk_fieldname');
            for ($i = 0; $i < count($insertcustomfields); $i++) {
                if (!isset($insertcustomfieldcheck[$i])) {
                    $insertcustomfieldcheck[$i] = "False";
                }

                if ($insertcustomfields[$i] != "") {

                    $insertrecord = array(
                        "device_id" => $id,
                        "field_name" => $insertcustomfields[$i],
                        "field_value" => $insertcustomfieldvalue[$i],
                        "field_check" => $insertcustomfieldcheck[$i]
                    );
                    $insert_custom_field = device_custom_field::insert($insertrecord);
                }
            }

            $customfieldid = Input::get('customhidden');
            $editcustomfields = Input::get('fieldnameedit');
            $editcustomfieldvalue = Input::get('fieldvalueedit');
            $editcustomfieldcheck = Input::get('chk_fieledit');
            for ($i = 0; $i < count($editcustomfields); $i++) {
                if (!isset($editcustomfieldcheck[$i])) {
                    $editcustomfieldcheck[$i] = "False";
                }

                $updatecustomfield = array(
                    "field_name" => $editcustomfields[$i],
                    "field_value" => $editcustomfieldvalue[$i],
                    "field_check" => $editcustomfieldcheck[$i]
                );

                $update_custom_field = DB::table('device_custom_field')->where('id', '=', $customfieldid[$i])->update($updatecustomfield);
            }


            $devicename = Input::get('device_name');
            $modelname = Input::get('model_name');
            $get_modelname = device::where('id', '=', $id)->value('model_name');
            $get_devicename = device::where('id', '=', $id)->value('device_name');
            if ($get_devicename == $devicename && $get_modelname == $modelname) {

                DB::table('device')->where('id', '=', $id)->update($updatedata);
            } else {

                if ($get_devicename != $devicename) {
                    $check_devices = device::where('device_name', '=', $devicename)->count();
                    if ($check_devices >= 1)
                        $errors['device_name'] = 'Device Name already exist in database.';
                }
                if ($get_modelname != $modelname) {
                    $check_model = device::where('model_name', '=', $modelname)->count();
                    if ($check_model >= 1)
                        $errors['model_name'] = 'Model Name already exist in database.';
                }

                if (isset($errors) || !empty($errors)) {
                    return Redirect::back()->withErrors($errors);
                }


                DB::table('device')->where('id', '=', $id)->update($updatedata);
            }
            if (isset($filename) || !empty($filename)) {
                Input::file('image')->move($destinationpath, $filename);
            }
            return Redirect::to('admin/devices');
        }
    }

    public function remove($id)
    {
        $removedata = array('is_delete' => '1');
        $remove = DB::table('device')->where('id', '=', $id)->delete();
        return Redirect::to('admin/devices');
    }

    public function search()
    {
        $fieldname = Input::get('fieldName');
        $fieldvalue = Input::get('value');
        if (Auth::user()->roll == 2) {
            $userid = Auth::user()->id;
            $organization = user::where('id', '=', $userid)->value('organization');

            $search_devices = device::leftjoin('manufacturers', 'manufacturers.id', '=', 'device.manufacturer_name')
                ->leftjoin('projects', 'projects.id', '=', 'device.project_name')
                ->leftjoin('category', 'category.id', '=', 'device.category_name')
                ->leftjoin('project_clients', 'project_clients.project_id', '=', 'projects.id')
                ->select('device.id', 'device.model_name', 'device.status', 'device.device_name', 'manufacturers.manufacturer_name as manu_name', 'projects.project_name', 'category.category_name')
                ->where('project_clients.client_name', '=', $organization)
                ->where('device.is_delete', '=', '0')->where($fieldname, 'LIKE', $fieldvalue . '%')
                ->orderBy('device.id', 'DESC')->get();
        } else {

            $search_devices = device::leftjoin('manufacturers', 'manufacturers.id', '=', 'device.manufacturer_name')
                ->leftjoin('projects', 'projects.id', '=', 'device.project_name')
                ->leftjoin('category', 'category.id', '=', 'device.category_name')
                ->select('device.id', 'device.model_name', 'device.device_name', 'manufacturers.manufacturer_name as manu_name', 'projects.project_name', 'category.category_name', 'device.status')
                ->where('device.is_delete', '=', '0')
                ->where($fieldname, 'LIKE', $fieldvalue . '%')
                ->orderBy('device.id', 'DESC')->get();
        }

        $data = $search_devices;


        if (count($data))
            return [
                'value' => $data,
                'status' => TRUE
            ];
        else
            return [
                'value' => 'No Result Found',
                'status' => FALSE
            ];
    }

    public function searchclientprice()
    {
        $id = Input::get('id');
        $fieldname = Input::get('fieldName');
        $fieldvalue = Input::get('value');


        if (Auth::user()->roll == 2) {
            $userid = Auth::user()->id;
            $organization = user::where('id', '=', $userid)->value('organization');

            $search_client_prices = client_price::join('clients', 'clients.id', '=', 'client_price.client_name')
                ->select('client_price.*', 'clients.client_name', 'client_price.client_name as client')
                ->where('client_price.device_id', '=', $id)
                ->where('client_price.is_delete', '=', 0)
                ->where($fieldname, 'LIKE', $fieldvalue . '%')
                ->where('client_price.client_name', '=', $organization)
                ->orderBy('client_price.id', 'DESC')->get();
        } else {
            $search_client_prices = client_price::join('clients', 'clients.id', '=', 'client_price.client_name')
                ->select('client_price.*', 'clients.client_name', 'client_price.client_name as client')
                ->where('client_price.device_id', '=', $id)
                ->where('client_price.is_delete', '=', 0)
                ->where($fieldname, 'LIKE', $fieldvalue . '%')
                ->orderBy('client_price.id', 'DESC')->get();
        }

        foreach ($search_client_prices as $search_client_price) {
            $deviceid = device::where('id', '=', $search_client_price->device_id)->value('device_name');

            $bulk_count = Order::join('users', 'users.id', '=', 'orders.orderby')
                ->where('orders.model_name', '=', $deviceid)
                ->where('users.organization', '=', $search_client_price->client)
                ->where('orders.bulk_check', '=', 'True')
                ->where('orders.status', '!=', 'Cancelled')
                ->where('orders.unit_cost', '!=', "")
                ->count();

            $system_bulk_count = Order::join('users', 'users.id', '=', 'orders.orderby')
                ->where('orders.model_name', '=', $deviceid)
                ->where('users.organization', '=', $search_client_price->client)
                ->where('orders.bulk_check', '=', 'True')
                ->where('orders.status', '!=', 'Cancelled')
                ->where('orders.system_cost', '!=', "")
                ->count();
            $search_client_price->remain_bulk = $search_client_price->bulk - $bulk_count;
            $search_client_price->remain_system_bulk = $search_client_price->system_bulk - $system_bulk_count;
        }

        $data = $search_client_prices;


        if (count($data))
            return [
                'value' => $data,
                'status' => TRUE
            ];
        else
            return [
                'value' => 'No Result Found',
                'status' => FALSE
            ];
    }

    public function clientprice($id)
    {
        $deviceid = $id;
        $projectname = device::where('id', '=', $deviceid)->where('is_delete', '=', '0')->value('project_name');
        $check_clientname = client_price::where('device_id', '=', $id)->get();
        if (Auth::user()->roll == 2) {
            $check_clientname = client_price::where('device_id', '=', $id)
                ->where('client_name', '=', Auth::user()->organization)
                ->get();
        }
        $clientname = [];;
        foreach ($check_clientname as $clients) {
            $clientname[] = $clients->client_name;

        }
        $manufacturer = ['0' => 'Client Name']
            + project_clients::join('clients', 'clients.id', '=', 'project_clients.client_name')
                ->where('project_clients.project_id', '=', $projectname)
                ->whereNotIn('project_clients.client_name', $clientname)
                ->lists('clients.client_name', 'clients.id')
                ->all();


        return view('pages.addclientprice', compact('deviceid', 'manufacturer'));
    }

    public function clientpricecreate()
    {
        $rules = array(
            'client_name' => 'required|not_in:0',
            'unit_cost' => 'required',
            'system_cost' => 'required',
            'order_email' => 'required|not_in:0'
        );
        $bulk_unit_cost_check = Input::get('chk_bulk_unit_cost');

        $unit_cost_check = Input::get('unit_cost_check');
        $system_cost_check = Input::get('system_cost_check');

        $bulk_check = Input::get('chk_bulk');
        $cco_discount_check = Input::get('chk_cco_discount');
        $cco_check = Input::get('chk_cco');
        $unit_rep_cost_check = Input::get('chk_unit_rep_cost');
        $unit_rep_discount_check = Input::get('chk_unit_rep_discount');
        $bulk_system_cost_check = Input::get('chk_bulk_system_cost');
        $system_rep_cost_check = Input::get('chk_system_rep_cost');
        $system_rep_discount_check = Input::get('chk_system_rep_discount');
        $reimbursement_check = Input::get('chk_reimbursement');
        $system_bulk_check = Input::get('system_bulk_check');

        if ($bulk_unit_cost_check != "True") {
            $bulk_unit_cost_check = "False";
        }

        if ($cco_discount_check != "True") {
            $cco_discount_check = "False";
        }

        if ($cco_check != "True") {
            $cco_check = "False";
        }


        if ($bulk_check != "True") {
            $bulk_check = "False";
        }

        if ($unit_rep_cost_check != "True") {
            $unit_rep_cost_check = "False";
        }

        if ($unit_rep_discount_check != "True") {
            $unit_rep_discount_check = "False";
        }


        if ($system_bulk_check != "True") {
            $system_bulk_check = "False";
        }

        if ($bulk_system_cost_check != "True") {
            $bulk_system_cost_check = "False";
        }

        if ($system_rep_cost_check != "True") {
            $system_rep_cost_check = "False";
        }

        if ($system_rep_discount_check != "True") {
            $system_rep_discount_check = "False";
        }

        if ($reimbursement_check != "True") {
            $reimbursement_check = "False";
        }

        if ($reimbursement_check != "True") {
            $reimbursement_check = "False";
        }

        if ($unit_cost_check != "True") {
            $unit_cost_check = "False";
        }

        if ($system_cost_check != "True") {
            $system_cost_check = "False";
        }


        $insertdata = array(
            "device_id" => Input::get('device_id'),
            "client_name" => Input::get('manufacturer_name'),
            "unit_cost" => Input::get('unit_cost'),
            "unit_cost_check" => $unit_cost_check,
            "system_cost_check" => $system_cost_check,
            "bulk_unit_cost" => Input::get('bulk_unit_cost'),
            "bulk_unit_cost_check" => $bulk_unit_cost_check,
            "bulk" => Input::get('bulk'),
            "bulk_check" => $bulk_check,
            "cco" => Input::get('cco_discount'),
            "cco_check" => $cco_discount_check,
            "cco_discount" => Input::get('cco'),
            "cco_discount_check" => $cco_check,
            "unit_rep_cost" => Input::get('unit_rep_cost'),
            "unit_rep_cost_check" => $unit_rep_cost_check,
            "unit_repless_discount" => Input::get('unit_rep_discount'),
            "unit_repless_discount_check" => $unit_rep_discount_check,
            "system_cost" => Input::get('system_cost'),
            "system_bulk" => Input::get('system_bulk'),
            "system_bulk_check" => $system_bulk_check,
            "bulk_system_cost" => Input::get('bulk_system_cost'),
            "bulk_system_cost_check" => $bulk_system_cost_check,
            "system_repless_cost" => Input::get('system_rep_cost'),
            "system_repless_cost_check" => $system_rep_cost_check,
            "system_repless_discount" => Input::get('system_rep_discount'),
            "system_repless_discount_check" => $system_rep_discount_check,
            "reimbursement" => Input::get('reimbursement'),
            "reimbursement_check" => $reimbursement_check,
            "order_email" => Input::get('order_email'),
            "is_delete" => "0",
            "is_updated" => Carbon::now()
        );
        $validator = Validator::make($insertdata, $rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        } else {
            if (Input::get('unit_cost') >= Input::get('system_cost')) {

                return Redirect::back()->withErrors(['unit_cost' => 'System cost should not be less than unit cost.']);
            } else {
                $insert_clientprice = 0;
                $insert_clientprice = client_price::insert($insertdata);
                if ($insert_clientprice >= 0) {
                    return Redirect::to('admin/devices/view/' . Input::get('device_id'));
                } else {
                    echo "Cancle";
                }
            }
        }
    }

    public function clientpriceedit($id)
    {


        $clientprice = client_price::FindOrFail($id);
        $deviceid = device::where('id', '=', $clientprice->device_id)->value('device_name');

        $bulk_count = Order::join('users', 'users.id', '=', 'orders.orderby')
            ->where('orders.model_name', '=', $deviceid)
            ->where('users.organization', '=', $clientprice->client_name)
            ->where('orders.bulk_check', '=', 'True')
            ->where('orders.status', '!=', 'Cancelled')
            ->where('orders.unit_cost', '!=', "")
            ->count();
        $system_bulk_count = Order::join('users', 'users.id', '=', 'orders.orderby')
            ->where('orders.model_name', '=', $deviceid)
            ->where('users.organization', '=', $clientprice->client_name)
            ->where('orders.bulk_check', '=', 'True')
            ->where('orders.status', '!=', 'Cancelled')
            ->where('orders.system_cost', '!=', "")
            ->count();
        $clientprice->bulk = $clientprice->bulk - $bulk_count;
        $clientprice->system_bulk = $clientprice->system_bulk - $system_bulk_count;
        $clientprice->bulk = $clientprice->bulk < 0 ? '0' : $clientprice->bulk;
        $clientprice->sytem_bulk = $clientprice->bulk < 0 ? '0' : $clientprice->system_bulk;

        $deviceid = $clientprice->device_id;
        $projectname = device::where('id', '=', $deviceid)->where('is_delete', '=', '0')->value('project_name');
        $check_clientname = client_price::where('device_id', '=', $deviceid)
            ->where('client_name', '!=', $clientprice->client_name)
            ->get();
        $clientname = [];
        foreach ($check_clientname as $clients) {
            $clientname[] = $clients->client_name;

        }

        $manufacturer = project_clients::join('clients', 'clients.id', '=', 'project_clients.client_name')
            ->where('project_clients.project_id', '=', $projectname)
            ->whereNotIn('project_clients.client_name', $clientname)
            ->lists('clients.client_name', 'clients.id')
            ->all();
        $orderemail = user::where('roll', '=', 4)->lists('email', 'id')->all();

        return view('pages.editclientprice', compact('manufacturer', 'clientprice', 'orderemail'));
    }

    public function clientpriceupdate($id)
    {
        $rules = array(
            'client_name' => 'required|not_in:0',
            'unit_cost' => 'required',
            'system_cost' => 'required',
            'order_email' => 'required|not_in:0'
        );
        $bulk_unit_cost_check = Input::get('chk_bulk_unit_cost');
        $unit_cost_check = Input::get('unit_cost_check');
        $system_cost_check = Input::get('system_cost_check');
        $bulk_check = Input::get('chk_bulk');
        $cco_discount_check = Input::get('chk_cco_discount');
        $cco_check = Input::get('chk_cco');
        $unit_rep_cost_check = Input::get('chk_unit_rep_cost');
        $unit_rep_discount_check = Input::get('chk_unit_rep_discount');
        $bulk_system_cost_check = Input::get('chk_bulk_system_cost');
        $system_rep_cost_check = Input::get('chk_system_rep_cost');
        $system_rep_discount_check = Input::get('chk_system_rep_discount');
        $reimbursement_check = Input::get('chk_reimbursement');
        $system_bulk_check = Input::get('system_bulk_check');


        $clientprice = client_price::where('id', '=', $id)->get();
        foreach ($clientprice as $client_price) {
            $device_id = $client_price->device_id;
            $clientname = $client_price->client_name;
            $bulk = $client_price->bulk;
        }
        $deviceid = device::where('id', '=', $device_id)->value('device_name');


        $bulk_count = Order::join('users', 'users.id', '=', 'orders.orderby')
            ->where('orders.model_name', '=', $deviceid)
            ->where('users.organization', '=', $clientname)
            ->where('orders.bulk_check', '=', 'True')
            ->where('orders.status', '!=', 'Cancelled')
            ->where('orders.unit_cost', '!=', "")
            ->count();
        $system_bulk_count = Order::join('users', 'users.id', '=', 'orders.orderby')
            ->where('orders.model_name', '=', $deviceid)
            ->where('users.organization', '=', $clientname)
            ->where('orders.bulk_check', '=', 'True')
            ->where('orders.status', '!=', 'Cancelled')
            ->where('orders.system_cost', '!=', "")
            ->count();

        $bulk = Input::get('bulk') + $bulk_count;
        $system_bulk = Input::get('system_bulk') + $system_bulk_count;


        if ($bulk_unit_cost_check != "True") {
            $bulk_unit_cost_check = "False";
        }

        if ($system_bulk_check != "True") {
            $system_bulk_check = "False";
        }

        if ($bulk_check != "True") {
            $bulk_check = "False";
        }

        if ($cco_discount_check != "True") {
            $cco_discount_check = "False";
        }

        if ($cco_check != "True") {
            $cco_check = "False";
        }

        if ($unit_rep_cost_check != "True") {
            $unit_rep_cost_check = "False";
        }

        if ($unit_rep_discount_check != "True") {
            $unit_rep_discount_check = "False";
        }


        if ($bulk_system_cost_check != "True") {
            $bulk_system_cost_check = "False";
        }

        if ($system_rep_cost_check != "True") {
            $system_rep_cost_check = "False";
        }

        if ($system_rep_discount_check != "True") {
            $system_rep_discount_check = "False";
        }

        if ($reimbursement_check != "True") {
            $reimbursement_check = "False";
        }

        if ($unit_cost_check != "True") {
            $unit_cost_check = "False";
        }

        if ($system_cost_check != "True") {
            $system_cost_check = "False";
        }


        $updatedata = array(
            "client_name" => Input::get('client_name'),
            "unit_cost" => Input::get('unit_cost'),
            "unit_cost_check" => $unit_cost_check,
            "system_cost_check" => $system_cost_check,
            "system_bulk" => $system_bulk,
            "system_bulk_check" => $system_bulk_check,
            "bulk_unit_cost" => Input::get('bulk_unit_cost'),
            "bulk_unit_cost_check" => $bulk_unit_cost_check,
            "bulk" => $bulk,
            "bulk_check" => $bulk_check,
            "cco" => Input::get('cco'),
            "cco_check" => $cco_discount_check,
            "cco_discount" => Input::get('cco_discount'),
            "cco_discount_check" => $cco_check,
            "unit_rep_cost" => Input::get('unit_rep_cost'),
            "unit_rep_cost_check" => $unit_rep_cost_check,
            "unit_repless_discount" => Input::get('unit_repless_discount'),
            "unit_repless_discount_check" => $unit_rep_discount_check,
            "system_cost" => Input::get('system_cost'),
            "bulk_system_cost" => Input::get('bulk_system_cost'),
            "bulk_system_cost_check" => $bulk_system_cost_check,
            "system_repless_cost" => Input::get('system_repless_cost'),
            "system_repless_cost_check" => $system_rep_cost_check,
            "system_repless_discount" => Input::get('system_repless_discount'),
            "system_repless_discount_check" => $system_rep_discount_check,
            "reimbursement" => Input::get('reimbursement'),
            "reimbursement_check" => $reimbursement_check,
            "order_email" => Input::get('order_email'),
            "is_delete" => "0",
            "is_updated" => Carbon::now()
        );

        $validator = Validator::make($updatedata, $rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        } else {
            if (Input::get('unit_cost') >= Input::get('system_cost')) {

                return Redirect::back()->withErrors(['unit_cost' => 'System cost should not be less than unit cost.']);
            } else {

                $update_clientprice = 0;
                $update_clientprice = DB::table('client_price')->where('id', '=', $id)->update($updatedata);
                $get_deviceid = client_price::where('id', '=', $id)->value('device_id');
                if ($update_clientprice >= 0) {
                    return Redirect::to('admin/devices/view/' . $get_deviceid);
                } else {
                    echo "Cancle";
                }
            }
        }
    }

    public function getcategory()
    {
        $projectid = Input::get('projectid');
        $getcategory = category::where('is_delete', '=', 0)->where('project_name', '=', $projectid)->get();
        $data = $getcategory;

        if (count($data))
            return [
                'value' => $data,
                'status' => TRUE
            ];
        else
            return [
                'value' => 'No result Found',
                'status' => FALSE
            ];
    }

    public function priceremove($id)
    {
        $removedata = array('is_delete' => '1');
        $remove = DB::table('client_price')->where('id', '=', $id)->delete();
        return Redirect::to('admin/devices');
    }

    public function customfieldremove($id)
    {
        $remove = DB::table('device_custom_field')->where('id', '=', $id)->delete();
        return Redirect::back();
    }

    public function getrepemail()
    {
        $projectid = Input::get('projectid');
        $manufacturer = Input::get('manufacturer');
        $getcategory = user::where('organization', '=', $manufacturer)
            ->where('roll', '=', 5)
            ->where('projectname', '=', $projectid)
            ->where('is_delete', '=', 0)
            ->get();

        $data = $getcategory;

        if (count($data))
            return [
                'value' => $data,
                'status' => TRUE
            ];
        else
            return [
                'value' => 'No result Found',
                'status' => FALSE
            ];
    }

    public function getorderemail()
    {
        $clientid = Input::get('clientid');
        $getcategory = user::where('organization', '=', $clientid)
            ->where('roll', '=', 4)
            ->where('is_delete', '=', 0)
            ->get();

        $data = $getcategory;

        if (count($data))
            return [
                'value' => $data,
                'status' => TRUE
            ];
        else
            return [
                'value' => 'No Result Found',
                'status' => FALSE
            ];
    }

    public function export()
    {
        $deviceid = Input::get('chk_device');

        $device_id = [];
        foreach ($deviceid as $device) {
            $device_id[] = $device;
        }


        $device_view = device::leftjoin('manufacturers', 'manufacturers.id', '=', 'device.manufacturer_name')
            ->leftjoin('projects', 'projects.id', '=', 'device.project_name')
            ->leftjoin('category', 'category.id', '=', 'device.category_name')
            ->select('device.id', 'device.model_name', 'device.status', 'device.device_name', 'manufacturers.manufacturer_name as manu_name', 'projects.project_name', 'category.category_name')
            ->whereIn('device.id', $device_id)
            ->get();


        $client_prices = client_price::leftjoin('clients', 'clients.id', '=', 'client_price.client_name')
            ->leftjoin('device', 'device.id', '=', 'client_price.device_id')
            ->select('client_price.*', 'clients.client_name', 'device.device_name')
            ->whereIn('client_price.device_id', $device_id)
            ->where('client_price.is_delete', '=', 0)
            ->orderBy('client_price.id', 'DESC')
            ->get();

        foreach ($device_view as $row) {

            $device_data[] = [
                $row['id'],
                $row['manu_name'],
                $row['device_name'],
                $row['model_name'],
                $row['project_name'],
                $row['category_name'],
                $row['status']
            ];
        }

        foreach ($client_prices as $row) {
            $client_price_data[] = [
                $row['device_name'],
                $row['client_name'],
                $row['unit_cost'],
                $row['bulk_unit_cost'],
                $row['unit_rep_cost'],
                $row['unit_repless_discount'],
                $row['system_cost'],
                $row['bulk_system_cost'],
                $row['cco_discount'],
                $row['system_repless_cost'],
                $row['system_repless_discount'],
                $row['bulk'],
                $row['reimbursement']
            ];
        }

        Excel::create('Devices', function ($excel) use ($device_data, $client_price_data) {

            $excel->setTitle('Devices');
            $excel->setCreator('Admin')->setCompany('Neptune-PPA');
            $excel->setDescription('Devices file');

            $excel->sheet('Devices Details', function ($sheet) use ($device_data) {
                $sheet->row(1, array('Id', 'Menu Name', 'Device Name', 'Model Name', 'Project Name', 'Category Name', 'Status'));
                $sheet->row($sheet->getHighestRow(), function ($row) {
                    $row->setFontWeight('bold');
                });
                foreach ($device_data as $row) {
                    $sheet->appendRow($row);
                }
            });

            $excel->sheet('Client Price Details', function ($sheet) use ($client_price_data) {
                $sheet->row(1, array('Device Name', 'Client Name', 'Unit Cost', 'Dis. Unit Cost%', 'Repless Disc.', 'Repless Disc%', 'System Cost', 'Disc.Sys.Cost%', 'CCO Disc. %', 'Repless Disc.', 'Repless Disc.%', 'Bulk', 'Reimb.'));
                $sheet->row($sheet->getHighestRow(), function ($row) {
                    $row->setFontWeight('bold');
                });
                foreach ($client_price_data as $row) {
                    $sheet->appendRow($row);
                }
            });
        })->download('xlsx');

        return Redirect::to('admin/devices');
    }

    public function import()
    {
        if (Input::hasFile('device_import')) {
            $path = Input::file('device_import')->getRealPath();
            $data = Excel::load($path, function ($reader) {

            })->get();

            if (!empty($data) && $data->count()) {
                foreach ($data as $key => $value) {

                    $level = array("Advanced Level", "Entry Level");
                    $exclusivecheck = array("True", "False");
                    $longevitycheck = array("True", "False");
                    $shockcheck = array("True", "False");
                    $sizecheck = array("True", "False");
                    $researchcheck = array("True", "False");
                    $websitecheck = array("True", "False");
                    $overallvalue = array("Low", "Medium", "High");
                    $overallvaluecheck = array("True", "False");
                    $statuscheck = array("Enabled", "Disabled");

                    if (in_array($value->level_name, $level)) {
                        $flag1 = true;
                    } else {
                        $value->level_name = $value->level_name . "(Wrong data)";
                        $flag1 = false;
                    }

                    if (in_array($value->exclusive_check, $exclusivecheck)) {
                        $flag2 = true;
                    } else {
                        $value->exclusive_check = $value->exclusive_check . "(Wrong data)";
                        $flag2 = false;
                    }

                    if (in_array($value->longevity_check, $longevitycheck)) {
                        $flag3 = true;
                    } else {
                        $value->longevity_check = $value->longevity_check . "(Wrong data)";
                        $flag3 = false;
                    }

                    if (in_array($value->shock_check, $shockcheck)) {
                        $flag4 = true;
                    } else {
                        $value->shock_check = $value->shock_check . "(Wrong data)";
                        $flag4 = false;
                    }

                    if (in_array($value->size_check, $sizecheck)) {
                        $flag5 = true;
                    } else {
                        $value->size_check = $value->size_check . "(Wrong data)";
                        $flag5 = false;
                    }

                    if (in_array($value->research_check, $researchcheck)) {
                        $flag6 = true;
                    } else {
                        $value->research_check = $value->research_check . "(Wrong data)";
                        $flag6 = false;
                    }

                    if (in_array($value->website_page_check, $websitecheck)) {
                        $flag7 = true;
                    } else {
                        $value->website_page_check = $value->website_page_check . "(Wrong data)";
                        $flag7 = false;
                    }

                    if (in_array($value->overall_value, $overallvalue)) {
                        $flag8 = true;
                    } else {
                        $value->overall_value = $value->overall_value . "(Wrong data)";
                        $flag8 = false;
                    }

                    if (in_array($value->overall_value_check, $overallvaluecheck)) {
                        $flag9 = true;
                    } else {
                        $value->overall_value_check = $value->overall_value_check . "(Wrong data)";
                        $flag9 = false;
                    }

                    if (in_array($value->status, $statuscheck)) {
                        $flag10 = true;
                    } else {
                        $value->status = $value->status . "(Wrong data)";
                        $flag10 = false;
                    }

                    if (preg_match("/^[1-9][0-9]*$/", $value->longevity)) {
                        $flag11 = true;
                    } else {
                        $value->longevity = $value->longevity . "(Wrong data)";
                        $flag11 = false;
                    }

                    if (preg_match("/^[0-9]{1,3}([\/][0-9]{1,3})?$/", $value->shock)) {
                        $flag12 = true;
                    } else {
                        $value->shock = $value->shock . "(Wrong data)";
                        $flag12 = false;
                    }

                    if (preg_match("/^[0-9]{1,3}([\/][0-9]{1,3})?$/", $value->size)) {
                        $flag13 = true;
                    } else {
                        $value->size = $value->size . "(Wrong data)";
                        $flag13 = false;
                    }

                    if (preg_match('/^(http|https):\\/\\/[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}' . '((:[0-9]{1,5})?\\/.*)?$/i', $value->url)) {
                        $flag14 = true;
                    } else {
                        $value->url = $value->url . "(Wrong data)";
                        $flag14 = false;
                    }


                    $manufacturer = manufacturers::where('manufacturer_name', '=', $value->manufacturer_name)->value('id');
                    $project = project::where('project_name', '=', $value->project_name)->value('id');
                    $category = category::where('category_name', '=', $value->category_name)->value('id');
                    $repemail = user::where('email', '=', $value->rep_email)->value('id');

                    $device_check = device::where('device_name', '=', $value->device_name)->count();
                    $model_check = device::where('model_name', '=', $value->model_no)->count();

                    if ($manufacturer != "") {
                        $flag15 = true;
                    } else {
                        $manufacturer = $value->manufacturer_name . "(Not available)";
                        $flag15 = false;
                    }

                    if ($project != "") {
                        $flag16 = true;
                    } else {
                        $project = $value->project_name . "(Not available)";
                        $flag16 = false;
                    }

                    if ($category != "") {
                        $flag17 = true;
                    } else {
                        $category = $value->category_name . "(Not available)";
                        $flag17 = false;
                    }


                    if ($repemail != "") {
                        $flag19 = true;
                    } else {
                        $repemail = $value->rep_email . "(Not available)";
                        $flag19 = false;
                    }


                    $insert = ['level_name' => $value->level_name,
                        'project_name' => $project,
                        'category_name' => $category,
                        'manufacturer_name' => $manufacturer,
                        'device_name' => $value->device_name,
                        'model_name' => $value->model_no,
                        'device_image' => "default.jpg",
                        'rep_email' => $repemail,
                        'status' => $value->status,
                        'exclusive' => $value->exclusive,
                        'exclusive_check' => $value->exclusive_check,
                        'longevity' => $value->longevity,
                        'longevity_check' => $value->longevity_check,
                        'shock' => $value->shock,
                        'shock_check' => $value->shock_check,
                        'size' => $value->size,
                        'size_check' => $value->size_check,
                        'research' => $value->research,
                        'research_check' => $value->research_check,
                        'website_page' => $value->website_page,
                        'website_page_check' => $value->website_page_check,
                        'url' => $value->url,
                        'overall_value' => $value->overall_value,
                        'overall_value_check' => $value->overall_value_check
                    ];


                    if ($flag1 && $flag2 && $flag3 && $flag4 && $flag5 && $flag6 && $flag7 && $flag8 && $flag9 && $flag10 && $flag11 && $flag12 && $flag13 && $flag14 && $flag15 && $flag16 && $flag17 && $flag19 && $device_check == 0 && $model_check == 0) {
                        device::insert($insert);

                    } else {

                        return Redirect::to('admin/devices');
                        /* $wrongdata = array('level_name' => $value->level_name,
                          'project_name'=> $project,
                          'category_name'=>$category,
                          'manufacturer_name'=>$manufacturer,
                          'device_name'=>$value->device_name,
                          'model_name'=>$value->model_no,
                          'device_image'=>"default.jpg",
                          'order_email'=>$orderemail,
                          'rep_email'=>$repemail,
                          'status'=>$value->status,
                          'exclusive'=>$value->exclusive,
                          'exclusive_check'=>$value->exclusive_check,
                          'longevity'=>$value->longevity,
                          'longevity_check'=>$value->longevity_check,
                          'shock'=>$value->shock,
                          'shock_check'=>$value->shock_check,
                          'size'=>$value->size,
                          'size_check'=>$value->size_check,
                          'research'=>$value->research,
                          'research_check'=>$value->research_check,
                          'website_page'=>$value->website_page,
                          'website_page_check'=>$value->website_page_check,
                          'url'=>$value->url,
                          'overall_value'=>$value->overall_value,
                          'overall_value_check'=>$value->overall_value_check
                          );
						*/

                    }
                }


                return Redirect::to('admin/devices');
            }
        }
    }


    public function devicefeatures($id)
    {

        $deviceid = $id;
        $projectname = device::where('id', '=', $deviceid)->where('is_delete', '=', '0')->value('project_name');
        $devices = device::findOrFail($id);
        $check_clientname = Devicefeatures::where('device_id', '=', $id)->get();


        $clientname = [];
        foreach ($check_clientname as $clients) {
            $clientname[] = $clients->client_name;

        }
        $client_name = ['' => 'Client Name']
            + project_clients::join('clients', 'clients.id', '=', 'project_clients.client_name')
                ->where('project_clients.project_id', '=', $projectname)
                ->whereNotIn('project_clients.client_name', $clientname)
                ->lists('clients.client_name', 'clients.id')
                ->all();
        $custom_field = device_custom_field::where('device_id', '=', $id)->get();
        return view('pages.adddevicefeatures', compact('devices', 'deviceid', 'client_name', 'custom_field'));
    }


    public function devicefeaturesstore(Request $request)
    {
        $rules = array(
            'client_name' => 'required',
        );

        $longevity_check = $request->get('chk_longevity') == 'true' ? 'True' : 'False';
        $shock_check = $request->get('chk_shock_ct') == 'true' ? 'True' : 'False';
        $size_check = $request->get('chk_size') == 'true' ? 'True' : 'False';
        $research_check = $request->get('chk_research') == 'true' ? 'True' : 'False';
        $site_info_check = $request->get('chk_site_info') == 'true' ? 'True' : 'False';
        $chk_overall_value = $request->get('chk_overall_value') == 'true' ? 'True' : 'False';


        $insertdata = array(
            'device_id' => $request->get('device_id'),
            'client_name' => $request->get('client_name'),
            //    'longevity' => $request->get('longevity'),
            'longevity_check' => $request->get('chk_longevity'),
            //    'shock' => $request->get('shock'),
            'shock_check' => $request->get('chk_shock_ct'),
            //    'size' => $request->get('size'),
            'size_check' => $request->get('chk_size'),
            //    'research' => $request->get('research'),
            'research_check' => $request->get('chk_research'),
            //    'site_info' => $request->get('site_info'),
            'siteinfo_check' => $request->get('chk_site_info'),
            //    'url' => $request->get('url'),
            //    'overall_value' => $request->get('overall_value'),
            'overall_value_check' => $request->get('chk_overall_value'),
        );

        $validator = Validator::make($insertdata, $rules);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator);
        }

        $device_id = $request->get('device_id');
        $client_name = $request->get('client_name');
        $customfieldid = $request->get('customhidden');
        $insertcustomfieldcheck = $request->get('chk_field');
        $insertcustomhiddencheck = $request->get('chk_hd_field');

        for ($i = 0; $i < count($customfieldid); $i++) {
            if ($customfieldid[$i] != "") {


                $insertrecord = array(
                    "device_id" => $device_id,
                    "client_name" => $client_name,
                    "field_check" => $insertcustomhiddencheck[$i],
                    "c_id" => $customfieldid[$i]
                );
                $insert_custom_field = Clientcustomfeatures::insert($insertrecord);

            }
        }

        $features = new Devicefeatures();
        $features->fill($insertdata);
        $features->save();


        return Redirect::to('admin/devices/view/' . $request->get('device_id'));


    }

    public function devicefeaturesedit($id)
    {
        $clientanme = Devicefeatures::where('id', $id)->value('client_name');
        $devicefeatures = device::leftJoin('device_features', 'device_features.device_id', '=', 'device.id')
            ->select('device_features.*', 'device.exclusive', 'device.longevity', 'device.shock', 'device.size', 'device.research', 'device.website_page', 'device.url', 'device.overall_value')
            ->where('device_features.id', '=', $id)->first();

        $deviceid = Devicefeatures::where('id', $id)->value('device_id');
        $custom_fields = device_custom_field::where('device_id', '=', $deviceid)
            ->get();

        foreach ($custom_fields as $row) {
            $row->field_check = Clientcustomfeatures::where('c_id', '=', $row->id)->where('client_name', '=', $clientanme)->value('field_check');

        }


        $projectname = device::where('id', '=', $deviceid)->where('is_delete', '=', '0')->value('project_name');
        $check_clientname = Devicefeatures::where('device_id', '=', $deviceid)
            ->where('client_name', '!=', $devicefeatures->client_name)
            ->get();
        $clientname = [];
        foreach ($check_clientname as $clients) {
            $clientname[] = $clients->client_name;

        }

        $client_name = ['' => 'Client Name']
            + project_clients::join('clients', 'clients.id', '=', 'project_clients.client_name')
                ->where('project_clients.project_id', '=', $projectname)
                ->whereNotIn('project_clients.client_name', $clientname)
                ->lists('clients.client_name', 'clients.id')
                ->all();


        return view('pages.editdevicefeatures', compact('custom_fields', 'deviceid', 'client_name', 'devicefeatures'));
    }

    public function devicefeaturesupdate(Request $request, $id)
    {

        $rules = array(
            'client_name' => 'required',
        );

        $longevity_check = $request->get('chk_longevity') == 'true' ? 'True' : 'False';
        $shock_check = $request->get('shock_check') == 'true' ? 'True' : 'False';
        $size_check = $request->get('size_check') == 'true' ? 'True' : 'False';
        $research_check = $request->get('research_check') == 'true' ? 'True' : 'False';
        $site_info_check = $request->get('siteinfo_check') == 'true' ? 'True' : 'False';
        $chk_overall_value = $request->get('overall_value_check') == 'true' ? 'True' : 'False';


        $updatedata = array(
            'device_id' => $request->get('device_id'),
            'client_name' => $request->get('client_name'),
            //  'longevity' => $request->get('longevity'),
            'longevity_check' => $request->get('chk_longevity'),
            //    'shock' => $request->get('shock'),
            'shock_check' => $request->get('shock_check'),
            //    'size' => $request->get('size'),
            'size_check' => $request->get('size_check'),
            //    'research' => $request->get('research'),
            'research_check' => $request->get('research_check'),
            //    'site_info' => $request->get('site_info'),
            'siteinfo_check' => $request->get('siteinfo_check'),
            //    'url' => $request->get('url'),
            //    'overall_value' => $request->get('overall_value'),
            'overall_value_check' => $request->get('overall_value_check'),
        );

        $validator = Validator::make($updatedata, $rules);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator);
        }

        $removeoldfield = Clientcustomfeatures::where('device_id', '=', $request->get('device_id'))
            ->where('client_name', '=', $request->get('client_name'))->delete();
        /*insert new custom field */
        $device_id = $request->get('device_id');
        $client_name = $request->get('client_name');
        $customfieldid = $request->get('customhidden');
        $insertcustomfieldcheck = $request['chk_field'];
        $insertcustomhiddencheck = $request->get('chk_hd_field');


        for ($i = 0; $i < count($customfieldid); $i++) {
            if ($customfieldid[$i] != "") {


                $insertrecord = array(
                    "device_id" => $device_id,
                    "client_name" => $client_name,
                    "field_check" => $insertcustomhiddencheck[$i],
                    "c_id" => $customfieldid[$i]
                );
                $insert_custom_field = Clientcustomfeatures::insert($insertrecord);

            }
        }

        /*update custom field*/
        /*  $customfieldid = Input::get('customhidden');
          $editcustomfields = Input::get('fieldnameedit');
          $editcustomfieldvalue = Input::get('fieldvalueedit');
          $editcustomfieldcheck = Input::get('chk_fieledit');
          for ($i = 0; $i < count($editcustomfields); $i++) {
              if (!isset($editcustomfieldcheck[$i])) {
                  $editcustomfieldcheck[$i] = "False";
              }

              $updatecustomfield = array(
                  "field_name" => $editcustomfields[$i],
                  "field_value" => $editcustomfieldvalue[$i],
                  "field_check" => $editcustomfieldcheck[$i]
              );

              $update_custom_field = DB::table('device_custom_field')->where('id', '=', $customfieldid[$i])->update($updatecustomfield);
          }*/

        $features = Devicefeatures::where('id', '=', $id)->first();
        $features->fill($updatedata);
        $features->update();


        return Redirect::to('admin/devices/view/' . $request->get('device_id'));

    }

    public function devicefeaturesremove($id)
    {
        $remove = devicefeatures::where('id', $id)->delete();
        return Redirect::back();
    }


    public function serarchdevicefeatures(Request $request)
    {

        $fieldname = Input::get('fieldName');
        $fieldvalue = Input::get('value');
        $deviceid = Input::get('deviceid');


        $searchfeatures = Devicefeatures::leftjoin('clients', 'clients.id', '=', 'device_features.client_name')
            ->select('device_features.*', 'clients.client_name', 'device_features.client_name as client')
            ->where('device_features.device_id', '=', $deviceid)
            ->where($fieldname, 'LIKE', $fieldvalue . '%')
            ->orderBy('device_features.id', 'DESC')
            ->get();
        if (Auth::user()->roll == 2) {
            $searchfeatures = Devicefeatures::leftjoin('clients', 'clients.id', '=', 'device_features.client_name')
                ->select('device_features.*', 'clients.client_name', 'device_features.client_name as client')
                ->where('device_features.device_id', '=', $deviceid)
                ->where('device_features.client_name', '=', Auth::user()->organization)
                ->where($fieldname, 'LIKE', $fieldvalue . '%')
                ->orderBy('device_features.id', 'DESC')
                ->get();

        }

        $data = $searchfeatures;

        if (count($data))
            return [
                'value' => $data,
                'status' => TRUE
            ];
        else
            return [
                'value' => 'No result Found',
                'status' => FALSE
            ];
    }


// Device survey Module Start
    public function deviceSurvey($id)
    {
        $deviceid = $id;
        $projectname = device::where('id', '=', $deviceid)->where('is_delete', '=', '0')->value('project_name');
        $devices = device::findOrFail($id);
        $check_clientname = Survey::where('deviceId', '=', $id)->get();


        $clientname = [];
        foreach ($check_clientname as $clients) {
            $clientname[] = $clients->clientId;

        }
        $client_name = ['' => 'Client Name']
            + project_clients::join('clients', 'clients.id', '=', 'project_clients.client_name')
                ->where('project_clients.project_id', '=', $projectname)
                ->whereNotIn('project_clients.client_name', $clientname)
                ->lists('clients.client_name', 'clients.id')
                ->all();

        return view('pages.devicesurvey.addDeviceSurvey', compact('devices', 'deviceid', 'client_name'));

    }

    public function deviceSurveyStore(Request $request)
    {

        $rules = array(
            'clientId' => 'required',
            'status' => 'required'
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();

        }
        $data = $request->all();

        $data['que_1_check'] = $request->get('que_1_check') == 'True' ? 'True' : 'False';
        $data['que_2_check'] = $request->get('que_2_check') == 'True' ? 'True' : 'False';
        $data['que_3_check'] = $request->get('que_3_check') == 'True' ? 'True' : 'False';
        $data['que_4_check'] = $request->get('que_4_check') == 'True' ? 'True' : 'False';
        $data['que_5_check'] = $request->get('que_5_check') == 'True' ? 'True' : 'False';
        $data['que_6_check'] = $request->get('que_6_check') == 'True' ? 'True' : 'False';
        $data['que_7_check'] = $request->get('que_7_check') == 'True' ? 'True' : 'False';
        $data['que_8_check'] = $request->get('que_8_check') == 'True' ? 'True' : 'False';

        $data['deviceId'] = $request['device_id'];

        $survey = new Survey;
        $survey->fill($data);
        if ($survey->save()) {
            Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> Your Survey Added Sucessfully.!! </div>');

            return redirect('admin/devices/view/' . $data['deviceId']);
        }

    }

    public function deviceSurveyEdit($id)
    {

        $device_survey = Survey::where('id', '=', $id)->first();

        $projectname = device::where('id', '=', $device_survey->deviceId)->where('is_delete', '=', '0')->value('project_name');
        $devices = device::findOrFail($device_survey->deviceId);

        $check_clientname = Survey::where('deviceId', '=', $device_survey->deviceId)
            ->where('clientId', '!=', $device_survey->clientId)->get();


        $clientname = [];

        foreach ($check_clientname as $clients) {
            $clientname[] = $clients->clientId;

        }

        $client_name = ['' => 'Client Name']
            + project_clients::join('clients', 'clients.id', '=', 'project_clients.client_name')
                ->where('project_clients.project_id', '=', $projectname)
                ->whereNotIn('project_clients.client_name', $clientname)
                ->lists('clients.client_name', 'clients.id')
                ->all();


        return view('pages.devicesurvey.editDeviceSurvey', compact('devices', 'client_name', 'device_survey'));
    }

    public function deviceSurveyUpdate(Request $request, $id)
    {
        $rules = array(
            'clientId' => 'required',
            'status' => 'required'
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();

        }
        $data['deviceId'] = $request['device_id'];
        $data = $request->except('_token', '_method', 'device_id');

        $data['que_1_check'] = $request->get('que_1_check') == 'True' ? 'True' : 'False';
        $data['que_2_check'] = $request->get('que_2_check') == 'True' ? 'True' : 'False';
        $data['que_3_check'] = $request->get('que_3_check') == 'True' ? 'True' : 'False';
        $data['que_4_check'] = $request->get('que_4_check') == 'True' ? 'True' : 'False';
        $data['que_5_check'] = $request->get('que_5_check') == 'True' ? 'True' : 'False';
        $data['que_6_check'] = $request->get('que_6_check') == 'True' ? 'True' : 'False';
        $data['que_7_check'] = $request->get('que_7_check') == 'True' ? 'True' : 'False';
        $data['que_8_check'] = $request->get('que_8_check') == 'True' ? 'True' : 'False';

        $data['deviceId'] = $request['device_id'];

        $update_survey = Survey::where('id', '=', $id)->update($data);

        return redirect('admin/devices/view/' . $data['deviceId']);
    }

    public function deviceSurveyRemove($id)
    {
        $remove = Survey::where('id', $id)->delete();
        return Redirect::back();
    }

    public function deviceSurveyCopy($id)
    {
        
        $data = Survey::where('id', '=', $id)->first();

        $projectname = device::where('id', '=', $data->deviceId)->where('is_delete', '=', '0')->value('project_name');
        $devices = device::findOrFail($data->deviceId);

        $check_clientname = Survey::where('deviceId', '=', $data->deviceId)
//            ->where('client_name','!=',$data->client_name)
            ->get();


        $clientname = [];

        foreach ($check_clientname as $clients) {
            $clientname[] = $clients->clientId;

        }
        $client_name = ['' => 'Client Name']
            + project_clients::join('clients', 'clients.id', '=', 'project_clients.client_name')
                ->where('project_clients.project_id', '=', $projectname)
                ->whereNotIn('project_clients.client_name', $clientname)
                ->lists('clients.client_name', 'clients.id')
                ->all();

        return view('pages.devicesurvey.copyDeviceSurvey', compact('devices', 'client_name', 'data'));
    }

    public function deviceSurveySearch(Request $request)
    {
        $data = $request->all();
        $deviceId = $data['device_id'];
        $client = $data['search'][0];
        $que1 = $data['search'][1];
        $que2 = $data['search'][2];
        $que3 = $data['search'][3];
        $que4 = $data['search'][4];
        $que5 = $data['search'][5];
        $que6 = $data['search'][6];
        $que7 = $data['search'][7];
        $que8 = $data['search'][8];

        $searchfeatures = Survey::clientname()->where('survey.deviceId', '=', $deviceId);

        if (!empty($client)) {
            $searchfeatures = $searchfeatures->where('clients.client_name', 'LIKE', $client . '%');

        }
        if (!empty($que1)) {
            $searchfeatures = $searchfeatures->where('que_1', 'LIKE', $que1 . '%');

        }
        if (!empty($que2)) {
            $searchfeatures = $searchfeatures->where('que_2', 'LIKE', $que2 . '%');

        }
        if (!empty($que3)) {
            $searchfeatures = $searchfeatures->where('que_3', 'LIKE', $que3 . '%');

        }
        if (!empty($que4)) {
            $searchfeatures = $searchfeatures->where('que_4', 'LIKE', $que4 . '%');

        }
        if (!empty($que5)) {
            $searchfeatures = $searchfeatures->where('que_5', 'LIKE', $que5 . '%');

        }
        if (!empty($que6)) {
            $searchfeatures = $searchfeatures->where('que_6', 'LIKE', $que6 . '%');

        }
        if (!empty($que7)) {
            $searchfeatures = $searchfeatures->where('que_7', 'LIKE', $que7 . '%');

        }
        if (!empty($que8)) {
            $searchfeatures = $searchfeatures->where('que_8', 'LIKE', $que8 . '%');

        }


        $searchfeatures = $searchfeatures->orderBy('survey.id', 'DESC')
            ->get();


        if (Auth::user()->roll == 2) {
            $searchfeatures = Survey::clientname()->where('survey.deviceId', '=', $deviceId)
                ->where('survey.client_name', '=', Auth::user()->organization);

            if (!empty($client)) {
                $searchfeatures = $searchfeatures->where('clients.client_name', 'LIKE', $client . '%');

            }
            if (!empty($que1)) {
                $searchfeatures = $searchfeatures->where('que_1', 'LIKE', $que1 . '%');

            }
            if (!empty($que2)) {
                $searchfeatures = $searchfeatures->where('que_2', 'LIKE', $que2 . '%');

            }
            if (!empty($que3)) {
                $searchfeatures = $searchfeatures->where('que_3', 'LIKE', $que3 . '%');

            }
            if (!empty($que4)) {
                $searchfeatures = $searchfeatures->where('que_4', 'LIKE', $que4 . '%');

            }
            if (!empty($que5)) {
                $searchfeatures = $searchfeatures->where('que_5', 'LIKE', $que5 . '%');

            }
            if (!empty($que6)) {
                $searchfeatures = $searchfeatures->where('que_6', 'LIKE', $que6 . '%');

            }
            if (!empty($que7)) {
                $searchfeatures = $searchfeatures->where('que_7', 'LIKE', $que7 . '%');

            }
            if (!empty($que8)) {
                $searchfeatures = $searchfeatures->where('que_8', 'LIKE', $que8 . '%');

            }
            $searchfeatures = $searchfeatures->orderBy('survey.id', 'DESC')
                ->get();

        }

        $data = $searchfeatures;

        if (count($data))
            return [
                'value' => $data,
                'status' => TRUE
            ];
        else
            return [
                'value' => 'No result Found',
                'status' => FALSE
            ];


    }

    public function deviceSurveyView($id,Request $request)
    {   
        $pagesize = $request->get('repPageSize');

        if ($pagesize == "") {
            $pagesize = 10;
        }

        $survey= Survey::where('id','=',$id)->first();
        $device_survey = SurveyAnswer::where('surveyId',$id)
        ->paginate($pagesize);;

        $count= SurveyAnswer::where('surveyId',$id)->count();
        return view('pages.devicesurvey.answerDeviceSurvey',compact('device_survey','survey','pagesize','count'));
    }

    public function deviceSurveyAnswerRemove($id)
    {
        $remove = SurveyAnswer::where('id', $id)->delete();
        return Redirect::back();
    }

    public function deviceSurveyAnswerSearch(Request $request)
    {
        $data = $request->all();
        $surveyId = $data['surveyId'];
        $deviceId = $data['device_id'];
        $client = $data['search'][0];
        $que1 = $data['search'][1];
        $que2 = $data['search'][2];
        $que3 = $data['search'][3];
        $que4 = $data['search'][4];
        $que5 = $data['search'][5];
        $que6 = $data['search'][6];
        $que7 = $data['search'][7];
        $que8 = $data['search'][8];

        $searchfeatures = SurveyAnswer::username()->where('survey_answer.deviceId', '=', $deviceId)
            ->where('survey_answer.surveyId',$surveyId);

        if (!empty($client)) {
            $searchfeatures = $searchfeatures->where('users.name', 'LIKE', $client . '%');

        }
        if (!empty($que1)) {
            $searchfeatures = $searchfeatures->where('que_1', 'LIKE', $que1 . '%');

        }
        if (!empty($que2)) {
            $searchfeatures = $searchfeatures->where('que_2', 'LIKE', $que2 . '%');

        }
        if (!empty($que3)) {
            $searchfeatures = $searchfeatures->where('que_3', 'LIKE', $que3 . '%');

        }
        if (!empty($que4)) {
            $searchfeatures = $searchfeatures->where('que_4', 'LIKE', $que4 . '%');

        }
        if (!empty($que5)) {
            $searchfeatures = $searchfeatures->where('que_5', 'LIKE', $que5 . '%');

        }
        if (!empty($que6)) {
            $searchfeatures = $searchfeatures->where('que_6', 'LIKE', $que6 . '%');

        }
        if (!empty($que7)) {
            $searchfeatures = $searchfeatures->where('que_7', 'LIKE', $que7 . '%');

        }
        if (!empty($que8)) {
            $searchfeatures = $searchfeatures->where('que_8', 'LIKE', $que8 . '%');

        }


        $searchfeatures = $searchfeatures->orderBy('survey_answer.id', 'DESC')
            ->get();


        if (Auth::user()->roll == 2) {
            $searchfeatures = SurveyAnswer::username()->where('survey_answer.deviceId', '=', $deviceid);
                // ->where('survey.client_name', '=', Auth::user()->organization);

            if (!empty($client)) {
                $searchfeatures = $searchfeatures->where('users.name', 'LIKE', $client . '%');

            }
            if (!empty($que1)) {
                $searchfeatures = $searchfeatures->where('que_1', 'LIKE', $que1 . '%');

            }
            if (!empty($que2)) {
                $searchfeatures = $searchfeatures->where('que_2', 'LIKE', $que2 . '%');

            }
            if (!empty($que3)) {
                $searchfeatures = $searchfeatures->where('que_3', 'LIKE', $que3 . '%');

            }
            if (!empty($que4)) {
                $searchfeatures = $searchfeatures->where('que_4', 'LIKE', $que4 . '%');

            }
            if (!empty($que5)) {
                $searchfeatures = $searchfeatures->where('que_5', 'LIKE', $que5 . '%');

            }
            if (!empty($que6)) {
                $searchfeatures = $searchfeatures->where('que_6', 'LIKE', $que6 . '%');

            }
            if (!empty($que7)) {
                $searchfeatures = $searchfeatures->where('que_7', 'LIKE', $que7 . '%');

            }
            if (!empty($que8)) {
                $searchfeatures = $searchfeatures->where('que_8', 'LIKE', $que8 . '%');

            }
            $searchfeatures = $searchfeatures->orderBy('survey_answer.id', 'DESC')
                ->get();

        }

        $data = $searchfeatures;

        if (count($data))
            return [
                'value' => $data,
                'status' => TRUE
            ];
        else
            return [
                'value' => 'No result Found',
                'status' => FALSE
            ];
    }
// Device Survey Module End

// Custom Contact information module start 

    public function customContact($id)
    {
        $deviceid = $id;
        $projectname = device::where('id', '=', $deviceid)->where('is_delete', '=', '0')->value('project_name');
        $devices = device::findOrFail($id);
        $check_clientname = customContact::where('deviceId', '=', $id)->get();


        $clientname = [];
        foreach ($check_clientname as $clients) {
            $clientname[] = $clients->clientId;

        }
        $client_name = ['' => 'Client Name']
            + project_clients::join('clients', 'clients.id', '=', 'project_clients.client_name')
                ->where('project_clients.project_id', '=', $projectname)
                ->whereNotIn('project_clients.client_name', $clientname)
                ->lists('clients.client_name', 'clients.id')
                ->all();

        return view('pages.customcontact.addCustomContact', compact('devices', 'deviceid', 'client_name'));
    }

    public function contactStore(Request $request)
    {
        $rules = array(
            'clientId' => 'required',
            'order_email'=>'required'
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();

        }
        $data = $request->all();

        $custom = new customContact;
        $custom->fill($data);
        if ($custom->save()) {
            Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> Your Survey Added Sucessfully.!! </div>');

            return redirect('admin/devices/view/' . $data['deviceId']);
        }

    }

    public function getOrderMail()
    {
         $userId = Input::get('userId');

         $orderuser = User::where('organization',$userId)->where('roll','4')->get();
         
         $data = $orderuser;

        if (count($data))
            return [
                'value' => $data,
                'status' => TRUE
            ];
        else
            return [
                'value' => 'No result Found',
                'status' => FALSE
            ];
    }

    public function customContactEdit($id)
    {
        $customContact = customContact::where('id', '=', $id)->first();

        $projectname = device::where('id', '=', $customContact->deviceId)->where('is_delete', '=', '0')->value('project_name');
        $devices = device::findOrFail($customContact->deviceId);

        $check_clientname = customContact::where('deviceId', '=', $customContact->deviceId)
            ->where('clientId', '!=', $customContact->clientId)->get();


        $clientname = [];

        foreach ($check_clientname as $clients) {
            $clientname[] = $clients->clientId;

        }

        $client_name = ['' => 'Client Name']
            + project_clients::join('clients', 'clients.id', '=', 'project_clients.client_name')
                ->where('project_clients.project_id', '=', $projectname)
                ->whereNotIn('project_clients.client_name', $clientname)
                ->lists('clients.client_name', 'clients.id')
                ->all();


        return view('pages.customcontact.editCustomContact', compact('devices', 'client_name', 'customContact'));
    }

    public function customContactUpdate(Request $request,$id)
    {
        $rules = array(
            'clientId' => 'required',
            'order_email'=>'required'
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();

        }
        
        $data = $request->except('_token', '_method');
       
        $custom = customContact::where('id', '=', $id)->update($data);
      
            return redirect('admin/devices/view/' . $data['deviceId']);
     
        
    }

    public function customContactRemove($id)
    {
        $remove = customContact::where('id', $id)->delete();
        return Redirect::back();
    }

    public function contactSearch(Request $request){

         $data = $request->all();
         
        // dd($data);
        $deviceId = $data['device_id'];
        $client = $data['search'][0];
        $ordermail = $data['search'][1];
        $cc1 = $data['search'][2];
        $cc2 = $data['search'][3];
        $cc3 = $data['search'][4];
        $cc4 = $data['search'][5];
        $cc5 = $data['search'][6];
        $subject = $data['search'][7];

        $searchfeatures = customContact::clientname()->where('custom_contact_info.deviceId', '=', $deviceId);

        if (!empty($client)) {

            $searchfeatures = $searchfeatures->where('clients.client_name', 'LIKE', $client . '%');

        }
        if (!empty($ordermail)) {
            $searchfeatures = $searchfeatures->where('users.email', 'LIKE', $ordermail . '%');

        }
        if (!empty($cc1)) {
            $searchfeatures = $searchfeatures->where('cc1', 'LIKE', $cc1 . '%');

        }
        if (!empty($cc2)) {
            $searchfeatures = $searchfeatures->where('cc2', 'LIKE', $cc2 . '%');

        }
        if (!empty($cc3)) {
            $searchfeatures = $searchfeatures->where('cc3', 'LIKE', $cc3 . '%');

        }
        if (!empty($cc4)) {
            $searchfeatures = $searchfeatures->where('cc4', 'LIKE', $cc4. '%');

        }
        if (!empty($cc5)) {
            $searchfeatures = $searchfeatures->where('cc5', 'LIKE', $cc5 . '%');

        }
        if (!empty($subject)) {
            $searchfeatures = $searchfeatures->where('subject', 'LIKE', $subject . '%');

        }
        


        $searchfeatures = $searchfeatures->orderBy('custom_contact_info.id', 'DESC')
            ->get();

        $data = $searchfeatures;

        if (count($data))
            return [
                'value' => $data,
                'status' => TRUE
            ];
        else
            return [
                'value' => 'No result Found',
                'status' => FALSE
            ];
    }



    // Custom Contact information module start 

    // Device Rep User Details Start

        public function repstatus(Request $request)
        {
            $deviceId=Input::get('deviceId');
            $repId=Input::get('repId');
            $status=Input::get('repStatus');

            if($status=='Yes'){

            $data['deviceId'] = $deviceId;
            $data['repId'] = $repId;
            $data['repStatus']=$status;
            // dd($data['status']);
            $rep = new RepContact;
            $rep->fill($data);
            if ($rep->save()) {
                    if (count($rep))
                    return [
                        'value' => $rep,
                        'status' => TRUE
                    ];
                else
                    return [
                        'value' => 'No result Found',
                        'status' => FALSE
                    ];
            }
            }else{
                 $remove = RepContact::where('repId', $repId)->delete();

                 if (count($remove))
                    return [
                        'value' => $remove,
                        'status' => TRUE
                    ];
                else
                    return [
                        'value' => 'No result Found',
                        'status' => FALSE
                    ];
                 
            }


        }

        public function repStatusEdit($id,$deviceId)
        {
            // Device Rep User Tab
            $organization = device::where('id',$deviceId)->first();

            $deviceRepUser = User::where('roll','5')->where('organization',$organization->manufacturer_name)->where('id',$id)->first();

            
                $repSta = RepContact::where('repId',$deviceRepUser->id)->where('deviceId',$deviceId)->value('repStatus');
                // dd($repSta);
                $repStatus= $repSta ==""?"No":"Yes";
            
            
            return view('pages.repcontact.editRepContact', compact('deviceRepUser','deviceId','repStatus'));
        }

        public function repStatusUpdate(Request $request,$id)
        {
        
            $check = RepContact::where('repId',$id)->first();
            if(!empty($check)){

                if($request['status']!='Yes'){
                   
                    $remove = RepContact::where('repId', $id)->delete();
                    return redirect('admin/devices/view/' . $request['deviceId']);
                }
            return redirect('admin/devices/view/' . $request['deviceId']);

            }else{
                
                $add['repId']=$id;
                $add['deviceId']=$request['deviceId'];
                $add['repStatus']=$request['status'];
                $rep = new RepContact;
                $rep->fill($add);
                if ($rep->save()) {
                    return redirect('admin/devices/view/' . $request['deviceId']);
                }
            }

        }

        public function repinfoexport(Request $request)
        {
            $id=$request['deviceId'];
            $chkRep=$request['chk_rep'];

            $organization = device::where('id',$id)->first();
            $deviceRepUser =User::repcontact()->where('users.roll','5')->where('users.organization',$organization->manufacturer_name)->whereIn('users.id',$chkRep)->get();
            foreach ($deviceRepUser as $row) {
               
                $row->repStatus =$row->repStatus==""?"No":"Yes";
            }

            
            foreach ($deviceRepUser as $row) {

            $rep_data[] = [
                $row['id'],
                $row['name'],
                $row['email'],
                $row['mobile'],
                $row['title'],
                $row['manufacturer_name'],
                $row['repStatus']
            ];
        }
        

            Excel::create('Rep_contact_info', function ($excel) use ($rep_data) {

                $excel->setTitle('Rep Contact Info');
                $excel->setCreator('Admin')->setCompany('Neptune-PPA');
                $excel->setDescription('Rep Contact Information');

                $excel->sheet('Rep Contact Info', function ($sheet) use ($rep_data) {
                    $sheet->row(1, array('Id', 'Name', 'Email', 'Mobile', 'Title', 'Company', 'Status'));
                    $sheet->row($sheet->getHighestRow(), function ($row) {
                        $row->setFontWeight('bold');
                    });
                    foreach ($rep_data as $row) {
                        $sheet->appendRow($row);
                    }
                });
            })->download('xlsx');

            return redirect('admin/devices/view/' . $id);

                
        }

        public function repcontactsearch(Request $request)
        {
            $data = $request->all();
            
            $deviceId = $data['deviceId'];
            $name = $data['search'][0];
            $email = $data['search'][1];
            $mobile = $data['search'][2];
            $title = $data['search'][3];
            $company = $data['search'][4];

             $searchrep = User::repcontact()->where('roll','5');

        if (!empty($name)) {

            $searchrep = $searchrep->where('users.name', 'LIKE', $name . '%');

        }
        if (!empty($email)) {
            $searchrep = $searchrep->where('users.email', 'LIKE', $email . '%');

        }
        if (!empty($mobile)) {
            $searchrep = $searchrep->where('users.mobile', 'LIKE', $mobile . '%');

        }
        if (!empty($title)) {
            $searchrep = $searchrep->where('users.title', 'LIKE', $title . '%');

        }
        if (!empty($company)) {
            $searchrep = $searchrep->where('manufacturers.manufacturer_name', 'LIKE', $company . '%');

        }
       
        


        $searchrep = $searchrep->orderBy('users.id', 'DESC')
            ->get();

             foreach ($searchrep as $row) {
               
                $row->repStatus =$row->repStatus==""?"No":"Yes";
                $row->device = $deviceId;
            }


        $data = $searchrep;
      
        if (count($data))
            return [
                'value' => $data,
                'status' => TRUE
            ];
        else
            return [
                'value' => 'No result Found',
                'status' => FALSE
            ];

        }

    // Device Rep User Details End

}