<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\project;
use App\manufacturers;
use App\device;
use App\schedule;
use App\User;
use App\clients;
use App\project_clients;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Auth;
use Mail;
use Response;


class schedules extends Controller {

    public function index(Request $request) {
        $pagesize = $request->get('pagesize');
        if ($pagesize == "") {
            $pagesize = 10;
        }
        if (Auth::user()->roll == 2) {
            $userid = Auth::user()->id;
            $organization = user::where('id', '=', $userid)->value('organization');
            $schedules = schedule::leftjoin('projects', 'projects.id', '=', 'schedule.project_name')
						->leftjoin('users', 'users.id', '=', 'schedule.physician_name')
						->leftjoin('manufacturers', 'manufacturers.id', '=', 'schedule.manufacturer')
						->leftjoin('device', 'device.id', '=', 'schedule.device_name')
						->leftjoin('clients', 'clients.id', '=', 'schedule.client_name')
						->where('users.organization', '=', $organization)
						->select('schedule.*', 'clients.client_name', 'users.name', 'manufacturers.manufacturer_name', 'device.device_name as device', 'projects.project_name as pro_name')
						->where('schedule.is_delete', '=', '0')->orderby('schedule.id', '=', 'DESC')->paginate($pagesize);
			
        	$count = schedule::leftjoin('projects', 'projects.id', '=', 'schedule.project_name')
					 ->leftjoin('users', 'users.id', '=', 'schedule.physician_name')
					 ->leftjoin('manufacturers', 'manufacturers.id', '=', 'schedule.manufacturer')
					 ->leftjoin('device', 'device.id', '=', 'schedule.device_name')
					 ->leftjoin('clients', 'clients.id', '=', 'schedule.client_name')
					 ->where('users.organization', '=', $organization)
					 ->where('schedule.is_delete', '=', '0')->count();
			
		} else {
            $schedules = schedule::leftjoin('projects', 'projects.id', '=', 'schedule.project_name')
						 ->leftjoin('users', 'users.id', '=', 'schedule.physician_name')
						 ->leftjoin('manufacturers', 'manufacturers.id', '=', 'schedule.manufacturer')
						 ->leftjoin('device', 'device.id', '=', 'schedule.device_name')
						 ->leftjoin('clients', 'clients.id', '=', 'schedule.client_name')
						 ->select('schedule.*', 'clients.client_name', 'users.name', 'manufacturers.manufacturer_name', 'device.device_name as device', 'projects.project_name as pro_name')
						 ->where('schedule.is_delete', '=', '0')
						 ->orderby('schedule.id', '=', 'DESC')
						 ->paginate($pagesize);
       
	   	 $count = schedule::leftjoin('projects', 'projects.id', '=', 'schedule.project_name')
		          ->leftjoin('users', 'users.id', '=', 'schedule.physician_name')
				  ->leftjoin('manufacturers', 'manufacturers.id', '=', 'schedule.manufacturer')
				  ->leftjoin('device', 'device.id', '=', 'schedule.device_name')
				  ->leftjoin('clients', 'clients.id', '=', 'schedule.client_name')
				  ->where('schedule.is_delete','=',0)->count();
	   
	   
	    }
	//	$now = date('d-m-Y');
    //   	$schedule_date =  $schedules[0]->event_date;
	//	$datediff = $schedule_date - $now;
	
	   
        return view('pages.schedule', compact('schedules', 'count', 'physician', 'manufacturer', 'devices', 'pagesize'));
    }

    public function add() {
        if (Auth::user()->roll == 2) {
            $userid = Auth::user()->id;
            $organization = user::where('id', '=', $userid)->value('organization');
			$projects = [NULL=> 'Project Name'] + project_clients::leftjoin('projects','projects.id','=','project_clients.project_id')
												 ->where('project_clients.client_name','=',$organization)
												 ->where('projects.is_delete','=',0)
												 ->distinct('project.id')
												 ->lists('projects.project_name','projects.id')->all();
            $clients = [NULL => 'Client Name'] + clients::where('id', '=', $organization)->where('is_delete', '=', 0)->lists('client_name', 'id')->all();
        } else {
            $clients = [NULL => 'Client Name'] + clients::where('is_delete', '=', 0)->lists('client_name', 'id')->all();
			$projects = [NULL => 'Project Name'] + Project::where('is_delete', '=', 0)->lists('project_name', 'id')->all();
		}

        
        $manufacturer = ['0' => 'Select Manufacturer'] + Manufacturers::where('is_delete', '=', 0)->lists('manufacturer_name', 'id')->all();
        $devices = ['0' => 'Select Device'] + device::where('is_delete', '=', 0)->lists('device_name', 'id')->all();
        $physician = ['0' => 'Select Physician'] + User::where('is_delete', '=', 0)->where('roll', 3)->lists('name', 'id')->all();
        return view('pages.addschedule', compact('projects', 'manufacturer', 'devices', 'physician', 'clients'));
    }

    public function create(Request $request) {
        $hours = $request->get('start_time_hours');
        $minutes = $request->get('start_time_minutes');
        $time = $request->get('start_time');

        $starttime = $hours . ':' . $minutes . " " . $time;

        $rules = array(
            'project_name' => 'required|not_in:0',
            'manufacturer' => 'required|not_in:0',
            'physician_name' => 'required|not_in:0',
            'device_name' => 'required|not_in:0',
            'model_no' => 'required',
            'rep_name' => 'required',
            'event_date' => 'required',
            'status' => 'required|not_in:0'
        );
        if (Auth::user()->roll == '1') {
            $rules['client_name'] = "required|not_in:0";
        }

        $insertdata = array(
            'project_name' => $request->get('project_name'),
            'client_name' => (Auth::user()->roll == '1') ? $request->get('client_name') : Auth::user()->organization,
            'physician_name' => $request->get('physician_name'),
            'patient_id' => rand(1000, 9999),
            'manufacturer' => $request->get('manufacturer_name'),
            'device_name' => $request->get('device_name'),
            'model_no' => $request->get('model_no'),
            'rep_name' => $request->get('rep_name'),
            'event_date' => $request->get('event_date'),
            'start_time' => $starttime,
            'status' => $request->get('status')
        );


        $validator = Validator::make($insertdata, $rules);

        if ($validator->fails()) {
            return Redirect::to('admin/schedule/add')
                            ->withErrors($validator);
        } else {
		

            $insert_schedule = schedule::insert($insertdata);
            $insert_schedule = 1;
			if ($insert_schedule) {
                $schedule_id = schedule::get()->last();
                $schedule_id = $schedule_id->id;
                $schedules = schedule::join('projects', 'projects.id', '=', 'schedule.project_name')
							->join('users', 'users.id', '=', 'schedule.physician_name')
							->join('manufacturers', 'manufacturers.id', '=', 'schedule.manufacturer')
							->join('device', 'device.id', '=', 'schedule.device_name')
							->join('clients', 'clients.id', '=', 'schedule.client_name')
							->select('schedule.*', 'clients.client_name', 'users.name', 'manufacturers.manufacturer_name', 'device.device_name as device', 'projects.project_name as pro_name')
							->where('schedule.id', '=', $schedule_id)
							->get();
							
                foreach ($schedules as $schedule) {
                    $title = "Welcome to Neptune-PPA - Schedule ";
                    $projectname = $schedule->pro_name;
                    $clientname = $schedule->client_name;
                    $physician = $schedule->name;
                    $patientid = $schedule->patient_id;
                    $manufacturer = $schedule->manufacturer_name;
                    $device = $schedule->device;
                    $model_name = $schedule->model_no;
                    $rep_name = $schedule->rep_name;
                    $event_date = $schedule->event_date;
                    $start_time = $schedule->start_time;
					
					$physicianid = $schedule->physician_name;
					
				
                    $data = array('title' => $title, 'projectname' => $projectname, 'clientname' => $clientname, 'physician' => $physician, 'patient_id' => $patientid, 'manufacturer' => $manufacturer, 'device' => $device, 'model_no' => $model_name, 'rep_name' => $rep_name, 'event_date' => $event_date, 'start_time' => $start_time,'physicianid'=>$physicianid);
                
				}	
				
					
				Mail::send('emails.createschedule', $data, function($message) use ($data) {
				$repemail = user::where('name','=',$data['rep_name'])
							->where('roll','=',5)
							->where('status','=','Enabled')
							->value('email');
				$physicianmail = user::where('id','=',$data['physicianid'])
								->where('roll','=',3)
								->where('status','=','Enabled')
								->value('email');
				
				if($repemail != "")
				{
					$message->to($repemail);
				}
				
				if($physicianmail != "")
				{
					$message->to($physicianmail);
				}
                    $message->from('admin@neptuneppa.com')->subject($data['title']);
				$organization = clients::where('client_name','=',$data['clientname'])->value('id');
				$get_user_emails = user::where('roll','=',2)
									->where('organization','=',$organization)
									->where('status','=','Enabled')
									->where('is_delete','=',0)
									->get();
				foreach ($get_user_emails as $user_email) 
				{
						if($user_email != "")
						{
					    	$message->to($user_email->email);
						}
				} 
                });

				
				
				
				

				
            }
           return Redirect::to('admin/schedule');
        }
    }

    public function edit($id) {
        if (Auth::user()->roll == 2) {
            $userid = Auth::user()->id;
            $organization = user::where('id', '=', $userid)->value('organization');
			$projects = [NULL=> 'Project Name'] + project_clients::join('projects','projects.id','=','project_clients.project_id')
												 ->where('project_clients.client_name','=',$organization)
												 ->where('projects.is_delete','=',0)
												 ->distinct('project.id')
												 ->lists('projects.project_name','projects.id')->all();
            $clients = [NULL => 'Client Name'] + clients::where('id', '=', $organization)->where('is_delete', '=', 0)->lists('client_name', 'id')->all();
			
		
		} 
		else {
            $clients = [NULL => 'Client Name'] + clients::where('is_delete', '=', 0)->lists('client_name', 'id')->all();
			$projects = Project::where('is_delete', '=', 0)->lists('project_name', 'id')->all();
		}
        
        $manufacturer = Manufacturers::where('is_delete', '=', 0)->lists('manufacturer_name', 'id')->all();
        $devices = ['0' => 'Select Device'] + device::where('is_delete', '=', 0)->lists('device_name', 'id')->all();
        $physician = User::where('roll', 3)->where('is_delete', '=', 0)->lists('name', 'id')->all();
        $schedules = schedule::FindOrFail($id);
        $time = $schedules->start_time;
        $starttime = preg_split("/(\s|:)/", $time);
        return view('pages.editschedule', compact('schedules', 'clients', 'projects', 'manufacturer', 'devices', 'physician', 'starttime'));
    }

    public function update($id, Request $request) {
        $hours = $request->get('start_time_hours');
        $minutes = $request->get('start_time_minutes');
        $time = $request->get('start_time');
        $start_time = $hours . ':' . $minutes . " " . $time;

        $rules = array(
            'project_name' => 'required|not_in:0',
            'physician_name' => 'required|not_in:0',
            'manufacturer' => 'required|not_in:0',
            'device_name' => 'required|not_in:0',
            'model_no' => 'required',
            'rep_name' => 'required',
            'event_date' => 'required',
            'status' => 'required|not_in:0'
        );
        if (Auth::user()->roll == '1') {
            $rules['client_name'] = "required|not_in:0";
        }

        $updatedata = array(
            'project_name' => $request->get('project_name'),
            'client_name' => (Auth::user()->roll == '1') ? $request->get('client_name') : Auth::user()->organization,
            'physician_name' => $request->get('physician_name'),
            'manufacturer' => $request->get('manufacturer'),
            'device_name' => $request->get('device_name'),
            'model_no' => $request->get('model_no'),
            'rep_name' => $request->get('rep_name'),
            'event_date' => $request->get('event_date'),
            'start_time' => $start_time,
            'status' => $request->get('status')
        );

        $validator = Validator::make($updatedata, $rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        } else {

            $update = DB::table('schedule')->where('id', '=', $id)->update($updatedata);
           
            if($update)
            {

                $schedules = schedule::join('projects', 'projects.id', '=', 'schedule.project_name')
                            ->join('users', 'users.id', '=', 'schedule.physician_name')
                            ->join('manufacturers', 'manufacturers.id', '=', 'schedule.manufacturer')
                            ->join('device', 'device.id', '=', 'schedule.device_name')
                            ->join('clients', 'clients.id', '=', 'schedule.client_name')
                            ->select('schedule.*', 'clients.client_name', 'users.name', 'manufacturers.manufacturer_name', 'device.device_name as device', 'projects.project_name as pro_name')
                            ->where('schedule.id', '=', $id)
                            ->get();
                
                $title = "Welcome to Neptune-PPA - Schedule update";
                $projectname = $schedules[0]['pro_name'];
                $clientname = $schedules[0]['client_name'];
                $physician = $schedules[0]['name'];
                $patientid = $schedules[0]['patient_id'];
                $manufacturer = $schedules[0]['manufacturer_name'];
                $device = $schedules[0]['device'];
                $model_name = $schedules[0]['model_no'];
                $rep_name = $schedules[0]['rep_name'];
                $event_date = $schedules[0]['event_date'];
                $start_time = $schedules[0]['start_time'];
                $physicianid = $schedules[0]['physician_name'];



                $data = array(
                        'title' => $title, 
                        'projectname' => $projectname, 
                        'clientname' => $clientname, 
                        'physician' => $physician, 
                        'patient_id' => $patientid, 
                        'manufacturer' => $manufacturer, 
                        'device' => $device, 
                        'model_no' => $model_name, 
                        'rep_name' => $rep_name, 
                        'event_date' => $event_date, 
                        'start_time' => $start_time,
                        'physicianid'=>$physicianid
                        );

                Mail::send('emails.createschedule', $data, function($message) use ($data) {
                $repemail = user::where('name','=',$data['rep_name'])
                            ->where('roll','=',5)
                            ->where('status','=','Enabled')
                            ->value('email');
                $physicianmail = user::where('id','=',$data['physicianid'])
                                ->where('roll','=',3)
                                ->where('status','=','Enabled')
                                ->value('email');
                
                if($repemail != "")
                {
                    $message->to($repemail);
                }
                
                if($physicianmail != "")
                {
                    $message->to($physicianmail);
                }
                    $message->from('admin@neptuneppa.com')->subject($data['title']);
                $organization = clients::where('client_name','=',$data['clientname'])->value('id');
                $get_user_emails = user::where('roll','=',2)
                                    ->where('organization','=',$organization)
                                    ->where('status','=','Enabled')
                                    ->where('is_delete','=',0)
                                    ->get();
                foreach ($get_user_emails as $user_email) 
                {
                        if($user_email != "")
                        {
                            $message->to($user_email->email);
                        }
                } 
                });
                
            }
            return Redirect::to('admin/schedule');
        }
    }

    public function search() {
        $fieldname = Input::get('fieldName');
        $fieldvalue = Input::get('value');
        $projectid = Input::get('projectid');
        if (Auth::user()->roll == 2) {
            $userid = Auth::user()->id;
            $organization = user::where('id', '=', $userid)->value('organization');
            $search_schedules = schedule::leftjoin('projects', 'projects.id', '=', 'schedule.project_name')
								->leftjoin('users', 'users.id', '=', 'schedule.physician_name')
								->leftjoin('manufacturers', 'manufacturers.id', '=', 'schedule.manufacturer')
								->leftjoin('device', 'device.id', '=', 'schedule.device_name')
								->leftjoin('clients', 'clients.id', '=', 'schedule.client_name')
								->where('users.organization', '=', $organization)
								->select('schedule.*', 'clients.client_name', 'users.name', 'manufacturers.manufacturer_name', 'device.device_name as device', 'projects.project_name as pro_name')
								->where('schedule.is_delete', '=', '0')
								->where($fieldname, 'LIKE', $fieldvalue . '%'
								)->orderby('schedule.id', '=', 'DESC')
								->get();
        } else {


            $search_schedules = schedule::leftjoin('projects', 'projects.id', '=', 'schedule.project_name')
								->leftjoin('clients', 'clients.id', '=', 'schedule.client_name')
								->leftjoin('users', 'users.id', '=', 'schedule.physician_name')
								->leftjoin('manufacturers', 'manufacturers.id', '=', 'schedule.manufacturer')
								->leftjoin('device', 'device.id', '=', 'schedule.device_name')
								->select('schedule.*', 'users.name', 'clients.client_name', 'manufacturers.manufacturer_name', 'device.device_name as device', 'projects.project_name as pro_name')
								->where('schedule.is_delete', '=', '0')
								->where($fieldname, 'LIKE', $fieldvalue . '%')
								->orderby('schedule.id', '=', 'DESC')
								->get();
        }
        $data = $search_schedules;

        if (count($data))
            return [
                'value' => $data,
                'status' => TRUE
            ];
        else
            return [
                'value' => 'No Result Found',
                'status' => FALSE
            ];
    }

    public function updateall() {
        /* $hiddenid = Input::get('hiddenid');
          $physician = Input::get('physician_name');
          $manufacturer = Input::get('manufacturer_name');
          $device = Input::get('device_name');


          for($i = 0; $i < count($hiddenid); $i++)
          {
          $updaterecord = array(
          "physician_name" => $physician[$i],
          "device_name" => $device[$i],
          "manufacturer" => $manufacturer[$i]
          );
          $updateschedule = DB::table('schedule')->where('id', '=', $hiddenid[$i])->update($updaterecord);
          } */
        return Redirect::to('admin/schedule');
    }

    public function devicedetails() {
        $deviceid = Input::get('deviceid');

        $search_device = device::join('users', 'users.id', '=', 'device.rep_email')->select('device.model_name', 'users.name')->where('device.id', '=', $deviceid)->get();
        $data = $search_device;

        if (count($data))
            return [
                'value' => $data,
                'status' => TRUE
            ];
        else
            return [
                'value' => 'No Result Found',
                'status' => FALSE
            ];
    }

    public function remove($id) {
        $removedata = array('is_delete' => '1');
        $remove = DB::table('schedule')->where('id', '=', $id)->update($removedata);
        return Redirect::to('admin/schedule');
    }

    public function getclientname() {
        $projectid = Input::get('projectid');
        if (Auth::user()->roll == 2) {
            $userid = Auth::user()->id;
            $organization = user::where('id', '=', $userid)->value('organization');
            $getclientname = project_clients::join('clients', 'clients.id', '=', 'project_clients.client_name')
			->where('clients.id', '=', $organization)
			->where('project_clients.project_id', '=', $projectid)
			->select('clients.client_name', 'clients.id')
			->get();
        } else {
            $getclientname = project_clients::join('clients', 'clients.id', '=', 'project_clients.client_name')
			->where('project_clients.project_id', '=', $projectid)
			->select('clients.client_name', 'clients.id')
			->get();
        }

        $data = $getclientname;

        if (count($data))
            return [
                'value' => $data,
                'status' => TRUE
            ];
        else
            return [
                'value' => 'No result Found',
                'status' => FALSE
            ];
    }

    public function getphysician() 
	{
		$clientid = Input::get('clientid');
		$projectid = Input::get('projectid');
		if(Auth::user()->roll == 2)
		{
			$clientid = Auth::user()->organization;
			$getphysician = user::where('projectname','=',$projectid)
							->where('organization','=',$clientid)
							->where('roll','=',3)
							->where('is_delete','=',0)
							->get();
		}
		
        else{
        $getphysician = user::where('organization', '=', $clientid)->where('projectname','=',$projectid)->where('roll','=',3)->where('is_delete','=',0)->get();
		}

        $data = $getphysician;

        if (count($data))
            return [
                'value' => $data,
                'status' => TRUE
            ];
        else
            return [
                'value' => 'No result Found',
                'status' => FALSE
            ];
    }

    public function getmanufacturer() {
        $projectid = Input::get('projectid');
        $getmanufacturer = device::join('manufacturers', 'manufacturers.id', '=', 'device.manufacturer_name')
		->where('device.project_name', '=', $projectid)
		->groupby('device.manufacturer_name')
		->where('device.is_delete','=',0)
		->get();


        $manufacturerdata = $getmanufacturer;

        if (count($manufacturerdata))
            return [
                'value' => $manufacturerdata,
                'status' => TRUE
            ];
        else
            return [
                'value' => 'No result Found',
                'status' => FALSE
            ];
    }

    public function getdevicename() {
        $projectid = Input::get('projectid');
        $manufacturerid = Input::get('manufacturerid');
        $getclientname = device::where('manufacturer_name', '=', $manufacturerid)
		->where('project_name', '=', $projectid)
		->where('is_delete','=',0)
		->select('device_name', 'id')->get();
        $data = $getclientname;

        if (count($data))
            return [
                'value' => $data,
                'status' => TRUE
            ];
        else
            return [
                'value' => 'No result Found',
                'status' => FALSE
            ];
    }
	
	
	public function export()
	{
	
	
		$scheduleid = Input::get('schedule_chk');
			
			$schedule_id = [];
			foreach($scheduleid as $schedule)
			{
				$schedule_id[] = $schedule;
				
			}
		$schedules = schedule::leftjoin('projects', 'projects.id', '=', 'schedule.project_name')
				 ->leftjoin('users', 'users.id', '=', 'schedule.physician_name')
				 ->leftjoin('manufacturers', 'manufacturers.id', '=', 'schedule.manufacturer')
				 ->leftjoin('device', 'device.id', '=', 'schedule.device_name')
				 ->leftjoin('clients', 'clients.id', '=', 'schedule.client_name')
				 ->select('schedule.*', 'clients.client_name', 'users.name', 'manufacturers.manufacturer_name', 'device.device_name as device', 'projects.project_name as pro_name')
				 ->where('schedule.is_delete', '=', '0')
				 ->orderby('schedule.id', '=', 'DESC')
				 ->whereIn('schedule.id',$schedule_id)
        		 ->get();
		
		
		$filename = "schedule.csv";
			$handle = fopen($filename, 'w+');
			fputcsv($handle, array('ID', 'Project Name', 'Client Name','Physician','Patient ID','Manufacturer','Device Name','Model No.','Rep Name','Event date','Start Time'));
		
			foreach($schedules as $row) {
				fputcsv($handle, array($row['id'], $row['pro_name'], $row['client_name'], $row['name'], $row['patient_id'], $row['manufacturer_name'], $row['device'], $row['model_no'], $row['rep_name'], $row['event_date'], $row['start_time']));
			}
		
			fclose($handle);
		
			$headers = array(
				'Content-Type' => 'text/csv',
			);
		
			return Response::download($filename, 'schedule.csv', $headers);
			return Redirect::to('admin/schedule');
	}

}
