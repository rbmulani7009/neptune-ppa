<?php

use Illuminate\Support\Facades\Redirect;

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/admin', function () {
    if (!Auth::check()) {
        return Redirect::to("admin/login");
    } else {
        if (Auth::user()->roll == 1) {
            return Redirect::to("admin/clients");
        } else if (Auth::user()->roll == 2) {
            return Redirect::to("admin/projects");
        } else if (Auth::user()->roll == 4) {
            return Redirect::to("admin/orders");
        } else if (Auth::user()->roll == 5) {
            return Redirect::to("admin/marketshare");
        }
    }
});

Route::get('/', function() {
    if (!Auth::check()) {
        return Redirect::to("login");
    } else {
        return Redirect::to("menu");
    }
});


Route::get('admin/agreeuser', array('uses' => 'client@agreeuser'));

Route::post('admin/agreeconditionsuser', array('uses' => 'client@agreeconditionsuser'));


//client
Route::get('admin/clients', ['middleware' => ['auth', 'roll'], 'uses' => 'client@index']);

Route::get('admin/clients/add', ['middleware' => ['auth', 'roll'], 'uses' => 'client@add']);

Route::post('admin/clients/create', ['middleware' => ['auth', 'roll'], 'uses' => 'client@create']);

Route::get('admin/clients/edit/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'client@edit']);

Route::patch('admin/clients/update/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'client@update']);

Route::get('admin/search_clients', array('as' => 'searchajax', 'uses' => 'client@search'));

Route::get('admin/clients/remove/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'client@remove']);

Route::get('admin/clients/category/sort', ['middleware' => ['auth', 'roll'], 'uses' => 'client@categorysort']);

Route::get('admin/category/sort/getcategoryname',  ['uses' => 'client@getcategoryname']);

Route::post('admin/clients/category/sort/store', ['middleware' => ['auth', 'roll'], 'uses' => 'client@categorysortstore']);

Route::post('admin/clients/category/sort/update', ['middleware' => ['auth', 'roll'], 'uses' => 'client@categorysortupdate']);

Route::get('admin/clients/category/sort/remove/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'client@categorysortremove']);


//project 
Route::get('admin/projects', ['middleware' => ['auth', 'roll'], 'uses' => 'projects@index']);

Route::get('admin/projects/add', ['middleware' => ['auth', 'roll'], 'uses' => 'projects@add']);

Route::post('admin/projects/create', ['middleware' => ['auth', 'roll'], 'uses' => 'projects@create']);

Route::get('admin/projects/edit/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'projects@edit']);

Route::patch('admin/projects/update/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'projects@update']);

Route::get('admin/project_clients/remove/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'projects@clients_remove']);

Route::get('admin/search_projects', array('as' => 'searchajax', 'uses' => 'projects@search'));

Route::get('admin/projects/remove/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'projects@remove']);

//category
Route::get('admin/category', ['middleware' => ['auth', 'roll'], 'uses' => 'categories@index']);

Route::get('admin/category/add', ['middleware' => ['auth', 'roll'], 'uses' => 'categories@add']);

Route::post('admin/category/create', ['middleware' => ['auth', 'roll'], 'uses' => 'categories@create']);

Route::get('admin/category/edit/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'categories@edit']);

Route::patch('admin/category/update/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'categories@update']);

Route::get('admin/search_category', array('as' => 'searchajax', 'uses' => 'categories@search'));

Route::get('admin/category/viewclient/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'categories@viewclient']);

Route::get('admin/viewclientsearch', array('as' => 'searchajax', 'uses' => 'categories@viewclientsearch'));

Route::get('admin/category/remove/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'categories@remove']);

//users

Route::get('admin/users', ['middleware' => ['auth', 'roll'], 'uses' => 'users@index']);
Route::get('admin/users/add', ['middleware' => ['auth', 'roll'], 'uses' => 'users@add']);
Route::post('admin/users/create', ['middleware' => ['auth', 'roll'], 'uses' => 'users@create']);
Route::get('admin/users/edit/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'users@edit']);
Route::patch('admin/users/update/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'users@update']);
Route::get('admin/search_user', array('as' => 'searchajax', 'uses' => 'users@search'));
Route::post('admin/users/updateall', ['middleware' => ['auth', 'roll'], 'uses' => 'users@updateall']);
Route::get('admin/users/remove/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'users@remove']);
Route::post('admin/users/remove/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'users@remove']);
Route::get('admin/getprojects', ['uses' => 'users@getprojectname']);
Route::get('admin/getprojectnames', ['uses' => 'users@getprojectnames']);


//manufacturers
Route::get('admin/manufacturer', ['middleware' => ['auth', 'roll'], 'uses' => 'manufacturer@index']);
Route::get('admin/manufacturer/add', ['middleware' => ['auth', 'roll'], 'uses' => 'manufacturer@add']);
Route::post('admin/manufacturer/create', ['middleware' => ['auth', 'roll'], 'uses' => 'manufacturer@create']);
Route::get('admin/manufacturer/edit/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'manufacturer@edit']);
Route::post('admin/manufacturer/update/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'manufacturer@update']);
Route::get('admin/search_manufacturer', array('as' => 'searchajax', 'uses' => 'manufacturer@search'));
Route::get('admin/manufacturer/remove/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'manufacturer@remove']);

//devices
Route::get('admin/devices', ['middleware' => ['auth', 'roll'], 'uses' => 'devices@index']);

Route::get('admin/devices/add', ['middleware' => ['auth', 'roll'], 'uses' => 'devices@add']);

Route::post('admin/devices/create', ['middleware' => ['auth', 'roll'], 'uses' => 'devices@create']);

Route::get('admin/devices/view/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'devices@view']);

Route::get('admin/devices/edit/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'devices@edit']);

Route::patch('admin/devices/update/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'devices@update']);

Route::get('admin/search_device', array('as' => 'searchajax', 'uses' => 'devices@search'));

Route::get('admin/searchclientprice', array('as' => 'searchajax', 'uses' => 'devices@searchclientprice'));

Route::get('admin/devices/clientprice/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'devices@clientprice']);

Route::post('admin/devices/clientpricecreate', ['middleware' => ['auth', 'roll'], 'uses' => 'devices@clientpricecreate']);

Route::get('admin/devices/clientpriceedit/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'devices@clientpriceedit']);

Route::patch('admin/devices/clientpriceupdate/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'devices@clientpriceupdate']);

Route::get('admin/devices/remove/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'devices@remove']);

Route::get('admin/getcategory', ['middleware' => ['auth', 'roll'], 'uses' => 'devices@getcategory']);

Route::get('admin/devices/clientpriceremove/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'devices@priceremove']);

Route::get('admin/devices/customfield/remove/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'devices@customfieldremove']);

Route::get('admin/getrep', ['uses' => 'devices@getrepemail']);

Route::get('admin/getorderemail', ['uses' => 'devices@getorderemail']);

Route::get('admin/devices/devicefeatures/{id}',['middleware' => ['auth', 'roll'], 'uses' => 'devices@devicefeatures']);

Route::post('admin/devices/devicefeatures/create',['middleware' => ['auth', 'roll'], 'uses' => 'devices@devicefeaturesstore']);

Route::get('admin/devices/devicefeatures/edit/{id}',['middleware' => ['auth', 'roll'], 'uses' => 'devices@devicefeaturesedit']);

Route::post('admin/devices/devicefeatures/update/{id}',['middleware' => ['auth', 'roll'], 'uses' => 'devices@devicefeaturesupdate']);

Route::get('admin/devices/devicefeatures/remove/{id}',['middleware' => ['auth', 'roll'], 'uses' => 'devices@devicefeaturesremove']);

Route::get('admin/searchdevicefeatures', ['uses' => 'devices@serarchdevicefeatures']);


//orders

Route::get('admin/orders', ['middleware' => ['auth', 'roll'], 'uses' => 'orders@index']);

Route::get('admin/orders/edit/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'orders@edit']);

Route::Patch('admin/orders/update/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'orders@update']);

Route::post('admin/orders/updateall', ['middleware' => ['auth', 'roll'], 'uses' => 'orders@updateall']);

Route::get('admin/search_order', array('as' => 'searchajax', 'uses' => 'orders@search'));

Route::get('admin/orders/remove/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'orders@remove']);

Route::post('admin/orders/archive', ['middleware' => ['auth', 'roll'], 'uses' => 'orders@archive']);

Route::get('admin/orders/viewarchive', ['middleware' => ['auth', 'roll'], 'uses' => 'orders@viewarchive']);

Route::get('admin/archive_search_order', array('as' => 'searcharchiveajax', 'uses' => 'orders@archiveordersearch'));


//schedule
Route::get('admin/schedule', ['middleware' => ['auth', 'roll'], 'uses' => 'schedules@index']);

Route::get('admin/schedule/add', ['middleware' => ['auth', 'roll'], 'uses' => 'schedules@add']);

Route::post('admin/schedule/create', ['middleware' => ['auth', 'roll'], 'uses' => 'schedules@create']);

Route::get('admin/schedule/edit/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'schedules@edit']);

Route::Patch('admin/schedule/update/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'schedules@update']);

Route::get('admin/search_schedule', array('as' => 'searchajax', 'uses' => 'schedules@search'));

Route::post('admin/schedule/updateall', ['middleware' => ['auth', 'roll'], 'uses' => 'schedules@updateall']);

Route::get('admin/devicedetails', array('as' => 'searchajax', 'uses' => 'schedules@devicedetails'));

Route::get('admin/schedule/remove/{id}', ['middleware' => ['auth', 'roll'], 'uses' => 'schedules@remove']);

Route::get('admin/getclientname', ['middleware' => ['auth', 'roll'], 'uses' => 'schedules@getclientname']);

Route::get('admin/getdevicename', ['middleware' => ['auth', 'roll'], 'uses' => 'schedules@getdevicename']);
Route::get('admin/getphysician', ['uses' => 'schedules@getphysician']);
Route::get('admin/getmanufacturers', ['uses' => 'schedules@getmanufacturer']);

//marketshare
Route::get('admin/marketshare', ['middleware' => ['auth', 'roll'], 'uses' => 'marketshare@index']);

Route::get('admin/search_marketshare', ['middleware' => 'auth', 'uses' => 'marketshare@search']);

Route::get('admin/login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@showLoginForm']);
Route::post('admin/login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@login']);
Route::get('admin/logout', ['as' => 'auth.logout', 'uses' => 'Auth\AuthController@logout']);

//forgot password
Route::auth();

//frontend
Route::get('login', array('uses' => 'userlogin@showlogin'));
Route::post('login', array('uses' => 'userlogin@dologin'));

Route::group(['middleware' => ['frontendauth']], function () {
    Route::post('logincontinue','userlogin@logincontinue');
    Route::get('menu', array('uses' => 'usermenu@index'));
    Route::get('changeout/mainmenu', array('uses' => 'usermenu@changeout_menu'));
    Route::get('newdevice/mainmenu', array('uses' => 'usermenu@newdevice_menu'));

    Route::get('newdevice/devices/{id}', array('uses' => 'products@newdevice'));

    Route::get('changeout/devices/{id}', array('uses' => 'products@changeout'));

    Route::get('compareproducts', array('uses' => 'products@compareproduct'));

    Route::post('purchase', array('uses' => 'products@purchase'));
    Route::get('purchase', array('uses' => 'products@purchase'));

    Route::get('agree', array('uses' => 'userlogin@agree'));

    Route::post('agreeconditions', array('uses' => 'userlogin@agreeconditions'));
    Route::get('logout', array('uses' => 'userlogin@logout'));
});



//export

Route::post('admin/devices/export', ['as' => 'admin.device_export.excel', 'uses' => 'devices@export']);

Route::post('admin/devices/import', ['as' => 'admin.device_export.excel', 'uses' => 'devices@import']);

Route::post('admin/orders/export', ['as' => 'admin.order_export.excel', 'uses' => 'orders@export']);

Route::post('admin/schedule/export', ['as' => 'admin.schedule_export.excel', 'uses' => 'schedules@export']);

Route::get('admin/marketshare/export', ['as' => 'admin.marketshare_export.excel', 'uses' => 'marketshare@export']);

// Device Survey Routes

Route::get('admin/devices/devicesurvey/{id}','devices@deviceSurvey');
Route::post('admin/devices/devicesurvey/create','devices@deviceSurveyStore');
Route::get('admin/devices/devicesurvey/edit/{id}','devices@deviceSurveyEdit');
Route::PUT('admin/devices/devicesurvey/update/{id}','devices@deviceSurveyUpdate');
Route::get('admin/devices/devicesurvey/remove/{id}','devices@deviceSurveyRemove');
Route::get('admin/devices/devicesurvey/copysurvey/{id}','devices@deviceSurveyCopy');
Route::post('admin/devices/devicesurvey/search','devices@deviceSurveySearch');
Route::get('admin/devices/devicesurvey/view/{id}','devices@deviceSurveyView');
Route::get('admin/devices/devicesurvey/answerRemove/{id}','devices@deviceSurveyAnswerRemove');
Route::post('admin/devices/devicesurveyanswer/search','devices@deviceSurveyAnswerSearch');


// Device Custom Contact information Routes

Route::get('admin/devices/customcontact/{id}','devices@customContact');
Route::post('admin/devices/customcontact/store','devices@contactStore');
Route::post('admin/devices/customcontact/getordermail','devices@getOrderMail');
Route::get('admin/devices/customcontact/edit/{id}','devices@customContactEdit');
Route::PUT('admin/devices/customcontact/update/{id}','devices@customContactUpdate');
Route::get('admin/devices/customcontact/remove/{id}','devices@customContactRemove');
Route::post('admin/devices/customcontact/search','devices@contactSearch');


// device Rep infomation Routes

Route::post('admin/devices/repinfo/status','devices@repstatus');
Route::get('admin/devices/repinfo/edit/{id}/{deviceId}','devices@repStatusEdit');
Route::post('admin/devices/repinfo/update/{id}','devices@repStatusUpdate');
Route::post('admin/devices/reinfo/export','devices@repinfoexport');
Route::post('admin/devices/repcontact/search','devices@repcontactsearch');



// Physician ScoreCard Routes

Route::get('admin/users/scorecard/{id}','users@phyScoreCard');
Route::get('admin/users/scorecard/create/{id}','users@phyScoreCardCreate');
Route::post('admin/users/scorecard/store/{id}','users@phyScoreCardStore');
Route::get('admin/users/scorecard/view/{id}','users@phyScoreCardView');
Route::get('admin/users/scorecard/edit/{id}','users@phyScoreCardEdit');
Route::PUT('admin/users/scorecard/update/{id}','users@phyScoreCardUpdate');
Route::post('admin/users/scorecard/image/remove/{id}','users@phyScoreCardImageRemove');
Route::post('admin/users/scorecard/remove/{id}','users@phyScoreCardRemove');
Route::post('admin/users/scorecard/search','users@phySCoreCardSearch');





// Frontend Device Rep INfo routes

Route::post('repcontact/info','products@repContactInfo');

// Frontend  Device Survey Routes
Route::post('survey/question','products@surveyQuestion');
Route::post('survey/questionAnswer','products@surveyQuestionAnswer');

// Frontend Scorecard Routes
Route::get('scorecard','ScoreCardController@index');
Route::post('scorecard/getmonth','ScoreCardController@getMonth');
Route::get('scorecard/scorecardimage/{id}','ScoreCardController@getImage');




/* scripts for data migration one table to another table */
Route::get('migratedata', 'users@migratedata');